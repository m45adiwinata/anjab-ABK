@extends('layouts.main')

@section('title' , 'Dashboard Anjab ABK')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
              <ol class="breadcrumb float-sm-left">
                <li class="breadcrumb-item"><a href="/">Home</a></li>
                <li class="breadcrumb-item active">Profile</li>
              </ol>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <!-- <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard</li>
            </ol> -->
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Main row -->
        <div class="row">
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="card card-primary">
                    <!-- form start -->
                    <form method="POST" action="{{route('updateuser')}}" id="form-tambah">
                        @csrf
                        <div class="row">
                            <div class="col-md-8">
                                <div class="card-body">
                                    <div class="form-group required">
                                        <label for="nama" class="control-label">Nama</label>
                                        <input type="text" class="form-control" id="nama" name="nama" autocomplete="off" value="{{Auth::user()->name}}">
                                    </div>
                                    <div class="form-group required">
                                        <label for="email" class="control-label">Email</label>
                                        <input type="email" class="form-control" id="email" name="email" autocomplete="off" value="{{Auth::user()->email}}">
                                    </div>
                                    <div class="form-group required">
                                        <label for="username" class="control-label">Username</label>
                                        <input type="username" class="form-control" id="username" name="username" autocomplete="off" value="{{Auth::user()->username}}">
                                    </div>
                                    <div class="form-group required">
                                        <label for="password" class="control-label">Password</label>
                                        <input type="password" class="form-control" id="password" name="password" autocomplete="off">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-8">
                                <div class="card-footer" style="text-align: end;">
                                  <button type="submit" class="btn btn-primary btn-primary-custom">Simpan</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

@endsection
@section('script')
<script>
      $(document).ready(function() {
        $('[data-toggle="tooltip"]').tooltip();   
      });
  </script>

  
  @endsection