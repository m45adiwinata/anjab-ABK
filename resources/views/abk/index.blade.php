@extends('layouts.main')

@section('title' , 'Anjab ABK')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h5 class="m-0">ANJAB ABK</h5>
            <ol class="breadcrumb float-sm-left">
              <li class="breadcrumb-item"><a href="#">Anjab ABK</a></li>
              <li class="breadcrumb-item active">List SKPD</li>
            </ol>
          </div><!-- /.col -->
          <div class="col-sm-6">
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- <div class="row">
          <div class="col-12">
            <a class="btn btn-info mr-2 mb-3 pl-3 pr-3" href="/abk/tambah-skpd"><span class="fas fa-plus" style="margin-right: 5px;"></span>Tambah</a>
          </div>
        </div> -->
        @if(Session::has('success'))
          <div class="alert alert-success" style="padding: 5px 10px;background-color: #d4edda;border-color: #c3e6cb;color: #155724;" role="alert">
            <i class="far fa-check-circle" style="margin-right:5px;"></i>{{ Session::get('success') }}<a href="#" class="close" data-dismiss="alert" aria-label="close"><i class="far fa-times-circle"></i></a>
          </div>
        @endif
        <!-- Main row -->
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">List SKPD</h3>
                <div class="card-tools">
                  <div class="input-group input-group-sm" style="width: 150px;">
                    <input type="text" name="table_search" id="table_search" class="form-control float-right" placeholder="Search" value="{{isset($_GET['table_search']) ? $_GET['table_search'] : ''}}">
                    <div class="input-group-append">
                      <button class="btn btn-default" id="submit-search">
                        <i class="fas fa-search"></i>
                      </button>
                    </div>
                  </div>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body table-responsive p-0">
                <table class="table table-head-fixed text-nowrap">
                  <thead>
                    <tr>
                      <th style="width:50px;">No</th>
                      <th style="width:200px;">Kode SKPD</th>
                      <th>Nama SKPD</th>
                      <th id="sortable-jumlah-jab" style="cursor:pointer;">
                        Jumlah
                        @if(isset($_GET['sortjumlah']))
                          @if($_GET['sortjumlah'] == 'ASC')
                          <i class="fas fa-long-arrow-alt-up"></i>
                          @elseif($_GET['sortjumlah'] == 'DESC')
                          <i class="fas fa-long-arrow-alt-down"></i>
                          @endif
                        @else
                          <i class="fas fa-long-arrow-alt-up"></i>
                        @endif
                      </th>
                      <th colspan="3" style="text-align: center;">Opsi</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($skpds as $skpd)
                    <tr>
                      <td>{{(($skpds->currentPage()-1) * 20 + $loop->iteration)}}</td>
                      <td>{{$skpd->kode_skpd}}</td>
                      <td style="max-width: 100px;white-space: break-spaces;">{{$skpd->nama_skpd}}</td>
                      <td>{{$skpd->jabatan_count}}</td>
                      <td style="width:40px;"><a class="btn btn-success btn-success-custom" href="/abk/detailskpd/{{$skpd->id}}">Detail</a></td>
                      <!-- @if(Auth::user()->role->edit_skpd_jabatan)
                      <td style="width:40px;"><a class="btn btn-warning btn-warning-custom" href="{{route('editskpd', $skpd->id)}}">Edit</a></td>
                      @endif
                      @if(Auth::user()->role->hapus_skpd_jabatan)
                      <td style="width:40px;"><button class="btn btn-danger btn-danger-custom" onclick="deleteForm({{$skpd->id}})">Hapus</button></td>
                      @endif -->
                    </tr>
                    @endforeach
                  </tbody>
                </table>
                {{ $skpds->onEachSide(2)->links() }}
              </div>
              <!-- /.card-body -->
            
            </div>
            <!-- /.card -->
          </div>
          <div class="container-fluid">
            @if(!empty($var_search))
              <p class="m-0" style="text-align:center">search of - <label style="font-weight:bold">{{$var_search}}</label></p>
            @endif
          </div>
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
      <!-- Modal -->
      <div class="modal fade" id="modaldelete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Hapus Data SKPD</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <form method="POST" id="action-del" action="">
              <div class="modal-body" id="delete-text"></div>
              @csrf
              <input type="hidden" id="id-delete" name="id" >
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-primary">Iya, Hapus</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection
@section('script')
<script>
  function deleteForm(id) {
    $.get('/api/get-skpd/'+id, function(data) {
      $('#modaldelete #delete-text').html('Anda yakin ingin menghapus? '+data.nama_skpd);
      $('#modaldelete #id-delete').val();
      $('#modaldelete').modal('show');
      $('#action-del').attr('action', '/abk/hapus-skpd/'+id);
    });
  }
  var getReq = {!! json_encode($_GET) !!}
  $(document).ready(function() {
    $("#submit-search").click(function() {
      if($('#table_search').val() == "") {
        if(getReq.sortjumlah === undefined) {
          window.location.href = '/abk';
        }
        else {
          window.location.href = '/abk?sortjumlah='+getReq.sortjumlah;
        }
      }
      else {
        if(getReq.sortjumlah === undefined) {
          window.location.href = '/abk?table_search='+$('#table_search').val();
        }
        else {
          window.location.href = '/abk?table_search='+$('#table_search').val()+'&sortjumlah='+getReq.sortjumlah;
        }
      }
    });
    $('#table_search').keypress(function (e) {
      var key = e.which;
      if(key == 13)  // the enter key code
      {
        if($(this).val() == "") {
          if(getReq.sortjumlah === undefined) {
            window.location.href = '/abk';
          }
          else {
            window.location.href = '/abk?sortjumlah='+getReq.sortjumlah;
          }
        }
        else {
          if(getReq.sortjumlah === undefined) {
            window.location.href = '/abk?table_search='+$('#table_search').val();
          }
          else {
            window.location.href = '/abk?table_search='+$('#table_search').val()+'&sortjumlah='+getReq.sortjumlah;
          }
        }
        return false;  
      }
    });
    $('#sortable-jumlah-jab').click(function() {
      if(getReq.sortjumlah === undefined) {
        if(getReq.table_search === undefined) {
          window.location.href = '/abk?sortjumlah=DESC';
        }
        else {
          window.location.href = '/abk?table_search='+getReq.table_search+'&sortjumlah=ASC';
        }
      }
      else {
        if(getReq.table_search === undefined) {
          if(getReq.sortjumlah == 'ASC') {
            window.location.href = '/abk?sortjumlah=DESC';
          }
          else if(getReq.sortjumlah == 'DESC') {
            window.location.href = '/abk?sortjumlah=ASC';
          }
        }
        else {
          if(getReq.sortjumlah == 'ASC') {
            window.location.href = '/abk?table_search='+getReq.table_search+'&sortjumlah=DESC';
          }
          else if(getReq.sortjumlah == 'DESC') {
            window.location.href = '/abk?table_search='+getReq.table_search;
          }
        }
      }
    });
  });
  
</script>
@endsection