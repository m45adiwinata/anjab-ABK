@extends('layouts.main')

@section('title' , 'Detail Jabatan')


@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-left">
            <li class="breadcrumb-item"><a href="/">Anjab ABK</a></li>
                @if(Auth::user()->role_id == 1)
                <li class="breadcrumb-item"><a href="/abk">List SKPD</a></li>
                @endif
              <li class="breadcrumb-item"><a href="/abk/detailskpd/{{$skpd[0]->id_skpd}}">List Jabatan</a></li>
              <li class="breadcrumb-item active">Detail Jabatan</li>
            </ol>
          </div><!-- /.col -->
          <div class="col-sm-6">
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Main row -->
        <div class="row">
            <div class="col-md-12 col-sm-6 col-md-7 col-lg-7">
                <!-- general form elements -->
                <div class="card card-primary">
                    <div style="padding-left:20px;padding-top:10px">
                        <h5 class="m-0">Detail Jabatan SKPD</h5>
                    </div>
                    <!-- form start -->
                    <form method="POST" action="{{route('updatejab')}}" name="updatejab">
                        @csrf
                        <input type="hidden" name="id_skpd_jab" value="{{$id_skpd_jab}}">
                        <div class="row">
                            <div class="col-md-12">
                                @php $pegawai_skpd_jab = $skpd_jab->pegawai()->get(); @endphp
                                @if(count($pegawai_skpd_jab) == 0)
                                <div class="card-body" style="padding-bottom: 0">
                                    <div class="box-img-user">
                                        <img src="{{asset('main/dist/img/user-detail.png')}}" class="img-responsive-user"/>
                                    </div>
                                </div>
                                @else
                                <!-- Topic Cards -->
                                <div id="cards_landscape_wrap-2">
                                    <div class="container">
                                        <div class="row" style="margin: 0;">
                                            @foreach($pegawai_skpd_jab as $pegawai)
                                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                <a href="">
                                                    <div class="card-flyer">
                                                        <div class="text-box">
                                                            <div class="image-box">
                                                                @if(!empty($pegawai->foto))
                                                                    <img src="{{asset($pegawai->foto)}}" alt="pasfoto">
                                                                @else
                                                                    <img src="{{asset('main/dist/img/user-lg.png')}}" style="width: auto;" alt="pasfoto">
                                                                @endif
                                                            </div>
                                                            <div class="text-container">
                                                                <h6>{{$pegawai->nama}}</h6>
                                                                <p>{{$pegawai->nip}}</p>
                                                                <!-- <p class="card-text">m45adiwinata@gmail.com</p> -->
                                                            </div>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                                @endif
                                <div class="card-body">
                                    <div class="form-group required">
                                        <label for="jab-atasan" class="control-label">Jabatan Atasan</label>
                                        @if(Auth::user()->role()->first()->edit_skpd_jabatan == 1)
                                        <select class="form-control select2" id="jab-atasan" name="id_jab_atasan" style="width: 100%;">
                                        @else
                                        <select class="form-control select2" id="jab-atasan" name="id_jab_atasan" style="width: 100%;" disabled>
                                        @endif
                                             <option selected="selected" value="-">-</option>
                                             @foreach($jabatan_atasans as $key => $jab)
                                                @if($jab->pivot->id == $skpd_jab->id_jab_atasan)
                                                     <option selected="selected" value="{{$jab->pivot->id}}">{{$jab->jabatan}}
                                                @else
                                                    <option value="{{$jab->pivot->id}}">{{$jab->jabatan}}
                                                @endif 
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group  col-md-6 required">
                                            <label for="jenis-jab" class="control-label">Jenis Jabatan</label>
                                            <input type="text" class="form-control" id="jenis-jab" name="jenis_jab" autocomplete="off" value="{{$skpd[0]->nama_jenis_jabatan}}" disabled>
                                        </div>
                                        <div class="form-group required col-md-6">
                                            <label for="jab" class="control-label">Jabatan</label>
                                            @if(Auth::user()->role()->first()->edit_skpd_jabatan == 1)
                                            <select class="form-control select2" id="jab" name="id_jabatan" style="width: 100%;" required>
                                            @else
                                            <select class="form-control select2" id="jab" name="id_jabatan" style="width: 100%;" disabled>
                                            @endif
                                                @foreach($jabatan_all as $key => $jab)
                                                    @if($jab->id == $skpd[0]->id_jabatan)
                                                        <option selected="selected" value="{{$jab->id}}">
                                                    @else
                                                        <option value="{{$jab->id}}">
                                                    @endif 
                                                {{$jab->jabatan}}
                                                </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-row">
                                        <div class="form-group col-md-6 required" >
                                            <label for="kelas-jab" class="control-label">Kelas Jabatan</label>
                                            @if(Auth::user()->role()->first()->edit_skpd_jabatan == 1)
                                            <input type="number" class="form-control" value="{{$skpd[0]->kelas_jab}}" id="kelas-jab" name="kelas_jab" autocomplete="off" required>
                                            @else
                                            <input type="number" class="form-control" value="{{$skpd[0]->kelas_jab}}" id="kelas-jab" name="kelas_jab" autocomplete="off" disabled>
                                            @endif
                                        </div>
                                        <div class="form-group col-md-6 required">
                                            <label for="eselon" class="control-label">Eselon Jabatan</label>
                                            <select class="form-control select2" id="eselon" style="width: 100%;" disabled>
                                                <option></option>
                                                @foreach($eselons as $key => $eselon)
                                                @if($eselon->id == $skpd_jab->id_eselon)
                                                <option value="{{$eselon->id}}" selected="selected">
                                                @else
                                                <option value="{{$eselon->id}}">
                                                @endif
                                                {{$eselon->eselon}}
                                                </option>
                                                @endforeach
                                            </select>
                                            <input type="hidden" id="id_eselon" name="id_eselon" value="">
                                        </div>
                                    </div>
                                    <div class="form-group required" >
                                        <label for="pemangku-jab" class="control-label">Jumlah Pemangku Saat Ini</label>
                                        @if(Auth::user()->role()->first()->edit_skpd_jabatan == 1)
                                        <input type="text" class="form-control" id="pemangku-jab" value="{{$skpd[0]->jml_pemangku}}" name="jml_pemangku" autocomplete="off" required>
                                        @else
                                        <input type="text" class="form-control" id="pemangku-jab" value="{{$skpd[0]->jml_pemangku}}" name="jml_pemangku" autocomplete="off" disabled>
                                        @endif
                                    </div>
                                    <div class="form-group required">
                                        <label for="ikhtisar-jab" class="control-label">Ikhtisar Jabatan</label>
                                        @if(Auth::user()->role()->first()->edit_skpd_jabatan == 1)
                                        <textarea type="text" class="form-control" id="ikhtisar-jab" rows="3" name="ikhtisar" autocomplete="off" required>{{$skpd[0]->ikhtisar}}</textarea>
                                        @else
                                        <textarea type="text" class="form-control" id="ikhtisar-jab" rows="3" name="ikhtisar" autocomplete="off" disabled>{{$skpd[0]->ikhtisar}}</textarea>                                        
                                        @endif
                                    </div>
                                </div>
                                <!-- /.card-body -->
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card-footer text-end">
                                    <a href="/abk/detailskpd/{{$skpd[0]->id_skpd}}" class="btn btn-info btn-info-custom mr-3">Kembali</a>
                                    <button type="submit" id="update-jab" class="btn btn-primary btn-primary-custom"{{Auth::user()->role()->first()->edit_skpd_jabatan == 1 ? '':' disabled'}}>Simpan</button>
                                </div>
                            </div>
                            <div class="col-md-4">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-5 col-lg-5">
                <!-- general form elements -->
                <div class="card card-primary">
                    <div class="row">
                        <div class="standard-padding-box" style="width: 100%;">
                            <h5 class="m-0">Laporan Analisis Beban Kerja</h5>
                            <br>
                            @include('components.cetaklaporanbtn')
                            <br>
                            <div class="form-group">
                                <div class="noted-information">
                                    <p style="color:#5555;margin-bottom:5px"><b>Petunjuk Pengisian</b></p>
                                    <p style="color:#5555;margin-bottom:5px">Mohon untuk melengkapi seluruh form ABK dengan baik dan benar</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="modal-xl">
            <div class="modal-dialog modal-xl">
                <form method="post" action="" id="formtambah">
                <div class="modal-content">
                    <div class="modal-body">
                    <div class="card card-primary">
                        <div class="row">
                        <div class="col-md-12">
                            
                        </div>
                        </div>
                    </div>
                    </div>
                    <div class="modal-footer text-right">
                    <button type="button" class="btn btn-default" data-dismiss="modal" style="width:100px; margin-right:30px;">Batal</button>
                    <button class="btn btn-primary" type="submit" style="width:100px;">Simpan</button>
                    </div>
                </div>
                <!-- /.modal-content -->
                </form>
            </div>
            <!-- /.modal-dialog -->
            </div>
            <!-- /.modal -->
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection
@section('script')
<script>
    $(document).ready(function() {
        $.get('/api/get-jenis-jab/' + $('#jab').val(), function(data) {
            $('#jenis-jab').val(data.nama_jenis_jabatan);
        });
        $.get('/api/get-eselon-jab/' + $('#jab').val(), function(data) {
            $('#eselon').val(data.id_eselon).trigger('change');
            $('#id_eselon').val(data.id_eselon);
        });
        $('#jab').change(function() {
            $.get('/api/get-jenis-jab/' + $(this).val(), function(data) {
                $('#jenis-jab').val(data.nama_jenis_jabatan);
            });
            $.get('/api/get-eselon-jab/' + $(this).val(), function(data) {
                $('#eselon').val(data.id_eselon).trigger('change');
                $('#id_eselon').val(data.id_eselon);
            });
        });
        $('#update-jab').click(function(){
                $("form[name='updatejab']").validate({
                    rules: {
                        jenis_jab:'required',
                        id_jabatan : 'required',
                        kelas_jab : 'required',
                        id_eselon : 'required',
                        jml_pemangku : 'required',
                        ikhtisar : 'required',

                    },
                    messages: {
                        jenis_jab: "Mohon masukan jenis jabatan!",
                        id_jabatan: "Mohon masukan jabatan!",
                        kelas_jab: "Mohon masukan kelas jabatan!",
                        id_eselon: "Mohon masukan eselon jabatan!",
                        jml_pemangku: "Mohon masukan jumlah pemangku saat ini!",
                        ikhtisar: "Mohon masukan ikhtisar jabatan!",
                        },
                    submitHandler: function(form) {
                        form.submit();
                    }
                });
        });
    });
</script>
@endsection