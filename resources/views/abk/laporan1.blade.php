<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <style>
            #banner {
                width: 100%;
                height: 50px;
                background-color: grey;
                /* background-image: url("{{asset('images/background-header-laporan.jpg')}}");
                background-repeat: no-repeat;
                background-size: cover; */
                overflow: auto; color:white; font-weight:bold; text-align:center; line-height:2.5;
            }
            #content {
                width: 90%;
                margin-left:40px;
                margin-right:30px;
                margin-bottom:30px;
                margin-top:30px;
            }
            .header-hijau tbody tr td {border:1px solid black;}
            .header-hijau {border-collapse: collapse;margin-left:40px; margin-right:40px;}
            .header-pink tbody tr td {border:1px solid black;}
            .header-pink {border-collapse: collapse;margin-left:40px; margin-right:40px;}
            .header-hijau thead tr th {background: green; border:1px solid black;}
            .header-pink thead tr th {background: #e3be46; border:1px solid black;}
            .borderless {margin-left:40px; margin-right:40px;}
            /* table {table-layout: fixed;} */
            table tbody tr td {vertical-align:top;}
            @page {margin-left: 0px;margin-right: 0px; margin-bottom: 0px; margin-top: 60px;}
            body { margin: 0px;}
        </style>
    </head>
    <body>
        <div id="banner">
            ANALISA JABATAN DAN ANALISIS BEBAN KERJA
        </div>
        <div id="content">
            <table class="table">
                <tbody>
                    <tr>
                        <td style="width:30px;"><b>1.</b></td>
                        <td colspan="2"><b>Nama Jabatan</b></td>
                        <td style="width:300px;">: <b>{{$jabatan->jabatan}}</b></td>
                    </tr>
                    <tr>
                        <td><b>2.</b></td>
                        <td colspan="2"><b>Kode Jabatan</b></td>
                        <td>: <b>-</b></td>
                    </tr>
                    <tr>
                        <td><b>3.</b></td>
                        <td colspan="2"><b>UNIT KERJA</b></td>
                        <td>: {{$skpd->nama_skpd}}</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td style="width:20px;">a.</td>
                        <td style="width:200px;">JPT Utama</td>
                        <td>: </td>
                    </tr>
                    <tr>
                        <td></td>
                        <td style="width:20px;">b.</td>
                        <td>JPT Madya</td>
                        <td>: </td>
                    </tr>
                    @php $count_atasan = count($atasans); @endphp
                    <tr>
                        <td></td>
                        <td style="width:20px;">c.</td>
                        <td>JPT Pratama</td>
                        <td>: {{$count_atasan == 1 ? 'Seretariat Daerah Kabupaten Buleleng' : $atasans[$count_atasan-1]->jabatan()->first()->jabatan}}</td>
                    </tr>
                    @php
                    if($count_atasan >= 3) {
                        $count_atasan--;
                    }
                    @endphp
                    <tr>
                        <td></td>
                        <td style="width:20px;">d.</td>
                        <td>Administrator </td>
                        <td>: {{!empty($atasans[$count_atasan-1]) ? $atasans[$count_atasan-1]->jabatan()->first()->jabatan : ''}}</td>
                    </tr>
                    @php $count_atasan--; @endphp
                    <tr>
                        <td></td>
                        <td style="width:20px;">e.</td>
                        <td>Pengawas </td>
                        <td>: {{!empty($atasans[$count_atasan-1]) ? $atasans[$count_atasan-1]->jabatan()->first()->jabatan : ''}}</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td style="width:20px;">f.</td>
                        <td>Pelaksana</td>
                        <td>: {{$skpd_jab->jabatan()->first()->jenisJab()->first()->id == 6 ? $skpd_jab->jabatan()->first()->jabatan : ''}}</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td style="width:20px;">g.</td>
                        <td>Jabatan Fungsional</td>
                        <td>: </td>
                    </tr>
                    <tr>
                        <td><b>4.</b></td>
                        <td colspan="2"><b>Ikhtisar Jabatan</b></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td colspan="3" style="text-align: justify;">
                            {{$skpd_jab->ikhtisar}}
                        </td>
                    </tr>
                    <tr>
                        <td><b>5.</b></td>
                        <td colspan="2"><b>KUALIFIKASI JABATAN</b></td>
                        <td></td>
                    </tr>
                    @foreach($kualifikasi_jabatans as $kualifikasi_jabatan)
                    <tr>
                        <td></td>
                        <td>a.</td>
                        <td>Pendidikan Formal</td>
                        <td>:{{$kualifikasi_jabatan->pendidikan()->first()->nama_pendidikan}} bidang {{$kualifikasi_jabatan->bidang_pendidikan}}</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>b.</td>
                        <td>Pendidikan dan Pelatihan</td>
                        <td>:{{$kualifikasi_jabatan->pelatihan}}</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td>c.</td>
                        <td>Pengalaman Kerja</td>
                        <td>:{{$kualifikasi_jabatan->tahun}} Tahun di bidang {{$kualifikasi_jabatan->keterangan}}</td>
                    </tr>
                    @endforeach
                    <tr>
                        <td><b>6.</b></td>
                        <td colspan="2"><b>TUGAS POKOK</b></td>
                        <td></td>
                    </tr>
                </tbody>
            </table>

            <table class="table header-hijau" id="table-header-1">
                <thead>
                    <tr>
                        <th style="width:30px;">No</th>
                        <th style="width:200px;">Uraian Tugas</th>
                        <th style="width:95px;">Hasil Kerja</th>
                        <th style="width:75px;">Jumlah Hasil</th>
                        <th style="width:75px;">Waktu Penyelesaian (Jam)</th>
                        <th style="width:75px;">Waktu Kerja Efektif (Jam)</th>
                        <th style="width:75px;">Pegawai yang dibutuhkan</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($tugas_pokoks as $key => $tugas_pokok)
                    <tr>
                        <td>{{($key+1)}}.</td>
                        <td style="max-width:200px; word-wrap:break-word;">{{$tugas_pokok->uraian_tugas}}</td>
                        <td>{{$tugas_pokok->hasilKerja()->first()->nama_hasil_kerja}}</td>
                        <td style="text-align:right; padding-right:10px;">{{$tugas_pokok->jumlah_hasil}}</td>
                        <td style="text-align:right; padding-right:10px;">{{$tugas_pokok->waktu_selesai}}</td>
                        <td style="text-align:right; padding-right:10px;">1250</td>
                        <td style="text-align:right; padding-right:10px;">{{$tugas_pokok->pegawai_dibutuhkan}}</td>
                    </tr>
                    @endforeach
                    <tr>
                        <td>|</td>
                        <td colspan="5"><b>JUMLAH</b></td>
                        <td style="text-align:right; padding-right:10px;"><b>{{number_format($jumlah_tugas_pokok, 3, '.', ',')}}</b></td>
                    </tr>
                    <tr>
                        <td>|</td>
                        <td colspan="5"><b>PEMBULATAN</b></td>
                        <td style="text-align:right; padding-right:10px;"><b>{{$jumlah_tugas_pokok_dibulatkan}}</b></td>
                    </tr>
                </tbody>
            </table>

            <table class="table">
                <tbody>
                    <tr>
                        <td style="width:30px;"><b>7.</b></td>
                        <td colspan="2"><b>HASIL KERJA</b></td>
                        <td style="width:400px;"></td>
                    </tr>
                </tbody>
            </table>
            
            <table class="table header-hijau" id="table-header-1">
                <thead>
                    <tr>
                        <th style="width:30px;">No</th>
                        <th style="width:200px;">Hasil Kerja</th>
                        <th style="width:425px;">Satuan</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($hasil_kerjas as $key => $hasil_kerja)
                    <tr>
                        <td>{{($key+1)}}.</td>
                        <td style="max-width:200px; word-wrap:break-word;">{{$hasil_kerja->hasil_kerja}}</td>
                        <td style="max-width:425px; word-wrap:break-word;">{{$hasil_kerja->satuan()->first()->nama_satuan}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>

            <table class="table">
                <tbody>
                    <tr>
                        <td style="width:30px;"><b>8.</b></td>
                        <td colspan="2"><b>BAHAN KERJA</b></td>
                        <td style="width:400px;"></td>
                    </tr>
                </tbody>
            </table>
            
            <table class="table header-hijau" id="table-header-1">
                <thead>
                    <tr>
                        <th style="width:30px;">No</th>
                        <th style="width:200px;">Bahan Kerja</th>
                        <th style="width:425px;">Penggunaan Dalam Tugas</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($bahan_kerjas as $key => $bahan_kerja)
                    <tr>
                        <td>{{($key+1)}}.</td>
                        <td style="max-width:200px; word-wrap:break-word;">{{$bahan_kerja->bahan_kerja}}</td>
                        <td style="max-width:425px; word-wrap:break-word;">{{$bahan_kerja->penggunaan}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>

            <table class="table">
                <tbody>
                    <tr>
                        <td style="width:30px;"><b>9.</b></td>
                        <td colspan="2"><b>PERANGKAT KERJA</b></td>
                        <td style="width:400px;"></td>
                    </tr>
                </tbody>
            </table>
            
            <table class="table header-hijau" id="table-header-1">
                <thead>
                    <tr>
                        <th style="width:30px;">No</th>
                        <th style="width:200px;">Perangkat Kerja</th>
                        <th style="width:425px;">Penggunaan Untuk Tugas</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($perangkat_kerjas as $key => $perangkat_kerja)
                    <tr>
                        <td>{{($key+1)}}.</td>
                        <td style="max-width:200px; word-wrap:break-word;">{{$perangkat_kerja->perangkat_kerja}}</td>
                        <td style="max-width:425px; word-wrap:break-word;">{{$perangkat_kerja->penggunaan}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>

            <table class="table">
                <tbody>
                    <tr>
                        <td style="width:30px;"><b>10.</b></td>
                        <td colspan="2"><b>TANGGUNG JAWAB</b></td>
                        <td style="width:400px;"></td>
                    </tr>
                </tbody>
            </table>
            
            <table class="table header-pink" id="table-header-1">
                <thead>
                    <tr>
                        <th style="width:30px;">No</th>
                        <th style="width:625px;">Uraian</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($tanggung_jawabs as $key => $tanggung_jawab)
                    <tr>
                        <td>{{($key+1)}}.</td>
                        <td style="max-width:625px; word-wrap:break-word;">{{$tanggung_jawab->tanggung_jawab}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>

            <table class="table">
                <tbody>
                    <tr>
                        <td style="width:30px;"><b>11.</b></td>
                        <td colspan="2"><b>WEWENANG</b></td>
                        <td style="width:400px;"></td>
                    </tr>
                </tbody>
            </table>
            
            <table class="table header-pink" id="table-header-1">
                <thead>
                    <tr>
                        <th style="width:30px;">No</th>
                        <th style="width:625px;">Uraian</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($wewenangs as $key => $wewenang)
                    <tr>
                        <td>{{($key+1)}}.</td>
                        <td style="max-width:625px; word-wrap:break-word;">{{$wewenang->wewenang}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            
            <table class="table">
                <tbody>
                    <tr>
                        <td style="width:30px;"><b>12.</b></td>
                        <td colspan="2"><b>KORELASI JABATAN</b></td>
                        <td style="width:400px;"></td>
                    </tr>
                </tbody>
            </table>
            
            <table class="table header-pink" id="table-header-1">
                <thead>
                    <tr>
                        <th style="width:30px;">No</th>
                        <th style="width:225px;">Nama Jabatan</th>
                        <th style="width:200px;">Unit Kerja/Instansi</th>
                        <th style="width:200px;">Dalam Hal</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($korelasi_jabs as $key => $korelasi_jab)
                    <tr>
                        <td>{{($key+1)}}.</td>
                        <td style="max-width:225px; word-wrap:break-word;">{{$korelasi_jab->nama_jabatan}}</td>
                        <td style="max-width:200px; word-wrap:break-word;">{{$korelasi_jab->unit_kerja}}</td>
                        <td style="max-width:200px; word-wrap:break-word;">{{$korelasi_jab->dalam_hal}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>

            <table class="table">
                <tbody>
                    <tr>
                        <td style="width:30px;"><b>13.</b></td>
                        <td colspan="2"><b>KONDISI LINGKUNGAN KERJA</b></td>
                        <td style="width:400px;"></td>
                    </tr>
                </tbody>
            </table>
            
            <table class="table header-pink" id="table-header-1">
                <thead>
                    <tr>
                        <th style="width:30px;">No</th>
                        <th style="width:225px;">Aspek</th>
                        <th style="width:400px;">FAKTOR</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($lingkungan_kerjas as $key => $lingkungan_kerja)
                    <tr>
                        <td>{{($key+1)}}.</td>
                        <td style="max-width:225px; word-wrap:break-word;">{{$lingkungan_kerja->nama_aspek}}</td>
                        <td style="max-width:400px; word-wrap:break-word;">{{$lingkungan_kerja->nilai_aspek}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>

            <table class="table">
                <tbody>
                    <tr>
                        <td style="width:30px;"><b>14.</b></td>
                        <td colspan="2"><b>RESIKO BAHAYA</b></td>
                        <td style="width:400px;"></td>
                    </tr>
                </tbody>
            </table>
            
            <table class="table header-pink" id="table-header-1">
                <thead>
                    <tr>
                        <th style="width:30px;">No</th>
                        <th style="width:225px;">NAMA RESIKO</th>
                        <th style="width:400px;">PENYEBAB</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($resiko_bahayas as $key => $resiko)
                    <tr>
                        <td>{{$key+1}}.</td>
                        <td style="max-width:225px; word-wrap:break-word;">{{$resiko->nama_resiko}}</td>
                        <td style="max-width:400px; word-wrap:break-word;">{{$resiko->penyebab}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>

            <table class="table">
                <tbody>
                    <tr>
                        <td style="width:30px;"><b>15.</b></td>
                        <td colspan="2"><b>SYARAT JABATAN</b></td>
                        <td style="width:400px;"></td>
                    </tr>
                </tbody>
            </table>
            
            <table class="table borderless" id="table-header-1">
                <tbody>
                    @php $subsyarat = ''; $ascii = 96; @endphp
                    @foreach($syarat_jabatans as $syarat)
                    @php $sub_syarat = $syarat->point()->first()->nama_sub_syarat; @endphp
                    @if($subsyarat != $sub_syarat)
                    @php $subsyarat = $sub_syarat; $ascii++; @endphp
                    <tr>
                        <td>{{chr($ascii)}}.</td>
                        <td>{{$subsyarat}}</td>
                        <td colspan="2">:</td>
                    </tr>
                    @endif
                    <tr>
                        <td colspan="2"></td>
                        <td>-</td>
                        <td style="max-width:500px; word-wrap:break-word;">{{$syarat->syarat}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>

            <table class="table">
                <tbody>
                    <tr>
                        <td style="width:30px;"><b>16.</b></td>
                        <td colspan="2"><b>PRESTASI KERJA YANG DIHARAPKAN</b></td>
                        <td style="width:400px;"></td>
                    </tr>
                </tbody>
            </table>
            
            <table class="table header-pink" id="table-header-1">
                <thead>
                    <tr>
                        <th style="width:30px;">NO</th>
                        <th style="width:425px;">HASIL KERJA</th>
                        <th style="width:100px;">Waktu Penyelesaian (Jam)</th>
                        <th style="width:100px;">Volume (setahun)</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($prestasi_kerjas as $key => $prestasi_kerja)
                    <tr>
                        <td>{{($key+1)}}.</td>
                        <td style="max-width:425px; word-wrap:break-word;">{{$prestasi_kerja->prestasi}}</td>
                        <td style="max-width:100px; word-wrap:break-word;">{{$prestasi_kerja->waktu_penyelesaian}}</td>
                        <td style="max-width:100px; word-wrap:break-word;">{{$prestasi_kerja->volume}}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>

            <table class="table">
                <tbody>
                    <tr>
                        <td style="width:30px;"><b>17.</b></td>
                        <td style="width:200px;"><b>KELAS JABATAN</b></td>
                        <td colspan="2" style="max-width:425px; word-wrap:break-word;">: {{$skpd_jab->kelas_jab}}</td>
                    </tr>
                </tbody>
            </table>
        </div>
    </body>
</html>