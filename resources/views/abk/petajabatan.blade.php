
@extends('layouts.main')
@section('title' , 'Peta Jabatan')
@section('style')
  <meta name="csrf-token" content="{{ csrf_token() }}">
  <link rel="stylesheet" href="{{asset('orgchart/css/jquery.orgchart.css')}}">
  <link rel="stylesheet" href="{{asset('orgchart/css/style.css')}}">
@endsection
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
          <h5 class="m-0">{{strtoupper($skpd->nama_skpd)}}</h5>
            <ol class="breadcrumb float-sm-left">
            <li class="breadcrumb-item"><a href="/">Anjab ABK</a></li>
              <li class="breadcrumb-item"><a href="/abk">List SKPD</a></li>
              <li class="breadcrumb-item"><a href="/abk/detailskpd/{{$skpd->id}}">List Jabatan</a></li>
              <li class="breadcrumb-item active">Peta Jabatan</li>
            </ol>
          </div><!-- /.col -->
          <div class="col-sm-6">
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Main row -->
        <div class="row">
            <div class="col-md-12 col-sm-12 col-md-12 col-lg-12">
              <div id="chart-container"></div>
            </div>
        </div>
      </div>
    </section>
</div>
@endsection
@section('script')
  <script type="text/javascript" src="{{asset('js/orgchart.js')}}"></script>
  <script type="text/javascript">
    var ds = {!! json_encode($struktur) !!}
    var user = {!! json_encode($user) !!}
    var layout = {!! json_encode($layout) !!}
    OrgChart.templates.myTemplate = Object.assign({}, OrgChart.templates.polina);
    // OrgChart.templates.myTemplate.plus = "+";
    OrgChart.templates.myTemplate.plus = '<circle cx="15" cy="15" r="15" fill="#ffffff" stroke="#aeaeae" stroke-width="1"></circle>'
        + '<text text-anchor="middle" style="font-size: 18px;cursor:pointer;" fill="#757575" x="15" y="22">+</text>';
    // OrgChart.templates.myTemplate.minus = "-";
    OrgChart.templates.myTemplate.minus = '<circle cx="15" cy="15" r="15" fill="#ffffff" stroke="#aeaeae" stroke-width="1"></circle>'
        + '<text text-anchor="middle" style="font-size: 18px;cursor:pointer;" fill="#757575" x="15" y="22">-</text>';
    OrgChart.templates.myTemplate.field_4 = '<text style="font-size: 12px;" fill="#ffffff" x="80" y="70" text-anchor="left">{val}</text>';
    OrgChart.templates.myTemplate.field_5 = '<text style="font-size: 12px;" fill="#ffffff" x="120" y="70" text-anchor="center">{val}</text>';
    OrgChart.templates.myTemplate.field_6 = '<text style="font-size: 12px;" fill="#ffffff" x="105" y="70" text-anchor="left">{val}</text>';
    OrgChart.templates.myTemplate.link = '<path stroke-linejoin="round" stroke="#aeaeae" stroke-width="1px" fill="none" d="{edge}" />';
    if(layout == true) {
      var chart = new OrgChart(document.getElementById("chart-container"), {
        layout: OrgChart.treeRightOffset,
        scaleInitial: OrgChart.match.boundary,
        mouseScrool: OrgChart.action.none,
        collapse: {level:2, allChildren: true},
        template: "myTemplate",
        nodeMouseClick: OrgChart.action.edit,
        nodeBinding: {
            field_0: "name",
            field_1: "title",
            field_2: "id_skpd_jab",
            field_3: "nip",
            field_4: "eselon",
            field_5: "golongan",
            field_6: "strip1",
            img_0: "img"
        },
        nodes: ds
      });
    }
    else {
      var chart = new OrgChart(document.getElementById("chart-container"), {
        scaleInitial: OrgChart.match.boundary,
        mouseScrool: OrgChart.action.none,
        template: "myTemplate",
        nodeMouseClick: OrgChart.action.edit,
        nodeBinding: {
            field_0: "name",
            field_1: "title",
            field_2: "id_skpd_jab",
            field_3: "nip",
            field_4: "eselon",
            field_5: "golongan",
            field_6: "strip1",
            img_0: "img"
        },
        nodes: ds
      });
    }
    
    chart.editUI.on('field', function(sender, args){
      if(args.name != 'name' && args.name != 'nip' && args.name != 'eselon' && args.name != 'golongan') {
        return false;
      }
      else if (args.name == 'name') {
        var id_node = sender.node.id;
        var txt = args.field.querySelector('input');
        if (txt){
            txt.style.color = "red";
            var select = document.createElement('select');
            select.innerHTML = '<option selected="selected" value="">'+args.field.querySelector('input').value+'</option>';
            $.get('/abk/peta-jabatan/get-opsi-pegawai/' + id_node, function(pegawais) {
              $.each(pegawais, function(index,value) {
                select.innerHTML += '<option value="'+value.id+'">'+value.nama+'</option>';
              });
            });
            select.style.width = '100%';
            select.setAttribute('val', '-');
            select.setAttribute('id', 'pilih-peg');
            if(user.boleh_edit_petajabatan == 0) {
              select.setAttribute('disabled', 'disabled');
            }
            select.style.fontSize = '16px';
            select.style.color = 'rgb(122, 122, 122)';
            select.style.paddingTop = '7px';
            select.style.paddingBottom = '7px';
            
            txt.parentNode.appendChild(select);
            txt.parentNode.removeChild(txt);
        }
      }
      else if (args.name = 'nip') {
        var txt = args.field.querySelector('input');
        txt.setAttribute('disabled', 'disabled');
      }
      else if (args.name = 'eselon') {
        var txt = args.field.querySelector('input');
        txt.setAttribute('disabled', 'disabled');
      }
      else if (args.name = 'golongan') {
        var txt = args.field.querySelector('input');
        txt.setAttribute('disabled', 'disabled');
      }
    });
    chart.on('update', function (sender, node) {
      var peg = $('#pilih-peg').val();
      if(peg) {
        $.ajaxSetup({
          headers: {
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          }
        });
        $.ajax({
          type:'POST',
          url:"/abk/peta-jabatan/update/"+node.id+"/"+peg,
          data: node,
          success:function(data){
            node.name = data.nama_peg;
            node.nip = data.nip;
            sender.updateNode(node);
            location.reload();
          }
        });
      }
      return false;
    });
  </script>
@endsection