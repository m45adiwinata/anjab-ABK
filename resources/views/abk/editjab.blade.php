@extends('layouts.main')
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">ANJAB ABK</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="/">Anjab ABK</a></li>
              <li class="breadcrumb-item"><a href="/abk">List SKPD</a></li>
              <li class="breadcrumb-item"><a href="/abk">List Jabatan</a></li>
              <li class="breadcrumb-item active">Tambah Jabatan</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Main row -->
        <div class="row">
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="card card-primary">
                    <!-- form start -->
                    <form method="POST" action="{{route('tambahjab', $skpd->id)}}">
                        @csrf
                        <div class="row">
                            <div class="col-md-8">
                                <div class="card-body">
                                    <div class="form-group">
                                        <label for="jab-atasan">Jabatan Atasan*</label>
                                        <select class="form-control select2" id="jab-atasan" name="id_jab_atasan" style="width: 100%;">
                                            <option value="-" {{$skpd->jabatan()->find($jabatanselected->id)->pivot->id_jab_atasan == 0 ? 'selected="selected"' : ''}}>-</option>
                                            @foreach($jabatans as $key => $jab)
                                            <option value="{{$jab->id}}" {{$skpd->jabatan()->find($jabatanselected->id)->pivot->id_jab_atasan == $jab->id ? 'selected="selected"' : ''}}>
                                            {{$jab->jabatan}}
                                            </option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="jenis-jab">Jenis Jabatan*</label>
                                        <input type="text" class="form-control" id="jenis-jab" name="jenis_jab" autocomplete="off" value="{{$jenis_jab->nama_jenis_jabatan}}" disabled>
                                    </div>
                                    <div class="form-group">
                                        <label for="jab">Jabatan*</label>
                                        <select class="form-control select2" id="jab" name="id_jabatan" style="width: 100%;">
                                            @foreach($jabatan_all as $key => $jab)
                                            <option value="{{$jab->id}}" {{$jabatanselected->id == $jab->id ? 'selected="selected"' : ''}}>
                                            {{$jab->jabatan}}
                                            </option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="kelas-jab">Kelas Jabatan</label>
                                        <input type="text" class="form-control" id="kelas-jab" name="kelas_jab" value="{{$skpd->jabatan()->find($jabatanselected->id)->pivot->kelas_jab}}" autocomplete="off">
                                    </div>
                                    <div class="form-group">
                                        <label for="eselon">Eselon Jabatan</label>
                                        <select class="form-control select2" id="eselon" name="id_eselon" style="width: 100%;">
                                            @foreach($eselons as $key => $eselon)
                                            <option value="{{$eselon->id}}" {{$skpd->jabatan()->find($jabatanselected->id)->pivot->id_eselon == $eselon->id ? 'selected="selected"' : ''}}>
                                            {{$eselon->eselon}}
                                            </option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="pemangku-jab">Jumlah Pemangku Saat Ini</label>
                                        <input type="number" class="form-control" id="pemangku-jab" name="jml_pemangku" autocomplete="off">
                                    </div>
                                    <div class="form-group">
                                        <label for="ikhtisar-jab">Ikhtisar Jabatan</label>
                                        <input type="text" class="form-control" id="ikhtisar-jab" name="ikhtisar" autocomplete="off">
                                    </div>
                                </div>
                                <!-- /.card-body -->
                            </div>
                            <div class="col-md-4">
                                <svg xmlns="http://www.w3.org/2000/svg" width="119.018" height="148.741" viewBox="0 0 119.018 148.741">
                                    <path id="Icon_awesome-file-image" data-name="Icon awesome-file-image" d="M119.018,35.425v1.76H79.345V0h1.878a7.7,7.7,0,0,1,5.26,2.042l30.356,28.453A6.759,6.759,0,0,1,119.018,35.425ZM76.866,46.482a7.238,7.238,0,0,1-7.439-6.972V0H7.439A7.217,7.217,0,0,0,0,6.972v134.8a7.217,7.217,0,0,0,7.439,6.972h104.14a7.217,7.217,0,0,0,7.439-6.972V46.482ZM34.882,51.13c8.217,0,14.877,6.243,14.877,13.944S43.1,79.019,34.882,79.019,20.005,72.776,20.005,65.074,26.666,51.13,34.882,51.13ZM99.35,120.852H20.005l.15-14.085L32.4,95.287a3.71,3.71,0,0,1,5.109.141L49.76,106.907,81.843,76.835a3.9,3.9,0,0,1,5.26,0L99.35,88.315Z"/>
                                </svg>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card-footer text-center">
                                <a href="/abk/detailskpd/{{$skpd->id}}" class="btn btn-info mr-3">Cancel</a>
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection
@section('script')
<script>
    $(document).ready(function() {
        $('#jab').change(function() {
            $.get('/api/get-jenis-jab/' + $(this).val(), function(data) {
                $('#jenis-jab').val(data.nama_jenis_jabatan);
            });
        });
    });
</script>
@endsection