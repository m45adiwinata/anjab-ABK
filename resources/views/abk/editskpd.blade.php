@extends('layouts.main')

@section('title' , 'Edit SKPD')


@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">ANJAB ABK</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="/">Anjab ABK</a></li>
              <li class="breadcrumb-item"><a href="/abk">List SKPD</a></li>
              <li class="breadcrumb-item active">Edit SKPD</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Main row -->
        <div class="row">
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="card card-primary">
                    <!-- form start -->
                    <form method="POST" action="{{route('updateskpd', $skpd->id)}}">
                        @csrf
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card-body">
                                    <div class="form-group">
                                        <label for="kode-skpd">Kode SKPD</label>
                                        <input type="text" class="form-control" id="kode-skpd" name="kode_skpd" autocomplete="off" value="{{$skpd->kode_skpd}}" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="nama-skpd">Nama SKPD</label>
                                        <input type="text" class="form-control" id="nama-skpd" name="nama_skpd" autocomplete="off" value="{{$skpd->nama_skpd}}" required>
                                    </div>
                                </div>
                                <!-- /.card-body -->
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card-footer" style="text-align:end">
                                    <a class="btn btn-info mr-3" href="/opdjab/opd">Cancel</a>
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection
@section('script')
<script>
    $(document).ready(function() {
        
    });
</script>
@endsection