@extends('layouts.main')

@section('title' , 'Tambah Jabatan')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">ANJAB ABK</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="/">Anjab ABK</a></li>
                @if(Auth::user()->role_id == 1) 
              <li class="breadcrumb-item"><a href="/abk">List SKPD</a></li>
                @endif
              <li class="breadcrumb-item"><a href="/abk">List Jabatan</a></li>
              <li class="breadcrumb-item active">Tambah Jabatan</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Main row -->
        <div class="row">
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="card card-primary">
                    <!-- form start -->
                    <form method="POST" action="{{route('tambahjab', $skpd->id)}}" id="form-tambah" name="insertjab">
                        @csrf
                        <input type="hidden" id="lanjut" name="lanjut" value="0">
                        <div class="row">
                            <div class="col-md-8">
                                <div class="card-body">
                                    <div class="form-group required">
                                        <label for="jab-atasan" class="control-label">Jabatan Atasan</label>
                                        <select class="form-control select2" id="jab-atasan" name="id_jab_atasan" style="width: 100%;">
                                            <option selected="selected" value="-">-</option>
                                            @foreach($jabatans as $key => $jab)
                                            <option value="{{$jab->pivot->id}}">
                                            {{$jab->jabatan}}
                                            </option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group required">
                                        <label for="jenis-jab" class="control-label">Jenis Jabatan</label>
                                        <input type="text" class="form-control" id="jenis-jab" name="jenis_jab" autocomplete="off" value="{{$jenis_jab ? $jenis_jab->nama_jenis_jabatan : ''}}" disabled>
                                    </div>
                                    <div class="form-group required">
                                        <label for="jab" class="control-label">Jabatan</label>
                                        <select class="form-control select2" id="jab" name="id_jabatan" style="width: 100%;">
                                            @foreach($jabatan_all as $key => $jab)
                                            @if($key == 0)
                                            <option selected="selected" value="{{$jab->id}}">
                                            @else
                                            <option value="{{$jab->id}}">
                                            @endif 
                                            {{$jab->jabatan}}
                                            </option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="form-group required">
                                        <label for="kelas-jab" class="control-label">Kelas Jabatan</label>
                                        <input type="text" class="form-control" id="kelas-jab" name="kelas_jab" autocomplete="off" required>
                                    </div>
                                    <div class="form-group required">
                                        <label for="eselon" class="control-label">Eselon Jabatan</label>
                                        <select class="form-control select2" id="eselon" name="id_eselon" style="width: 100%;" disabled>
                                            <option></option>
                                            @foreach($eselons as $key => $eselon)
                                            <option value="{{$eselon->id}}">{{$eselon->eselon}}</option>
                                            @endforeach
                                        </select>
                                        <input type="hidden" id="id_eselon" name="id_eselon" value="">
                                    </div>
                                    <div class="form-group required">
                                        <label for="pemangku-jab" class="control-label">Jumlah Pemangku Saat Ini</label>
                                        <input type="number" class="form-control" id="pemangku-jab" name="jml_pemangku" autocomplete="off" required>
                                    </div>
                                    <div class="form-group required">
                                        <label for="ikhtisar-jab" class="control-label">Ikhtisar Jabatan</label>
                                        <textarea type="text" class="form-control" id="ikhtisar-jab" rows="3" name="ikhtisar" autocomplete="off" required></textarea>
                                    </div>
                                </div>
                                <!-- /.card-body -->
                            </div>
                            <div class="col-md-4 hidden-xs hidden-sm" style="display: flex;justify-content: center; align-items: center;">
                                <img src="{{asset('main/dist/img/user-lg.png')}}" style="max-width: 50%;"/>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card-footer" style="text-align: end;">
                                    <a href="/abk/detailskpd/{{$skpd->id}}" class="btn btn-info btn-info-custom mr-3">Cancel</a>
                                    <!-- <button data-toggle="modal" data-target="#modal-xl" type="button" class="btn btn-primary btn-primary-custom">Submit</button> -->
                                    <button  id="add-jab" type="submit" class="btn btn-primary btn-primary-custom">Submit</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
      
      <div class="modal fade" id="modal-xl">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
              <div class="modal-body">
                  <div class="row">
                    <div class="col-md-12">
                      Apakah ingin melanjutkan untuk menginput ABK?
                    </div>
                  </div>
              </div>
              <div class="modal-footer text-right">
                <button type="button" class="btn btn-default btn-info-custom mr-3" style="width:100px; margin-right:30px;" id="btn-tidak">Tidak</button>
                <button class="btn btn-primary btn-primary-custom" type="button" id="btn-ya">Input ABK</button>
              </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection
@section('script')
<script>
    $(document).ready(function() {
        $.get('/api/get-jenis-jab/' + $('#jab').val(), function(data) {
            $('#jenis-jab').val(data.nama_jenis_jabatan);
        });
        $.get('/api/get-eselon-jab/' + $('#jab').val(), function(data) {
            $('#eselon').val(data.id_eselon).trigger('change');
            $('#id_eselon').val(data.id_eselon);
        });
        $('#jab').change(function() {
            $.get('/api/get-jenis-jab/' + $(this).val(), function(data) {
                $('#jenis-jab').val(data.nama_jenis_jabatan);
            });
            $.get('/api/get-eselon-jab/' + $(this).val(), function(data) {
                $('#eselon').val(data.id_eselon).trigger('change');
                $('#id_eselon').val(data.id_eselon);
            });
        });
        $('#add-jab').click(function(){
                $("form[name='insertjab']").validate({
                    rules: {
                        jenis_jab:'required',
                        id_jabatan : 'required',
                        kelas_jab : 'required',
                        id_eselon : 'required',
                        jml_pemangku : 'required',
                        ikhtisar : 'required',

                    },
                    messages: {
                        jenis_jab: "Mohon masukan jenis jabatan!",
                        id_jabatan: "Mohon masukan jabatan!",
                        kelas_jab: "Mohon masukan kelas jabatan!",
                        id_eselon: "Mohon masukan eselon jabatan!",
                        jml_pemangku: "Mohon masukan jumlah pemangku saat ini!",
                        ikhtisar: "Mohon masukan ikhtisar jabatan!",

                        },
                    submitHandler: function(form) {
                        $('#modal-xl').modal('show');

                        $('#modal-xl #btn-tidak').click(function() {
                            form.submit();
                        });
                        $('#modal-xl #btn-ya').click(function() {
                            $('#lanjut').val(1);
                            form.submit();
                        });
                    }
                });
        });
        
    });
</script>
@endsection