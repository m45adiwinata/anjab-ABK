@extends('layouts.main')

@section('title' , 'Detail SKPD')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
          <h5 class="m-0">{{strtoupper($skpd->nama_skpd)}}</h5>
            <ol class="breadcrumb float-sm-left">
              <li class="breadcrumb-item"><a href="/">Anjab ABK</a></li>
              @if(Auth::user()->role_id == 1)
              <li class="breadcrumb-item"><a href="/abk">List SKPD</a></li>
              @endif
              <li class="breadcrumb-item active">List Jabatan</li>
            </ol>
          </div><!-- /.col -->
          <div class="col-sm-6">
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
      <div class="row">
        <div class="col-12 mb-4">
            <a class="btn btn-info mr-2" href="/abk/{{$skpd->id}}/tambah-jabatan"><span class="fas fa-plus" style="margin-right: 5px;"></span>Tambah</a>
            <a href="/abk/detailskpd/{{$skpd->id}}/peta-jabatan" class="btn btn-warning" style="color:white"><span class="fas fa-map" style="margin-right: 5px;"></span>Lihat Peta Jabatan</a>
        </div>
      </div>
      @if(Session::has('success'))
        <div class="alert alert-success" style="padding: 5px 10px;background-color: #d4edda;border-color: #c3e6cb;color: #155724;" role="alert">
          <i class="far fa-check-circle" style="margin-right:5px;"></i>{{ Session::get('success') }}<a href="#" class="close" data-dismiss="alert" aria-label="close"><i class="far fa-times-circle"></i></a>
        </div>
      @endif
        <!-- Main row -->
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">List Jabatan SKPD</h3>
                <div class="card-tools">
                  <div class="input-group input-group-sm" style="width: 150px;">
                    <input type="text" id="table_search" name="table_search" class="form-control float-right" placeholder="Search" value="{{isset($_GET['table_search']) ? $_GET['table_search'] : ''}}">
                    <div class="input-group-append">
                      <button type="submit" class="btn btn-default" id="submit-search">
                        <i class="fas fa-search"></i>
                      </button>
                    </div>
                  </div>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body table-responsive p-0" style="height: 600px;">
                <table class="table table-head-fixed text-nowrap">
                @if(count($jabatans))
                  <thead>
                    <tr>
                      <th style="width:50px;">No</th>
                      <th>Nama Jabatan</th>
                      <th>Jenis Jabatan</th>
                      <th>Jabatan Atasan</th>
                      <th>Kelas<br>Jabatan</th>
                      <th colspan="2">Opsi</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($jabatans as $jabatan)
                    <tr>
                      <td>{{$loop->iteration}}</td>
                      <td>{{substr($jabatan->jabatan,0,40)}}</td>
                      <td>{{$jabatan->jenisJab()->first()->nama_jenis_jabatan}}</td>
                      <td>{{substr($jabatan->jab_atasan ? $jabatan->jab_atasan->jabatan : '-',0,40)}}</td>
                      <td>{{$jabatan->pivot->kelas_jab}}</td>
                      <td style="width:40px;"><a class="btn btn-success btn-success-custom" href="/abk/detailjab/{{$jabatan->pivot->id}}">Detail</a></td>
                      <td style="width:40px;">
                        @if($jabatan->has_child == 0)
                        <a class="btn btn-danger btn-danger-custom" onclick="deletejab.skpd({{$jabatan->pivot->id}}, '{{trim(preg_replace('/\s+/', ' ', $jabatan->jabatan))}}', '{{$route}}/abk/deletejabskpd/');" >Hapus</a>
                        @else
                        <a class="btn btn-muted btn-muted-custom" onclick="deletejabdenied({{$jabatan->pivot->id}})" >Hapus</a>
                        @endif
                      </td>
                    </tr>
                    @endforeach
                  </tbody>
                  @else
                  <div class="noresults-container">
                    <p>List jabatan SKPD kosong</p>
                  </div>
                  @endif
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Modal -->
  <div class="modal fade" id="modaldelete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Hapus Data Jabatan SKPD</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form method="POST" id="action-del" action="">
          <div class="modal-body" id="delete-text-jabatanskpd">
              Anda yakin ingin menghapus?
          </div>
          @csrf
          <input type="hidden" id="id-deletejabskpd" name="id" > 
          <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
            <button type="submit" class="btn btn-primary">Iya, Hapus</button>
          </div>
        </form>
      </div>
    </div>
  </div>

  <!-- Modal -->
  <div class="modal fade" id="modaldeletedenied" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Hapus Data Jabatan SKPD Ditolak!</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <form>
          <div class="modal-body">
              Penghapusan data jabatan <b id="nama_jabatan_denied"></b> di SKPD <b id="nama_skpd_denied"></b>
              ditolak karena jabatan tersebut berupa atasan.
          </div>
        </form>
      </div>
    </div>
  </div>


@endsection

@section('script')
<script>
  var id_skpd = {!! $skpd->id !!};
  function deletejabdenied(id) {
    $.get('/api/get-skpd-jabatan/'+id, function(data) {
      $('#nama_jabatan_denied').html(data.nama_jabatan);
      $('#nama_skpd_denied').html(data.nama_skpd);
      $('#modaldeletedenied').modal('show');
    });
  }
  $(document).ready(function() {
    $("#submit-search").click(function() {
      if($('#table_search').val() == "") {
        window.location.href = '/abk/detailskpd/'+id_skpd;
      }
      else {
        window.location.href = '/abk/detailskpd/'+id_skpd+'?table_search='+$('#table_search').val();
      }
    });
    $('#table_search').keypress(function (e) {
      var key = e.which;
      if(key == 13)  // the enter key code
      {
        if($(this).val() == "") {
          window.location.href = '/abk/detailskpd/'+id_skpd;
        }
        else {
          window.location.href = '/abk/detailskpd/'+id_skpd+'?table_search='+$('#table_search').val();
        }
        return false;  
      }
    });
  });
</script>
@endsection