@extends('layouts.main2')
@section('title')
Uraian Tugas
@endsection
@section('style')
<meta name="csrf-token" content="{{ csrf_token() }}">
<style>
  .main-sidebar {
    background-color: #8D2CE2;
  }
</style>
@endsection
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Analisis Beban Kerja</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item active" id="breadcrumb-dinamis">Tugas Pokok</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          @if($total >= 12)
            <div class="col-12 mb-4">
                <button class="btn btn-primary mr-2 pl-3 pr-3" id="maxtugaspokok"><span class="fas fa-plus"></span>Tambah</button>
            </div>
          @else
            <div class="col-12 mb-4">
                <button class="btn btn-primary mr-2 pl-3 pr-3" data-toggle="modal" data-target="#modal-xl"><span class="fas fa-plus"></span>Tambah</button>
            </div>
          @endif
      </div>
      <div id="alert"></div>
        @if(Session::has('success'))
          <div class="alert alert-success" style="padding: 5px 10px;background-color: #d4edda;border-color: #c3e6cb;color: #155724;" role="alert">
            <i class="far fa-check-circle" style="margin-right:5px;"></i>{{ Session::get('success') }}<a href="#" class="close" data-dismiss="alert" aria-label="close"><i class="far fa-times-circle"></i></a>
          </div>
        @endif
        <!-- Main row -->
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">List Tugas Pokok</h3>
                <div class="card-tools">
                  <div class="input-group input-group-sm" style="width: 150px;">
                    <!-- <input type="text" name="table_search" class="form-control float-right" placeholder="Search">
                    <div class="input-group-append">
                      <button type="submit" class="btn btn-default">
                        <i class="fas fa-search"></i>
                      </button>
                    </div> -->
                  </div>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body table-responsive p-0">
                <table class="table table-head-fixed text-nowrap">
                  <thead>
                    <tr>
                      <th style="width:50px;">No</th>
                      <th style="width:400px;">Uraian Tugas</th>
                      <th>Hasil Kerja</th>
                      <th style="width:100px;" class="text-center">Jumlah<br>Hasil</th>
                      <th style="width:100px;" class="text-center">Waktu<br>Penyelesaian</th>
                      <th style="width:100px;" class="text-center">Pegawai<br>yang<br>Dibutuhkan</th>
                      <th colspan="2">Actions</th>
                    </tr>
                  </thead>
                  <tbody class="main-job" id="main-table-body">
                    @foreach($tugas_pokoks as $key => $tugas_pokok)
                    <tr data-index="{{$tugas_pokok->id}}" data-position="{{$tugas_pokok->urutan}}">
                      <td id="nomor-row">{{$tugas_pokok->urutan}}</td>
                      <td class="uraian-td">{{($tugas_pokok->uraian_tugas)}}</td>
                      <td class="satuan-td">{{$tugas_pokok->hasilkerja()->first()->nama_hasil_kerja}}</td>
                      <td class="text-center">{{$tugas_pokok->jumlah_hasil}}</td>
                      <td class="text-center">{{$tugas_pokok->waktu_selesai}}</td>
                      <td class="text-center">{{$tugas_pokok->pegawai_dibutuhkan}}</td>
                      <td style="width: 30px;"><buttton class="btn btn-success btn-success-custom" style="border-radius: 30px;font-size: 14px;" onclick="updateForm({{$tugas_pokok->id}})" data-toggle="modal" data-target="#modal-xl-2">Edit</button></td>
                      <td style="width:30px;"><buttton class="btn btn-danger btn-danger-custom" style="border-radius: 30px;" onclick="deleteForm({{$tugas_pokok->id}})">Hapus</button></td>
                    </tr>
                    @endforeach
                  </tbody>
                  <tbody>
                    <tr>
                      <td colspan="5">Jumlah</td>
                      <td class="text-center font-weight-bold">{{number_format($skpd_jab->tugasPokok()->sum('pegawai_dibutuhkan'), 2, '.', '')}}</td>
                      <td></td>
                      <td></td>
                    </tr>
                    <tr class="bg-info">
                      <td colspan="5">Dibulatkan Menjadi</td>
                      <td class="text-center font-weight-bold">
                        @if($skpd_jab->tugasPokok()->sum('pegawai_dibutuhkan') > (intval($skpd_jab->tugasPokok()->sum('pegawai_dibutuhkan')) +0.5))
                        {{ceil($skpd_jab->tugasPokok()->sum('pegawai_dibutuhkan'))}}
                        @else
                        {{intval($skpd_jab->tugasPokok()->sum('pegawai_dibutuhkan'))}}
                        @endif
                      </td>
                      <td></td>
                      <td></td>
                    </tr>
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
      <div class="modal fade" id="modal-xl">
        <div class="modal-dialog modal-xl">
          <form method="post" action="{{route('tambahtgspokok', $skpd_jab->id)}}" id="formtambah">
            <div class="modal-content">
              <div class="modal-body">
                <div class="card card-primary">
                  <div class="row">
                    <div class="col-md-12">
                      @csrf
                      <table class="table table-borderless table-head-fixed">
                        <thead>
                          <tr>
                            <th style="width: 42px;">No</th>
                            <th>Uraian Tugas</th>
                            <th>Hasil Kerja<span class="text-danger">*</span></th>
                            <th>Jumlah Hasil<span class="text-danger">*</span></th>
                            <th>Waktu Selesai<span class="text-danger">*</span><span class="label-red" style="display: inline-grid;">Jam</span></th>
                            <th>Waktu Efektif<span class="text-danger">*</span></th>
                            <th style="word-break: break-word;">Pegawai Dibutuhkan<span class="text-danger">*</span></th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>1</td>
                            <td style="width: 42%;">
                              <div class="form-group">
                                <textarea type="text" class="form-control" rows="3" name="ut_1" id="ut_1" autocomplete="off"></textarea>
                              </div>
                            </td>
                            <td>
                              <div class="form-group">
                                <select class="form-control select2" id="hk_1" name="hk_1" style="width: 100%;" required>
                                  @foreach($hasil_kerjas as $key => $hasil_kerja)
                                  @if($key == 0)
                                  <option selected="selected" value="{{$hasil_kerja->id}}">{{$hasil_kerja->nama_hasil_kerja}}</option>
                                  @else
                                  <option value="{{$hasil_kerja->id}}">{{$hasil_kerja->nama_hasil_kerja}}</option>
                                  @endif
                                  @endforeach
                                </select>
                              </div>
                            </td>
                            <td>
                              <div class="form-group">
                                <input type="text" class="form-control" name="jh_1" id="jh_1" autocomplete="off" required>
                              </div>
                            </td>
                            <td>
                              <div class="form-group">
                                <input type="text" class="form-control" name="ws_1" id="ws_1" autocomplete="off" required>
                              </div>
                            </td>
                            <td>
                              <div class="form-group">
                                <input type="text" class="form-control" name="we_1" autocomplete="off" value="1250" disabled>
                              </div>
                            </td>
                            <td>
                              <div class="form-group">
                                <input type="hidden" class="form-control" name="pd_1" id="hpd_1" autocomplete="off">
                                <input type="text" class="form-control" id="pd_1" autocomplete="off" disabled>
                              </div>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                      
                    </div>
                  </div>
                </div>
              </div>
              <div class="modal-footer text-right">
                <button type="button" class="btn btn-default" data-dismiss="modal" style="width:100px; margin-right:30px;">Batal</button>
                <button class="btn btn-primary" type="submit" style="width:100px;">Simpan</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </form>
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->

      <div class="modal fade" id="modal-xl-2">
        <div class="modal-dialog modal-xl">
          <form method="post" action="" id="formupdate">
            <div class="modal-content">
              <div class="modal-body">
                <div class="card card-primary">
                  <div class="row">
                    <div class="col-md-12">
                      @csrf
                      <table class="table table-borderless table-head-fixed">
                        <thead>
                          <tr>
                            <th style="width: 42px;">No</th>
                            <th>Uraian Tugas</th>
                            <th>Hasil Kerja<span class="text-danger">*</span></th>
                            <th>Jumlah Hasil<span class="text-danger">*</span></th>
                            <th>Waktu Selesai<span class="text-danger">*</span> <span class="label-red">Jam</span></th>
                            <th>Waktu Efektif<span class="text-danger">*</span></th>
                            <th style="word-break: break-word;">Pegawai Dibutuhkan<span class="text-danger">*</span></th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>1</td>
                            <td style="width: 42%;">
                              <div class="form-group">
                                <textarea type="text" class="form-control" rows="4" name="ut_2" id="ut_2" autocomplete="off"></textarea>
                              </div>
                            </td>
                            <td>
                              <div class="form-group">
                                <select class="form-control select2" id="hk_2" name="hk_2" style="width: 100%;">
                                  @foreach($hasil_kerjas as $key => $hasil_kerja)
                                  <option value="{{$hasil_kerja->id}}">{{$hasil_kerja->nama_hasil_kerja}}</option>
                                  @endforeach
                                </select>
                              </div>
                            </td>
                            <td>
                              <div class="form-group">
                                <input type="text" class="form-control" name="jh_2" id="jh_2" autocomplete="off">
                              </div>
                            </td>
                            <td>
                              <div class="form-group">
                                <input type="text" class="form-control" name="ws_2" id="ws_2" autocomplete="off">
                              </div>
                            </td>
                            <td>
                              <div class="form-group">
                                <input type="text" class="form-control" name="we_2" autocomplete="off" value="1250" disabled>
                              </div>
                            </td>
                            <td>
                              <div class="form-group">
                                <input type="hidden" class="form-control" name="pd_2" id="hpd_2" autocomplete="off">
                                <input type="text" class="form-control" id="pd_2" autocomplete="off" disabled>
                              </div>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                      
                    </div>
                  </div>
                </div>
              </div>
              <div class="modal-footer text-right">
                <button type="button" class="btn btn-default" data-dismiss="modal" style="width:100px; margin-right:30px;">Batal</button>
                <button class="btn btn-primary" type="submit" style="width:100px;">Simpan</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </form>
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->

      <!-- Modal -->
      <div class="modal fade" id="modaldelete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Hapus Data Tugas Pokok</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <form method="POST" id="action-del" action="{{route('deletetugaspokok', $skpd_jab->id)}}">
              <div class="modal-body" id="delete-text"></div>
              @csrf
              <input type="hidden" id="id-delete" name="id" >
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-primary">Iya, Hapus</button>
              </div>
            </form>
          </div>
        </div>
      </div>
      
      <div class="modal fade" id="modal-xl-3">
        <div class="modal-dialog modal-xl">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Panduan Pengisian</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="card card-primary">
                <div class="row">
                  <div class="col-md-12">
                    {{$panduan->text_panduan}}
                  </div>
                </div>
              </div>
            </div>
            <div class="modal-footer text-right">
              <button id="jangan-tampil-panduan" data-dismiss="modal" class="btn btn-primary">Jangan tampilkan lagi.</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection
@section('script')
<script>

  $("#maxtugaspokok").click(function(){
      $('#alert').html('<div class="alert alert-warning" role="alert"><i class="fa fa-exclamation-circle" style="margin-right:5px;"></i>Telah mencapai maksimum untuk penginputan tugas pokok</div>')
  }); 
  var tampilpanduan = {!! $tampil_panduan !!};
  $(window).on('load', function() {
    if(tampilpanduan == 1) {
      $('#modal-xl-3').modal('show');
    }
  });
  

  var skpd_jab = {!! $skpd_jab !!};
  function deleteForm(id) {
    $.get('/api/get-tugaspokok/'+id, function(data) {
      $('#modaldelete #delete-text').html('Anda yakin ingin menghapus? '+data.uraian_tugas);
      $('#modaldelete #id-delete').val(id);
      $('#modaldelete').modal('show');
    });
  }

  function updateForm(id) {
    $.get('/api/get-tugaspokok/'+id, function(data) {
      $('#ut_2').val(data.uraian_tugas);
      $('#hk_2').val(data.id_hasil_kerja).trigger('change');
      $('#jh_2').val(data.jumlah_hasil);
      $('#ws_2').val(data.waktu_selesai);
      $('#hpd_2').val(data.pegawai_dibutuhkan);
      $('#pd_2').val(data.pegawai_dibutuhkan);
      $('#formupdate').attr('action', '/uraian-tugas/update/'+id);
    });
  }

  function simpanUrutan() {
    var pos = [];
    $('.updated').each(function() {
      pos.push([$(this).attr('data-index'), $(this).attr('data-position')]);
      $(this).children('#nomor-row').html($(this).attr('data-position'));
      $(this).removeClass('updated');
    });
    $.post('/uraian-tugas/'+skpd_jab.id+'/ubah-urutan', {
      '_token' : $('meta[name=csrf-token]').attr('content'),
      pos : pos,
    }).done(function(data) {
    });
  }
  $(document).ready(function() {
      $('#jh_1').keyup(function() {
        if($('#ws_1').val() != '') {
          let pd = $(this).val() * $('#ws_1').val() / 1250;
          $('#pd_1').val(pd.toFixed(2));
          $('#hpd_1').val(pd.toFixed(2));
        }
      });
      $('#ws_1').keyup(function() {
        if($('#jh_1').val() != '') {
          let pd = $(this).val() * $('#jh_1').val() / 1250;
          $('#pd_1').val(pd.toFixed(2));
          $('#hpd_1').val(pd.toFixed(2));
        }
      });
      $('#jh_2').keyup(function() {
        if($('#ws_2').val() != '') {
          let pd = $(this).val() * $('#ws_2').val() / 1250;
          $('#pd_2').val(pd.toFixed(2));
          $('#hpd_2').val(pd.toFixed(2));
        }
      });
      $('#ws_2').keyup(function() {
        if($('#jh_2').val() != '') {
          let pd = $(this).val() * $('#jh_2').val() / 1250;
          $('#pd_2').val(pd.toFixed(2));
          $('#hpd_2').val(pd.toFixed(2));
        }
      });
      $('#main-table-body').sortable({
        update: function( event, ui ) {
          $(this).children().each(function(index) {
            if ($(this).attr('data-position') != (index+1)) {
              $(this).attr('data-position', (index+1)).addClass('updated');
            }
          });
          simpanUrutan();
        }
      });
      $('#jangan-tampil-panduan').click(function() {
        $.get('/skpd-jab/'+skpd_jab.id+'/jangan-tampil-panduan/1', function(data) {
          
        });
      });
  });
</script>
@endsection