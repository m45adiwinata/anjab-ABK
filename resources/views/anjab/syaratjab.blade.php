@extends('layouts.main2')
@section('title')
Syarat Jabatan
@endsection
@section('style')
<style>
  .main-sidebar {
    background-color: #8D2CE2;
  }
</style>
@endsection
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Analisis Beban Kerja</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item active" id="breadcrumb-dinamis">Syarat Jabatan</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
        <div class="col-12 mb-4">
            <button class="btn btn-primary mr-2 pl-3 pr-3" data-toggle="modal" data-target="#modal-xl"><span class="fas fa-plus"></span>Tambah</button>
        </div>
      </div>
        @if(Session::has('success'))
          <div class="alert alert-success" style="padding: 5px 10px;background-color: #d4edda;border-color: #c3e6cb;color: #155724;" role="alert">
            <i class="far fa-check-circle" style="margin-right:5px;"></i>{{ Session::get('success') }}<a href="#" class="close" data-dismiss="alert" aria-label="close"><i class="far fa-times-circle"></i></a>
          </div>
        @endif
        <!-- Main row -->
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">List Syarat Jabatan</h3>
                <div class="card-tools">
                  
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body table-responsive p-0" style="height: 600px;">
                <table class="table table-head-fixed text-nowrap">
                  <thead>
                    <tr>
                      <th style="width:50px;">No</th>
                      <th style="width:150px;">Point</th>
                      <th style="width:300px;">Keterampilan</th>
                      <th colspan="2">Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($syarat_jabatans as $key => $syarat_jabatan)
                    <tr>
                      <td>{{($key+1)}}</td>
                      <td>{{$syarat_jabatan->point()->first()->nama_sub_syarat}}</td>
                      <td>{{$syarat_jabatan->syarat}}</td>
                      <td style="width:100px;"><buttton class="btn btn-success btn-success-custom" style="width:75px;" onclick="updateForm({{$syarat_jabatan->id}}, {{($key+1)}})" data-toggle="modal" data-target="#modal-xl-2">Edit</button></td>
                      <td style="width:100px;"><buttton class="btn btn btn-danger btn-danger-custom" style="width:75px;" onclick="deleteForm({{$syarat_jabatan->id}})">Delete</button></td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->

      <div class="modal fade" id="modal-xl">
        <div class="modal-dialog modal-xl">
          <form method="post" action="{{route('tambahsyaratjabatan', $skpd_jab->id)}}" id="formtambah">
            <div class="modal-content">
              <div class="modal-body">
                <div class="card card-primary">
                  <div class="row">
                    <div class="col-md-12">
                      @csrf
                      <table class="table table-borderless table-head-fixed">
                        <thead>
                          <tr>
                            <th style="width:50px;">No</th>
                            <th style="width:150px;">Point</th>
                            <th style="width:300px;">Keterampilan</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>1</td>
                            <td>
                              <div class="form-group">
                                <select class="form-control select2" id="ij_1" name="ij_1" style="width: 100%;">
                                  @foreach($subsyarats as $key => $subsyarat)
                                  @if($key == 0)
                                  <option selected="selected" value="{{$subsyarat->id}}">{{$subsyarat->nama_sub_syarat}}</option>
                                  @else
                                  <option value="{{$subsyarat->id}}">{{$subsyarat->nama_sub_syarat}}</option>
                                  @endif
                                  @endforeach
                                </select>
                              </div>
                            </td>
                            <td>
                              <div class="form-group">
                                <input type="text" class="form-control" name="kt_1" id="kt_1" autocomplete="off">
                              </div>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
              <div class="modal-footer text-right">
                <button type="button" class="btn btn-default" data-dismiss="modal" style="width:100px; margin-right:30px;">Batal</button>
                <button class="btn btn-primary" type="submit" style="width:100px;">Simpan</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </form>
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->
      
      <div class="modal fade" id="modal-xl-2">
        <div class="modal-dialog modal-xl">
          <form method="post" action="" id="formupdate">
            <div class="modal-content">
              <div class="modal-body">
                <div class="card card-primary">
                  <div class="row">
                    <div class="col-md-12">
                      @csrf
                      <table class="table table-borderless table-head-fixed">
                        <thead>
                          <tr>
                            <th style="width:50px;">No</th>
                            <th style="width:150px;">Point</th>
                            <th style="width:300px;">Keterampilan</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td id="nomor-row">1</td>
                            <td>
                              <div class="form-group">
                                <select class="form-control select2" id="ij_2" name="ij_2" style="width: 100%;">
                                  @foreach($subsyarats as $key => $subsyarat)
                                  @if($key == 0)
                                  <option selected="selected" value="{{$subsyarat->id}}">{{$subsyarat->nama_sub_syarat}}</option>
                                  @else
                                  <option value="{{$subsyarat->id}}">{{$subsyarat->nama_sub_syarat}}</option>
                                  @endif
                                  @endforeach
                                </select>
                              </div>
                            </td>
                            <td>
                              <div class="form-group">
                                <input type="text" class="form-control" name="kt_2" id="kt_2" autocomplete="off">
                              </div>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
              <div class="modal-footer text-right">
                <button type="button" class="btn btn-default" data-dismiss="modal" style="width:100px; margin-right:30px;">Batal</button>
                <button class="btn btn-primary" type="submit" style="width:100px;">Simpan</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </form>
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->

      <!-- Modal -->
      <div class="modal fade" id="modaldelete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Hapus Data Syarat Jabatan</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <form method="POST" id="action-del" action="{{route('deletesyaratjabatan', $skpd_jab->id)}}">
              <div class="modal-body" id="delete-text"></div>
              @csrf
              <input type="hidden" id="id-delete" name="id" >
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-primary">Iya, Hapus</button>
              </div>
            </form>
          </div>
        </div>
      </div>

      <div class="modal fade" id="modal-xl-3">
        <div class="modal-dialog modal-xl">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Panduan Pengisian</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="card card-primary">
                <div class="row">
                  <div class="col-md-12">
                    {{$panduan->text_panduan}}
                  </div>
                </div>
              </div>
            </div>
            <div class="modal-footer text-right">
              <button id="jangan-tampil-panduan" data-dismiss="modal" class="btn btn-primary">Jangan tampilkan lagi.</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection
@section('script')
<script>
  var tampilpanduan = {!! $tampil_panduan !!};
  $(window).on('load', function() {
    if(tampilpanduan == 1) {
      $('#modal-xl-3').modal('show');
    }
  });
  var skpd_jab = {!! $skpd_jab !!};
  function updateForm(id, nomor_row) {
    $.get('/api/get-syaratjabatan/'+id, function(data) {
      $('#nomor-row').html(nomor_row);
      $('#ij_2').val(data.id_sub_syarat).trigger('change');
      $('#kt_2').val(data.syarat);
      $('#formupdate').attr('action', '/syarat-jabatan/update/'+id);
    });
  }
  function deleteForm(id) {
    $.get('/api/get-syaratjabatan/'+id, function(data) {
      $('#modaldelete #delete-text').html('Anda yakin ingin menghapus? '+data.syarat);
      $('#modaldelete #id-delete').val(id);
      $('#modaldelete').modal('show');
    });
  }
  $(document).ready(function() {
    $('#jangan-tampil-panduan').click(function() {
      $.get('/skpd-jab/'+skpd_jab.id+'/jangan-tampil-panduan/10', function(data) {
      });
    });
  });
</script>
@endsection