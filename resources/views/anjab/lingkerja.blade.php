@extends('layouts.main2')
@section('title')
Lingkungan Kerja
@endsection
@section('style')
<meta name="csrf-token" content="{{ csrf_token() }}">
<style>
  .main-sidebar {
    background-color: #8D2CE2;
  }
</style>
@endsection
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Analisis Beban Kerja</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item active" id="breadcrumb-dinamis">Lingkungan Kerja</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
        <div class="col-12 mb-4">
            <button class="btn btn-primary mr-2 pl-3 pr-3" data-toggle="modal" data-target="#modal-xl"><span class="fas fa-plus"></span>Tambah</button>
        </div>
      </div>
        @if(Session::has('success'))
          <div class="alert alert-success" style="padding: 5px 10px;background-color: #d4edda;border-color: #c3e6cb;color: #155724;" role="alert">
            <i class="far fa-check-circle" style="margin-right:5px;"></i>{{ Session::get('success') }}<a href="#" class="close" data-dismiss="alert" aria-label="close"><i class="far fa-times-circle"></i></a>
          </div>
        @endif
        <!-- Main row -->
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">List Lingkungan Kerja</h3>
                <div class="card-tools">
                  
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body table-responsive p-0" style="height: 600px;">
                <table class="table table-head-fixed text-nowrap">
                  <thead>
                    <tr>
                      <th style="width:50px;">No</th>
                      <th style="width:300px;">Aspek</th>
                      <th>Faktor</th>
                      <th>Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    @php $nomor = 1; @endphp
                    @foreach($ling_kerjas as $key => $ling_kerja)
                    @if($ling_kerja->selected == 1)
                    <tr>
                      <td>{{($nomor)}}</td>
                      <td>{{$ling_kerja->aspek->nama_aspek}}</td>
                      <td>
                        <div class="form-group clearfix">
                          @foreach($ling_kerja->aspek()->first()->faktor()->get() as $keyf => $faktor)
                          <div class="icheck-primary d-inline mr-5">
                            <input type="radio" id="radioPrimary_{{$ling_kerja->id}}_{{$faktor->id}}" name="r_{{$ling_kerja->id}}" onclick="beriNilai({{$ling_kerja->id}}, {{$faktor->id}})"{{$ling_kerja->id_faktor == $faktor->id ? ' checked':''}}>
                            <label for="radioPrimary_{{$ling_kerja->id}}_{{$faktor->id}}" style="font-weight:normal;">
                              {{$faktor->nama_faktor}}
                            </label>
                          </div>
                          @endforeach
                        </div>
                      </td>
                      <td style="width:100px;"><buttton class="btn btn-danger btn-danger-custom" style="width:75px;" onclick="deleteForm({{$ling_kerja->id}})">Delete</button></td>
                    </tr>
                    @php $nomor++; @endphp
                    @endif
                    @endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->

      <div class="modal fade" id="modal-xl">
        <div class="modal-dialog modal-xl">
          <form method="post" action="{{route('tambahlingkungankerja', $skpd_jab->id)}}" id="formtambah">
            @csrf
            <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title">Pilih Aspek Lingkungan Kerja</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <div class="card card-primary">
                  <div class="row m-4">
                    @foreach($ling_kerjas as $key => $ling_kerja)
                    <div class="col-sm-4">
                      <div class="form-group clearfix">
                        <div class="icheck-primary d-inline">
                          <input type="checkbox" name="cb_{{$ling_kerja->id}}" id="checkboxPrimary{{$ling_kerja->id}}"{{$ling_kerja->selected == 1 ? ' checked':''}}>
                          <label for="checkboxPrimary{{$ling_kerja->id}}">
                            {{$ling_kerja->aspek->nama_aspek}}
                          </label>
                        </div>
                      </div>
                    </div>
                    @endforeach
                  </div>
                  <div class="row m-4">
                    <div class="col-sm-8">
                      <!-- we are adding the accordion ID so Bootstrap's collapse plugin detects it -->
                      <div id="accordion">
                        <div class="card card-primary">
                          <div class="card-header">
                            <h4 class="card-title w-100">
                              <a class="d-block w-100" data-toggle="collapse" href="#collapseOne">
                                Catatan
                              </a>
                            </h4>
                          </div>
                          <div id="collapseOne" class="collapse show" data-parent="#accordion">
                            <div class="card-body">
                              Pilih/centang pada pilihan yang ada untuk dapat mengisi data kondisi lingkungan kerja
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="modal-footer text-right">
                <button type="button" class="btn btn-default" data-dismiss="modal" style="width:100px; margin-right:30px;">Batal</button>
                <button class="btn btn-primary" type="submit" style="width:100px;">Simpan</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </form>
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->

      <!-- Modal -->
      <div class="modal fade" id="modaldelete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Hapus Data Hasil Kerja</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <form method="POST" id="action-del" action="{{route('deletelingkungankerja', $skpd_jab->id)}}">
              <div class="modal-body" id="delete-text"></div>
              @csrf
              <input type="hidden" id="id-delete" name="id" >
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-primary">Iya, Hapus</button>
              </div>
            </form>
          </div>
        </div>
      </div>
      
      <div class="modal fade" id="modal-xl-3">
        <div class="modal-dialog modal-xl">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Panduan Pengisian</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="card card-primary">
                <div class="row">
                  <div class="col-md-12">
                    {{$panduan->text_panduan}}
                  </div>
                </div>
              </div>
            </div>
            <div class="modal-footer text-right">
              <button id="jangan-tampil-panduan" data-dismiss="modal" class="btn btn-primary">Jangan tampilkan lagi.</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection
@section('script')
<script>
  var tampilpanduan = {!! $tampil_panduan !!};
  $(window).on('load', function() {
    if(tampilpanduan == 1) {
      $('#modal-xl-3').modal('show');
    }
  });
  var skpd_jab = {!! $skpd_jab !!};
  function beriNilai(id_ling_kerja, id_faktor) {
    $.post('/lingkungan-kerja/update/'+skpd_jab.id, {
      '_token' : $('meta[name=csrf-token]').attr('content'),
      id_ling_kerja : id_ling_kerja,
      id_faktor : id_faktor
    }).done(function(data) {
      // console.log(data);
    });
  }
  function deleteForm(id) {
    $.get('/api/get-lingkungankerja/'+id, function(data) {
      $('#modaldelete #delete-text').html('Anda yakin ingin menghapus? '+data.nama_aspek);
      $('#modaldelete #id-delete').val(id);
      $('#modaldelete').modal('show');
    });
  }
  $(document).ready(function() {
    $('#jangan-tampil-panduan').click(function() {
      $.get('/skpd-jab/'+skpd_jab.id+'/jangan-tampil-panduan/8', function(data) {
      });
    });
  });
</script>
@endsection