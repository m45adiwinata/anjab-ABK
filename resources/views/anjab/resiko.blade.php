@extends('layouts.main2')
@section('title')
Resiko Bahaya
@endsection
@section('style')
<style>
  .main-sidebar {
    background-color: #8D2CE2;
  }
</style>
@endsection
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Analisis Beban Kerja</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item active" id="breadcrumb-dinamis">Resiko Bahaya</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
        <div class="col-12 mb-4">
            <button class="btn btn-primary mr-2 pl-3 pr-3" data-toggle="modal" data-target="#modal-xl"><span class="fas fa-plus"></span>Tambah</button>
        </div>
      </div>
        @if(Session::has('success'))
          <div class="alert alert-success" style="padding: 5px 10px;background-color: #d4edda;border-color: #c3e6cb;color: #155724;" role="alert">
            <i class="far fa-check-circle" style="margin-right:5px;"></i>{{ Session::get('success') }}<a href="#" class="close" data-dismiss="alert" aria-label="close"><i class="far fa-times-circle"></i></a>
          </div>
        @endif
        <!-- Main row -->
        <div class="row">
          <div class="col-12">
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">List Resiko Bahaya</h3>
                <div class="card-tools">
                  
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body table-responsive p-0" style="height: 600px;">
                <table class="table table-head-fixed text-nowrap">
                  <thead>
                    <tr>
                      <th style="width:50px;">No</th>
                      <th style="width:150px;">Nama Resiko</th>
                      <th style="width:300px;">Penyebab</th>
                      <th colspan="2">Actions</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($resikos as $key => $resiko)
                    <tr>
                      <td>{{($key+1)}}</td>
                      <td>{{$resiko->nama_resiko}}</td>
                      <td>{{$resiko->penyebab}}</td>
                      <td style="width:100px;"><buttton class="btn btn-success btn-success-custom" style="width:75px;" onclick="updateForm({{$resiko->id}}, {{$key+1}})" data-toggle="modal" data-target="#modal-xl-2">Edit</button></td>
                      <td style="width:100px;"><buttton class="btn btn btn-danger btn-danger-custom" style="width:75px;" onclick="deleteForm({{$resiko->id}})">Delete</button></td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->

      <div class="modal fade" id="modal-xl">
        <div class="modal-dialog modal-xl">
          <form method="post" action="{{route('tambahresikobahaya', $skpd_jab->id)}}" id="formtambah">
            <div class="modal-content">
              <div class="modal-body">
                <div class="card card-primary">
                  <div class="row">
                    <div class="col-md-12">
                      @csrf
                      <table class="table table-borderless table-head-fixed">
                        <thead>
                          <tr>
                            <th style="width:50px;">No</th>
                            <th style="width:150px;">Nama Resiko</th>
                            <th style="width:300px;">Penyebab</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>1</td>
                            <td>
                                <div class="form-group">
                                  <input type="text" class="form-control" name="nr_1" id="nr_1" autocomplete="off">
                                </div>
                            </td>
                            <td>
                              <div class="form-group">
                                <input type="text" class="form-control" name="py_1" id="py_1" autocomplete="off">
                              </div>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
              <div class="modal-footer text-right">
                <button type="button" class="btn btn-default" data-dismiss="modal" style="width:100px; margin-right:30px;">Batal</button>
                <button class="btn btn-primary" type="submit" style="width:100px;">Simpan</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </form>
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->
      
      <div class="modal fade" id="modal-xl-2">
        <div class="modal-dialog modal-xl">
          <form method="post" action="{{route('updateresikobahaya')}}" id="formupdate">
            <div class="modal-content">
              <div class="modal-body">
                <div class="card card-primary">
                  <div class="row">
                    <div class="col-md-12">
                      @csrf
                      <input type="hidden" id="id-update" name="id">
                      <table class="table table-borderless table-head-fixed">
                        <thead>
                          <tr>
                            <th style="width:50px;">No</th>
                            <th style="width:150px;">Nama Resiko</th>
                            <th style="width:300px;">Penyebab</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td id="nomor-row">1</td>
                            <td>
                              <div class="form-group">
                                <input type="text" class="form-control" name="nr_2" id="nr_2" autocomplete="off">
                              </div>
                            </td>
                            <td>
                              <div class="form-group">
                                <input type="text" class="form-control" name="py_2" id="py_2" autocomplete="off">
                              </div>
                            </td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                  </div>
                </div>
              </div>
              <div class="modal-footer text-right">
                <button type="button" class="btn btn-default" data-dismiss="modal" style="width:100px; margin-right:30px;">Batal</button>
                <button class="btn btn-primary" type="submit" style="width:100px;">Simpan</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </form>
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->

      <!-- Modal -->
      <div class="modal fade" id="modaldelete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Hapus Data Resiko Bahaya</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <form method="POST" id="action-del" action="{{route('deleteresikobahaya')}}">
              <div class="modal-body" id="delete-text"></div>
              @csrf
              <input type="hidden" id="id-delete" name="id" >
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-primary">Iya, Hapus</button>
              </div>
            </form>
          </div>
        </div>
      </div>

      <div class="modal fade" id="modal-xl-3">
        <div class="modal-dialog modal-xl">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Panduan Pengisian</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <div class="card card-primary">
                <div class="row">
                  <div class="col-md-12">
                    {{$panduan->text_panduan}}
                  </div>
                </div>
              </div>
            </div>
            <div class="modal-footer text-right">
              <button id="jangan-tampil-panduan" data-dismiss="modal" class="btn btn-primary">Jangan tampilkan lagi.</button>
            </div>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>
      <!-- /.modal -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
@endsection
@section('script')
<script>
    var tampilpanduan = {!! $tampil_panduan !!};
  $(window).on('load', function() {
    if(tampilpanduan == 1) {
      $('#modal-xl-3').modal('show');
    }
  });
  var skpd_jab = {!! $skpd_jab !!};
  function updateForm(id, nomor_row) {
    $.get('/api/get-resikobahaya/'+id, function(data) {
      $('#nomor-row').html(nomor_row);
      $('#nr_2').val(data.nama_resiko);
      $('#py_2').val(data.penyebab);
      $('#id-update').val(id);
    });
  }
  function deleteForm(id) {
    $.get('/api/get-resikobahaya/'+id, function(data) {
      $('#modaldelete #delete-text').html('Anda yakin ingin menghapus? '+data.nama_resiko);
      $('#modaldelete #id-delete').val(id);
      $('#modaldelete').modal('show');
    });
  }
  $(document).ready(function() {
    $('#jangan-tampil-panduan').click(function() {
      $.get('/skpd-jab/'+skpd_jab.id+'/jangan-tampil-panduan/12', function(data) {
      });
    });
  });
</script>
@endsection