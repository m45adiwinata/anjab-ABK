<!-- Modal Create -->
<div class="modal fade" id="modalcreateakun" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">User Baru</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="card-body">
          <form action="{{route('create-akun')}}" class="form" method="POST">
            @csrf
            <div class="form-group">
              <label for="nama">Nama</label>
              <input type="text" class="form-control" id="nama" name="nama" placeholder="Nama" autocomplete="off">
            </div>
            <div class="form-group">
              <label for="username">Username</label>
              <input type="text" class="form-control" id="username" name="username" placeholder="Username" autocomplete="off">
            </div>
            <div class="form-group">
              <label for="password">Password</label>
              <input type="password" class="form-control" id="password" name="password" placeholder="Password">
            </div>
            <div class="form-group">
              <label>Role</label>
              <select class="form-control select2" name="role_id" style="width: 100%;">
                @foreach($roles as $key => $role)
                @if($key==0)
                <option value="{{$role->id}}" selected="selected">{{$role->name}}</option>
                @else
                <option value="{{$role->id}}">{{$role->name}}</option>
                @endif
                @endforeach
              </select>
            </div>
            <div class="form-group">
              <label>SKPD</label>
              <select class="form-control select2" id="id_skpd_akun" name="id_skpd" style="width: 100%;">
                <option value="" selected="selected">-</option>
                @foreach($skpd_all as $key => $skpd)
                <option value="{{$skpd->id}}">{{$skpd->nama_skpd}}</option>
                @endforeach
              </select>
            </div>
            <!-- <div class="form-group">
              <label>Pegawai</label>
              <select class="form-control select2" id="id_pegawai_skpd" name="id_pegawai" style="width: 100%;">
              </select>
            </div> -->
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
            <button type="submit" class="btn btn-primary" type="submit">Simpan</button>
          </form>
        </div>
        <!-- /.card-body -->
      </div>
    </div>
  </div>
</div>
<!-- /.Modal -->

<!-- Modal Update -->
<div class="modal fade" id="modalupdate" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">User Update</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="card-body">
          <form action="" class="form" method="POST" id="form-update-akun">
            @csrf
            <div class="form-group">
              <label for="nama2">Nama</label>
              <input type="text" class="form-control" id="nama2" name="nama" placeholder="Nama" autocomplete="off">
            </div>
            <div class="form-group">
              <label for="username2">Username</label>
              <input type="text" class="form-control" id="username2" name="username" placeholder="Username" autocomplete="off">
            </div>
            <div class="form-group">
              <label for="password2">Password</label>
              <input type="password" class="form-control" id="password2" name="password" placeholder="Password | Tidak perlu diubah jika tidak diganti">
            </div>
            <div class="form-group">
              <label>Role</label>
              <select class="form-control select2" name="role_id" id="role_id2" style="width: 100%;">
                @foreach($roles as $key => $role)
                @if($key==0)
                <option value="{{$role->id}}" selected="selected">{{$role->name}}</option>
                @else
                <option value="{{$role->id}}">{{$role->name}}</option>
                @endif
                @endforeach
              </select>
            </div>
            <div class="form-group">
              <label>SKPD</label>
              <select class="form-control select2" id="id_skpd_akun2" name="id_skpd" style="width: 100%;">
                <option value="" selected="selected">-</option>
                @foreach($skpd_all as $key => $skpd)
                <option value="{{$skpd->id}}">{{$skpd->nama_skpd}}</option>
                @endforeach
              </select>
            </div>
            <!-- <div class="form-group">
              <label>Pegawai</label>
              <select class="form-control select2" id="id_pegawai_skpd2" name="id_pegawai" style="width: 100%;">
              </select>
            </div> -->
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
            <button type="submit" class="btn btn-primary" type="submit">Simpan</button>
          </form>
        </div>
        <!-- /.card-body -->
      </div>
    </div>
  </div>
</div>
<!-- /.Modal -->

<!-- Modal Delete -->
<div class="modal fade" id="modaldelete" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Anda yakin ingin menghapus user ini?</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="card-body">
          <form action="" id="form-delete-akun" method="POST">
            @csrf
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
            <button type="submit" class="btn btn-primary" type="submit">Yakin</button>
          </form>
        </div>
        <!-- /.card-body -->
      </div>
    </div>
  </div>
</div>
<!-- /.Modal -->