<!-- Modal Update -->
<div class="modal fade" id="modalfilterskpd" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Filter SKPD</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="card-body">
          <form action="/master" class="form" method="GET">
            <input type="hidden" value="" id="hidden_search_opdjab">
            <div class="form-group">
              <label>Nama SKPD</label>
              <select class="select2" multiple="multiple" data-placeholder="Pilih SKPD (bisa lebih dari 1)" style="width: 100%;" id="select_filter_skpd">
                @foreach($skpd_all as $skpd)
                  <option value="{{$skpd->id}}">{{$skpd->nama_skpd}}</option>
                @endforeach
              </select>
            </div>
            <input type="hidden" name="filter_skpd" id="selected">
            <a href="/master#opdjab" class="btn btn-info">Reset</a>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
            <button type="submit" class="btn btn-primary" type="submit">Simpan</button>
          </form>
        </div>
        <!-- /.card-body -->
      </div>
    </div>
  </div>
</div>
<!-- /.Modal -->