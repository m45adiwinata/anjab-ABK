@extends('layouts.main')

@section('title' , 'Master')
@section('style')
<meta name="csrf-token" content="{{ csrf_token() }}">
@endsection
@section('content')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h5 class="m-0">MASTER</h5>
            <ol class="breadcrumb float-sm-left">
              <li class="breadcrumb-item active">All Menu</li>
            </ol>
          </div><!-- /.col -->
          <div class="col-sm-6">
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <ul class="nav nav-tabs" role="tablist">
          <li class="nav-item">
            <a class="nav-link active" href="#user" role="tab" data-toggle="tab">User</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#abk" role="tab" data-toggle="tab">ABK</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="#pegawai" role="tab" data-toggle="tab">Pegawai</a>
          </li>
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
          <div role="tabpanel" class="tab-pane fade in active" style="opacity:1" id="user">
            <div class="row">
              <div class="col-lg-12">
                <div class="card">
                  <div class="card-header border-0">
                    <div class="card-tools">
                      <a href="#" class="btn btn-tool btn-sm" data-toggle="modal" data-target="#modalcreateakun">
                        <i class="fas fa-user-plus"></i> Tambah User
                      </a>
                    </div>
                  </div>
                  <div class="card-body table-responsive p-0">
                    <table class="table table-striped table-valign-middle">
                      <thead>
                        <tr>
                          <th>Nama</th>
                          <th>Username</th>
                          <th>Password</th>
                          <th>Role</th>
                          <th>Status</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($users as $user)
                        <tr>
                          <td>{{$user->name}}</td>
                          <td>{{$user->username}}</td>
                          <td>{{$user->password}}</td>
                          <td>{{$user->role()->first()->name}}</td>
                          <td><input type="checkbox" name="my-checkbox" onclick="toggleActiveAkun({{$user->id}})"{{$user->status == 1 ? ' checked' : ''}} data-bootstrap-switch></td>
                          <td>
                            <button class="btn" data-toggle="modal" data-target="#modalupdate" onclick="clickUpdateUser({{$user->id}})"><i class="fas fa-edit"></i></button>
                            <button class="btn" data-toggle="modal" data-target="#modaldelete" onclick="clickDeleteUser({{$user->id}})"><i class="far fa-trash-alt"></i></button>
                          </td>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
                <!-- /.card -->
              </div>
            </div>
          </div>
          <div role="tabpanel" class="tab-pane fade" id="abk">
            <div class="row">
              <div class="col-lg-12">
                <div class="card">
                  <div class="card-header border-0">
                    <div class="card-tools">
                      
                    </div>
                  </div>
                  <div class="card-body table-responsive p-0">
                    <table class="table table-striped table-valign-middle">
                      <tbody>
                        <tr>
                          <td><a href="/master/hasil-uraian-kerja">1. Hasil Kerja</a></td>
                        </tr>
                        <tr>
                          <td><a href="/master/satuan">2. Satuan</a></td>
                        </tr>
                        <tr>
                          <td><a href="/master/aspek-lingkungan-kerja">3. Aspek Lingkungan Kerja</a></td>
                        </tr>
                        <tr>
                          <td><a href="/master/syarat">4. Syarat Jabatan</a></td>
                        </tr>
                        <tr>
                          <td><a href="/master/pendidikan">5. Klasifikasi Pendidikan</a></td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
                <!-- /.card -->
                <hr style="margin-top:20px">
                <div class="container-fluid">
                  <h5>Panduan</h5>
                    <div class="row">
                        <div class="col-xs-12 xol-sm-3 col-md-3 col-lg-2" style="margin-bottom: 1rem;">
                            <div class="list-group" id="list-tab" role="tablist">
                              <a  class="list-group-item list-group-item-action active" id="list-tugas-list" data-toggle="list" href="#list-tugas" role="tab" aria-controls="tugas">Tugas Pokok</a>
                              <a  class="list-group-item list-group-item-action" id="list-hasil-list" data-toggle="list" href="#list-hasil" role="tab" aria-controls="hasil">Hasil Kerja</a>
                              <a  class="list-group-item list-group-item-action" id="list-bahan-list" data-toggle="list" href="#list-bahan" role="tab" aria-controls="bahan">Bahan Kerja</a>
                              <a  class="list-group-item list-group-item-action" id="list-tanggung-list" data-toggle="list" href="#list-tanggung" role="tab" aria-controls="tanggung">Tanggung Jawab</a>
                              <a  class="list-group-item list-group-item-action" id="list-perangkat-list" data-toggle="list" href="#list-perangkat" role="tab" aria-controls="perangkat">Perangkat Kerja</a>
                              <a  class="list-group-item list-group-item-action" id="list-wewenang-list" data-toggle="list" href="#list-wewenang" role="tab" aria-controls="wewenang">Wewenang</a>
                              <a  class="list-group-item list-group-item-action" id="list-korelasi-list" data-toggle="list" href="#list-korelasi" role="tab" aria-controls="korelasi">Korelasi Jabatan</a>
                              <a  class="list-group-item list-group-item-action" id="list-lingkungan-list" data-toggle="list" href="#list-lingkungan" role="tab" aria-controls="lingkungan">Lingkungan Kerja</a>
                              <a  class="list-group-item list-group-item-action" id="list-prestasi-list" data-toggle="list" href="#list-prestasi" role="tab" aria-controls="prestasi">Prestasi Kerja</a>
                              <a  class="list-group-item list-group-item-action" id="list-syarat-list" data-toggle="list" href="#list-syarat" role="tab" aria-controls="syarat">Syarat Jabatan</a>
                              <a  class="list-group-item list-group-item-action" id="list-kualifikasi-list" data-toggle="list" href="#list-kualifikasi" role="tab" aria-controls="kualifikasi">Kualifikasi Jabatan</a>
                              <a  class="list-group-item list-group-item-action" id="list-resiko-list" data-toggle="list" href="#list-resiko" role="tab" aria-controls="resiko">Resiko Bahaya</a>
                            </div>
                        </div>
                        <div class="col-xs-12 xol-sm-6 col-md-6 col-lg-6" id="sideright-master-abk">
                              <div class="tab-content" id="nav-tabContent">
                                  <!-- bodylist -->
                                  <div class="tab-pane fade show active" id="list-tugas" role="tabpanel" aria-labelledby="list-tugas-list">
                                      <div id="message-update1"></div>
                                      <div class="form-group">
                                        <label for="idtugaspokok">Panduan Tugas Pokok</label>
                                        <textarea class="form-control" id="text-tugas-pokok" rows="5">@if(!empty($panduans[0])){{$panduans[0]->text_panduan}}@endif</textarea>
                                      </div>
                                      <div class="text-end">
                                          <button class="btn btn-primary" id="btn-tugaspokok" type="submit" style="width:100px;">Simpan</button>
                                      </div>
                                  </div>
                                  <div class="tab-pane fade" id="list-hasil" role="tabpanel" aria-labelledby="list-hasil-list">
                                      <div id="message-update2"></div>
                                      <div class="form-group">
                                        <label for="idhasilkerja">Panduan Hasil Kerja</label>
                                        <textarea class="form-control" id="text-hasil-panduan" rows="3">@if(!empty($panduans[1])){{$panduans[1]->text_panduan}}@endif</textarea>
                                      </div>
                                      <div class="text-end">
                                          <button class="btn btn-primary" id="btn-hasilkerja" type="button" data-loading-text="<i class='fa fa-spinner fa-spin '></i> Processing Data" style="width:100px;">Simpan</button>
                                      </div>
                                  </div>
                                  <div class="tab-pane fade" id="list-bahan" role="tabpanel" aria-labelledby="list-bahan-list">
                                      <div id="message-update3"></div>
                                      <div class="form-group">
                                        <label for="idbahankerja">Panduan Bahan Kerja</label>
                                        <textarea class="form-control" id="text-bahan-kerja" rows="3">@if(!empty($panduans[2])){{$panduans[2]->text_panduan}}@endif</textarea>
                                      </div>
                                      <div class="text-end">
                                        <button class="btn btn-primary" id="btn-bahankerja" type="submit" style="width:100px;">Simpan</button>
                                      </div>
                                  </div>
                                  <div class="tab-pane fade" id="list-tanggung" role="tabpanel" aria-labelledby="list-tanggung-list">
                                      <div id="message-update4"></div>
                                      <div class="form-group">
                                        <label for="idtanggungjawab">Panduan Tanggung Jawab</label>
                                        <textarea class="form-control" id="text-tanggung-jawab" rows="3">@if(!empty($panduans[3])){{$panduans[3]->text_panduan}}@endif</textarea>
                                      </div>
                                      <div class="text-end">
                                          <button class="btn btn-primary" id="btn-tanggungjawab" type="submit" style="width:100px;">Simpan</button>
                                      </div>
                                  </div>
                                  <div class="tab-pane fade" id="list-perangkat" role="tabpanel" aria-labelledby="list-perangkat-list">
                                      <div id="message-update5"></div>
                                      <div class="form-group">
                                        <label for="idperangkatkerja">Panduan Perangkat Kerja</label>
                                        <textarea class="form-control" id="text-perangkat-kerja" rows="3">@if(!empty($panduans[4])){{$panduans[4]->text_panduan}}@endif</textarea>
                                      </div>
                                      <div class="text-end">
                                          <button class="btn btn-primary" id="btn-perangkatkerja" type="submit" style="width:100px;">Simpan</button>
                                      </div>
                                  </div>
                                  <div class="tab-pane fade" id="list-wewenang" role="tabpanel" aria-labelledby="list-wewenang-list">
                                      <div id="message-update6"></div>
                                      <div class="form-group">
                                        <label for="idwewenang">Panduan Wewenang</label>
                                        <textarea class="form-control" id="text-wewenang" rows="3">@if(!empty($panduans[5])){{$panduans[5]->text_panduan}}@endif</textarea>
                                      </div>
                                      <div class="text-end">
                                        <button class="btn btn-primary" id="btn-wewenang" type="submit" style="width:100px;">Simpan</button>
                                      </div>
                                  </div>
                                  <div class="tab-pane fade" id="list-korelasi" role="tabpanel" aria-labelledby="list-korelasi-list">
                                      <div id="message-update7"></div>
                                      <div class="form-group">
                                        <label for="idkorelasi">Panduan Korelasi Jabatan</label>
                                        <textarea class="form-control" id="text-korelasi-jabatan" rows="3">@if(!empty($panduans[6])){{$panduans[6]->text_panduan}}@endif</textarea>
                                      </div>
                                      <div class="text-end">
                                        <button class="btn btn-primary" id="btn-korelasijabatan" type="submit" style="width:100px;">Simpan</button>
                                      </div>
                                  </div>
                                  <div class="tab-pane fade" id="list-lingkungan" role="tabpanel" aria-labelledby="list-lingkungan-list">
                                      <div id="message-update8"></div>
                                      <div class="form-group">
                                        <label for="idlingkungankerja">Panduan Lingkungan Kerja</label>
                                        <textarea class="form-control" id="text-lingkungan-kerja" rows="3">@if(!empty($panduans[7])){{$panduans[7]->text_panduan}}@endif</textarea>
                                      </div>
                                      <div class="text-end">
                                        <button class="btn btn-primary" id="btn-lingkungankerja" type="submit" style="width:100px;">Simpan</button>
                                      </div>
                                  </div>
                                  <div class="tab-pane fade" id="list-prestasi" role="tabpanel" aria-labelledby="list-prestasi-list">
                                      <div id="message-update9"></div>
                                      <div class="form-group">
                                        <label for="idprestasikerja">Panduan Prestasi Kerja</label>
                                        <textarea class="form-control" id="text-prestasi-kerja" rows="3">@if(!empty($panduans[8])){{$panduans[8]->text_panduan}}@endif</textarea>
                                      </div>
                                      <div class="text-end">
                                        <button class="btn btn-primary" id="btn-prestasikerja" type="submit" style="width:100px;">Simpan</button>
                                      </div>
                                  </div>
                                  <div class="tab-pane fade" id="list-syarat" role="tabpanel" aria-labelledby="list-syarat-list">
                                      <div id="message-update10"></div>
                                      <div class="form-group">
                                        <label for="idsyaratjabatan">Panduan Syarat Jabatan</label>
                                        <textarea class="form-control" id="text-syarat-jabatan" rows="3">@if(!empty($panduans[9])){{$panduans[9]->text_panduan}}@endif</textarea>
                                      </div>
                                      <div class="text-end">
                                        <button class="btn btn-primary" id="btn-syaratjabatan" type="submit" style="width:100px;">Simpan</button>
                                      </div>
                                  </div>
                                  <div class="tab-pane fade" id="list-kualifikasi" role="tabpanel" aria-labelledby="list-kualifikasi-list">
                                      <div id="message-update11"></div>
                                      <div class="form-group">
                                        <label for="idkualifikasijabatan">Panduan Kualifikasi Jabatan</label>
                                        <textarea class="form-control" id="text-kualifikasi-jabatan" rows="3">@if(!empty($panduans[10])){{$panduans[10]->text_panduan}}@endif</textarea>
                                      </div>
                                      <div class="text-end">
                                        <button class="btn btn-primary" id="btn-kualifikasijabatan" type="submit" style="width:100px;">Simpan</button>
                                      </div>
                                  </div>
                                  <div class="tab-pane fade" id="list-resiko" role="tabpanel" aria-labelledby="list-resiko-list">
                                      <div id="message-update12"></div>
                                      <div class="form-group">
                                        <label for="idresikobahaya">Panduan Resiko Bahaya</label>
                                        <textarea class="form-control" id="text-resiko-bahaya" rows="3">@if(!empty($panduans[11])){{$panduans[11]->text_panduan}}@endif</textarea>
                                      </div>
                                      <div class="text-end">
                                        <button class="btn btn-primary" id="btn-resikobahaya" type="submit" style="width:100px;">Simpan</button>
                                      </div>
                                  </div>
                                  <!-- end bodylist -->
                              </div>
                        </div>
                    </div>
                </div>
              </div>
            </div>
          </div>
          <div role="tabpanel" class="tab-pane fade" id="pegawai">
            <div class="row">
              <div class="col-lg-12">
                <div class="card">
                  <div class="card-header border-0">
                    <div class="card-tools">
                      <div class="row">
                        <div class="col">
                          <div class="input-group input-group-sm" style="width: 150px;">
                            <input type="text" id="table_search_pegawai" class="form-control float-right" placeholder="Search">
                            <div class="input-group-append">
                              <button class="btn btn-default" id="submit-search-pegawai">
                                <i class="fas fa-search"></i>
                              </button>
                            </div>
                          </div>
                        </div>
                        <div class="col">
                          <a href="#" class="btn btn-tool btn-sm" data-toggle="modal" data-target="#modalcreatepegawai">
                            <i class="fas fa-user-plus"></i> Tambah Pegawai
                          </a>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- /.card-header -->
                  <div class="card-body table-responsive p-0" id="list-table-pegawai">
                    <table class="table table-striped table-valign-middle">
                      <thead>
                        <tr>
                          <th>ID</th>
                          <th>Nama</th>
                          <th>NIP</th>
                          <th>Alamat</th>
                          <th>SKPD</th>
                          <th>Jabatan</th>
                          <th>Actions</th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($pegawais as $pegawai)
                        <tr>
                          <td>{{$pegawai->id}}</td>
                          <td>{{$pegawai->nama}}</td>
                          <td>{{$pegawai->nip}}</td>
                          <td>{{strlen($pegawai->alamat) > 50 ? substr($pegawai->alamat,0,50)."..." : $pegawai->alamat}}</td>
                          <td>{{$pegawai->id_skpd ? $pegawai->skpd()->first()->nama_skpd : ''}}</td>
                          <td>{{strlen($pegawai->jabatan) > 50 ? substr($pegawai->jabatan,0,50)."..." : $pegawai->jabatan}}</td>
                          <td>
                            <button class="btn" data-toggle="modal" data-target="#modalupdatepegawai" onclick="clickUpdatePegawai({{$pegawai->id}})"><i class="fas fa-edit"></i></button>
                            <button class="btn" data-toggle="modal" data-target="#modaldeletepegawai" onclick="clickDeletePegawai({{$pegawai->id}})"><i class="far fa-trash-alt"></i></button>
                          </td>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                    {{ $pegawais->onEachSide(2)->links() }}
                  </div>
                  <!-- /.card-body -->
                </div>
                <!-- /.card -->
              </div>
            </div>
          </div>
        </div>
        @include('master.akun')
        @include('master.pegawai')
      </div>
    </section>
</div>
<!-- /.content-wrapper -->

@endsection
@section('script')
<script>

  $('#btn-tugaspokok').click(function(){
    var text = $('#text-tugas-pokok').val();
    $.ajax({
          type:'GET',
          url:'/master/panduan-tugas-pokok',
          data: { text:text },
          success:function(json){
              if(json['message'] = 1){
                  $('#message-update1').html('<div class="alert alert-success fade show" style="padding: 5px 10px;background-color: #d4edda;border-color: #c3e6cb;color: #155724;" role="alert"><i class="far fa-check-circle" style="margin-right:5px;"></i>Data panduan tugas pokok berhasil diupdate<a href="#" class="close" data-dismiss="alert" aria-label="close"><i class="far fa-times-circle"></i></a></div>')
              }
              setTimeout(function(){ $('.alert').alert('close') }, 3000);
          }
    });
  })

  $('#btn-hasilkerja').click(function(){
    var text = $('#text-hasil-panduan').val();
    $.ajax({
          type:'GET',
          url:'/master/panduan-hasil-kerja',
          data: { text:text },
          success:function(json){
              if(json['message'] = 1){
                  $('#message-update2').html('<div class="alert alert-success fade show" style="padding: 5px 10px;background-color: #d4edda;border-color: #c3e6cb;color: #155724;" role="alert"><i class="far fa-check-circle" style="margin-right:5px;"></i>Data panduan hasil kerja berhasil diupdate<a href="#" class="close" data-dismiss="alert" aria-label="close"><i class="far fa-times-circle"></i></a></div>')
              }
              setTimeout(function(){ $('.alert').alert('close') }, 3000);
          }
    });
  })

  $('#btn-bahankerja').click(function(){
    var text = $('#text-bahan-kerja').val();
    $.ajax({
          type:'GET',
          url:'/master/panduan-bahan-kerja',
          data: { text:text },
          success:function(json){
              if(json['message'] = 1){
                  $('#message-update3').html('<div class="alert alert-success fade show" style="padding: 5px 10px;background-color: #d4edda;border-color: #c3e6cb;color: #155724;" role="alert"><i class="far fa-check-circle" style="margin-right:5px;"></i>Data panduan bahan kerja berhasil diupdate<a href="#" class="close" data-dismiss="alert" aria-label="close"><i class="far fa-times-circle"></i></a></div>')
              }
              setTimeout(function(){ $('.alert').alert('close') }, 3000);
          }
    });
  })

  $('#btn-tanggungjawab').click(function(){
    var text = $('#text-tanggung-jawab').val();
    $.ajax({
          type:'GET',
          url:'/master/panduan-tanggung-jawab',
          data: { text:text },
          success:function(json){
              if(json['message'] = 1){
                  $('#message-update4').html('<div class="alert alert-success fade show" style="padding: 5px 10px;background-color: #d4edda;border-color: #c3e6cb;color: #155724;" role="alert"><i class="far fa-check-circle" style="margin-right:5px;"></i>Data panduan tanggung jawab berhasil diupdate<a href="#" class="close" data-dismiss="alert" aria-label="close"><i class="far fa-times-circle"></i></a></div>')
              }
              setTimeout(function(){ $('.alert').alert('close') }, 3000);
          }
    });
  })


  $('#btn-perangkatkerja').click(function(){
    var text = $('#text-perangkat-kerja').val();
    $.ajax({
          type:'GET',
          url:'/master/panduan-perangkat-kerja',
          data: { text:text },
          success:function(json){
              if(json['message'] = 1){
                  $('#message-update5').html('<div class="alert alert-success fade show" style="padding: 5px 10px;background-color: #d4edda;border-color: #c3e6cb;color: #155724;" role="alert"><i class="far fa-check-circle" style="margin-right:5px;"></i>Data panduan perangkat kerja berhasil diupdate<a href="#" class="close" data-dismiss="alert" aria-label="close"><i class="far fa-times-circle"></i></a></div>')
              }
              setTimeout(function(){ $('.alert').alert('close') }, 3000);
          }
    });
  })

  $('#btn-wewenang').click(function(){
    var text = $('#text-wewenang').val();
    $.ajax({
          type:'GET',
          url:'/master/panduan-wewenang',
          data: { text:text },
          success:function(json){
              if(json['message'] = 1){
                  $('#message-update6').html('<div class="alert alert-success fade show" style="padding: 5px 10px;background-color: #d4edda;border-color: #c3e6cb;color: #155724;" role="alert"><i class="far fa-check-circle" style="margin-right:5px;"></i>Data panduan wewenang berhasil diupdate<a href="#" class="close" data-dismiss="alert" aria-label="close"><i class="far fa-times-circle"></i></a></div>')
              }
              setTimeout(function(){ $('.alert').alert('close') }, 3000);
          }
    });
  })

  $('#btn-korelasijabatan').click(function(){
    var text = $('#text-korelasi-jabatan').val();
    $.ajax({
          type:'GET',
          url:'/master/panduan-korelasi-jabatan',
          data: { text:text },
          success:function(json){
              if(json['message'] = 1){
                  $('#message-update7').html('<div class="alert alert-success fade show" style="padding: 5px 10px;background-color: #d4edda;border-color: #c3e6cb;color: #155724;" role="alert"><i class="far fa-check-circle" style="margin-right:5px;"></i>Data panduan korelasi jabatan berhasil diupdate<a href="#" class="close" data-dismiss="alert" aria-label="close"><i class="far fa-times-circle"></i></a></div>')
              }
              setTimeout(function(){ $('.alert').alert('close') }, 3000);
          }
    });
  })

  $('#btn-lingkungankerja').click(function(){
    var text = $('#text-lingkungan-kerja').val();
    $.ajax({
          type:'GET',
          url:'/master/panduan-lingkungan-kerja',
          data: { text:text },
          success:function(json){
              if(json['message'] = 1){
                  $('#message-update8').html('<div class="alert alert-success fade show" style="padding: 5px 10px;background-color: #d4edda;border-color: #c3e6cb;color: #155724;" role="alert"><i class="far fa-check-circle" style="margin-right:5px;"></i>Data panduan lingkungan kerja berhasil diupdate<a href="#" class="close" data-dismiss="alert" aria-label="close"><i class="far fa-times-circle"></i></a></div>')
              }
              setTimeout(function(){ $('.alert').alert('close') }, 3000);
          }
    });
  })

  $('#btn-prestasikerja').click(function(){
    var text = $('#text-prestasi-kerja').val();
    $.ajax({
          type:'GET',
          url:'/master/panduan-prestasi-kerja',
          data: { text:text },
          success:function(json){
              if(json['message'] = 1){
                  $('#message-update9').html('<div class="alert alert-success fade show" style="padding: 5px 10px;background-color: #d4edda;border-color: #c3e6cb;color: #155724;" role="alert"><i class="far fa-check-circle" style="margin-right:5px;"></i>Data panduan prestasi kerja berhasil diupdate<a href="#" class="close" data-dismiss="alert" aria-label="close"><i class="far fa-times-circle"></i></a></div>')
              }
              setTimeout(function(){ $('.alert').alert('close') }, 3000);
          }
    });
  })

  $('#btn-syaratjabatan').click(function(){
    var text = $('#text-syarat-jabatan').val();
    $.ajax({
          type:'GET',
          url:'/master/panduan-syarat-jabatan',
          data: { text:text },
          success:function(json){
              if(json['message'] = 1){
                  $('#message-update10').html('<div class="alert alert-success fade show" style="padding: 5px 10px;background-color: #d4edda;border-color: #c3e6cb;color: #155724;" role="alert"><i class="far fa-check-circle" style="margin-right:5px;"></i>Data panduan syarat jabatan berhasil diupdate<a href="#" class="close" data-dismiss="alert" aria-label="close"><i class="far fa-times-circle"></i></a></div>')
              }
              setTimeout(function(){ $('.alert').alert('close') }, 3000);
          }
    });
  })
  
  $('#btn-kualifikasijabatan').click(function(){
    var text = $('#text-kualifikasi-jabatan').val();
    $.ajax({
          type:'GET',
          url:'/master/panduan-kualifikasi-jabatan',
          data: { text:text },
          success:function(json){
              if(json['message'] = 1){
                  $('#message-update11').html('<div class="alert alert-success fade show" style="padding: 5px 10px;background-color: #d4edda;border-color: #c3e6cb;color: #155724;" role="alert"><i class="far fa-check-circle" style="margin-right:5px;"></i>Data panduan kualifikasi jabatan berhasil diupdate<a href="#" class="close" data-dismiss="alert" aria-label="close"><i class="far fa-times-circle"></i></a></div>')
              }
              setTimeout(function(){ $('.alert').alert('close') }, 3000);
          }
    });
  })
  
  $('#btn-resikobahaya').click(function(){
    var text = $('#text-resiko-bahaya').val();
    $.ajax({
          type:'GET',
          url:'/master/panduan-resiko-bahaya',
          data: { text:text },
          success:function(json){
              if(json['message'] = 1){
                  $('#message-update12').html('<div class="alert alert-success fade show" style="padding: 5px 10px;background-color: #d4edda;border-color: #c3e6cb;color: #155724;" role="alert"><i class="far fa-check-circle" style="margin-right:5px;"></i>Data panduan resiko bahaya berhasil diupdate<a href="#" class="close" data-dismiss="alert" aria-label="close"><i class="far fa-times-circle"></i></a></div>')
              }
              setTimeout(function(){ $('.alert').alert('close') }, 3000);
          }
    });
  })
  
</script>
<script>
  $(function () {
    $('#myList a:last-child').tab('show')
  })

    // Javascript to enable link to tab
    var hash = location.hash.replace(/^#/, '');  // ^ means starting, meaning only match the first hash
    if (hash) {
        $('.nav-tabs a[href="#' + hash + '"]').tab('show');
    } 

    // Change hash for page-reload
    $('.nav-tabs a').on('shown.bs.tab', function (e) {
        window.location.hash = e.target.hash;
    })

  function clickDeleteUser(id) {
    $('#form-delete-akun').attr('action', '/master/delete-akun/'+id);
  }
  function clickUpdateUser(id) {
    $.get('/master/get-user-detail/'+id, function(data) {
      $('#form-update-akun').attr('action', '/master/update-akun/'+id);
      $('#nama2').val(data.name);
      $('#username2').val(data.username);
      $('#role_id2').val(data.role_id).trigger('change');
      $('#id_skpd_akun2').val(data.id_skpd).trigger('change');
    });
  }

  function toggleActiveAkun(id) {
    console.log('hello');
    $.post('/api/master/toggle-active-akun', {
      '_token' : $('meta[name=csrf-token]').attr('content'),
      id: id
    }).done(function(data) {
      // console.log(data);
    });
  }

  function clickDeleteJenisJab(id) {
    $('#form-delete-jenisjabatan').attr('action', '/master/delete-jenis-jabatan/'+id);
  }
  function clickUpdateJenisJab(id) {
    $.get('/master/get-jenis-jabatan-detail/'+id, function(data) {
      $('#form-update-jenisjabatan').attr('action', '/master/update-jenis-jabatan/'+id);
      $('#nama_jenis_jabatan2').val(data.nama_jenis_jabatan);
    });
  }

  function clickDeleteJabatan(id) {
    $('#form-delete-jabatan').attr('action', '/master/delete-jabatan/'+id);
  }
  function clickUpdateJabatan(id) {
    $.get('/master/get-jabatan-detail/'+id, function(data) {
      console.log(data);
      $('#form-update-jabatan').attr('action', '/master/update-jabatan/'+id);
      $('#id_jenis_jabatan2').val(data.id_jenis_jabatan).trigger('change');
      $('#jabatan2').val(data.jabatan);
      $('#is_asisten').val(data.is_asisten).trigger('change');
      $('#id_eselon_jabatan2').val(data.id_eselon).trigger('change');
    });
  }

  function clickDeletePegawai(id) {
    $.get('/master/get-pegawai-detail/'+id, function(data) {
      $('#span-pegawai').html(data.nama);
      $('#id_pegawai3').val(data.id);
    });
  }
  function clickUpdatePegawai(id) {
    $.get('/master/get-pegawai-detail/'+id, function(data) {
      $('#id_pegawai2').val(data.id);
      $('#nama_pegawai2').val(data.nama);
      $('#nip2').val(data.nip);
      $('#alamat2').val(data.alamat);
      $('#id_skpd_2').val(data.id_skpd).trigger('change');
      $('#id_eselon_2').val(data.id_eselon).trigger('change');
    });
  }
  
  function deleteForm(id) {
    $.get('/api/get-skpd/'+id, function(data) {
      $('#modaldeleteskpd #delete-text').html('Anda yakin ingin menghapus? '+data.nama_skpd);
      $('#modaldeleteskpd #id-delete').val();
      $('#modaldeleteskpd').modal('show');
      $('#action-del').attr('action', '/abk/hapus-skpd/'+id);
    });
  }

  function clickDeleteOPDJab(id) {
    $.get('/master/get-opdjab-detail/'+id, function(data) {
      $('#span-opdjab').html(data.nama_jabatan+' - '+data.nama_skpd);
      $('#id_opdjab2').val(data.id);
    });
  }
  function clickUpdateOPDJab(id, id_jabatan) {
    $.get('/master/get-opdjab-detail/'+id, function(data) {
      $('#id_skpd_opdjab').val(data.id_skpd).trigger('change');
      $('#id_opdjab').val(id);
      $.get('/api/master/get-jabatan-opd-tersisa?id_skpd='+$('#id_skpd_opdjab').val()+'&id='+id, function(data) {
        $('#id_jabatan_opdjab').html('');
        $.each(data, function( key, value ) {
          if(key == id_jabatan) {
            $('#id_jabatan_opdjab').append('<option value="'+key+'" selected="selected">'+value+'</option>');
          }
          else {
            $('#id_jabatan_opdjab').append('<option value="'+key+'">'+value+'</option>');
          }
        });
      });
    });
  }

  $("#submit-search-pegawai").click(function() {
    if($('#table_search_pegawai').val() == "") {
      window.location.href = '/master#pegawai';
    }
    else {
      window.location.href = '/master?nama_pegawai='+$('#table_search_pegawai').val()+'#pegawai';
    }
  });
  $('#table_search_pegawai').keypress(function (e) {
    var key = e.which;
    if(key == 13)  // the enter key code
    {
      if($(this).val() == "") {
        window.location.href = '/master#pegawai';
      }
      else {
        window.location.href = '/master?nama_pegawai='+$(this).val()+'#pegawai';
      }
      return false;
    }
  });

  $("#submit-search-skpd").click(function() {
    if($('#table_search_skpd').val() == "") {
      window.location.href = '/master#opd';
    }
    else {
      window.location.href = '/master?nama_skpd='+$('#table_search_skpd').val()+'#opd';
    }
  });
  $('#table_search_skpd').keypress(function (e) {
    var key = e.which;
    if(key == 13)  // the enter key code
    {
      if($(this).val() == "") {
        window.location.href = '/master#opd';
      }
      else {
        window.location.href = '/master?nama_skpd='+$(this).val()+'#opd';
      }
      return false;
    }
  });

  $("#submit-search-opdjab").click(function() {
    if($('#table_search_opdjab').val() == "") {
      window.location.href = '/master#opdjab';
    }
    else {
      // window.location.href = '/master?nama_opdjab='+$('#table_search_skpd').val()+'#opd';
    }
  });
  $('#table_search_opdjab').keypress(function (e) {
    var key = e.which;
    if(key == 13)  // the enter key code
    {
      if($(this).val() == "") {
        window.location.href = '/master#opdjab';
      }
      else {
        window.location.href = '/master?nama_opdjab='+$(this).val()+'#opdjab';
      }
      return false;
    }
  });
  $.fn.appendAttr = function(attrName, suffix) {
    this.attr(attrName, function(i, val) {
      return val + suffix;
    });
    return this;
  };
  $(document).ready(function() {
    $("input[data-bootstrap-switch]").each(function(){
      $(this).bootstrapSwitch('state', $(this).prop('checked'));
      $(this).attr('onchange', $(this).attr('onclick'));
    });
    $('#list-table-pegawai').find('nav').find('a.page-link').appendAttr('href', '#pegawai');
    $('#list-table-skpd').find('nav').find('a.page-link').appendAttr('href', '#opd');
    $('#list-table-skpdjab').find('nav').find('a.page-link').appendAttr('href', '#opdjab');
    var searchPegawaiRequest = {!! isset($_GET['nama']) ? '"'.$_GET['nama'].'"' : '""' !!};
    $('#table_search_pegawai').val(searchPegawaiRequest);
    $('#filter_id_skpd').change(function() {
      $('#id_pegawai_skpd').html('<option value="" selected="selected">-</option>');
      $.get('/master/get-pegawai-by-skpd/'+$(this).val(), function(data) {
        $.each(data, function( index, value ) {
          $('#id_pegawai_skpd').append('<option value="'+index+'">'+value+'</option>');
        });
      });
    });
    $('#filter_id_skpd2').change(function() {
      $('#id_pegawai_skpd2').html('<option value="" selected="selected">-</option>');
      $.get('/master/get-pegawai-by-skpd/'+$(this).val(), function(data) {
        $.each(data, function( index, value ) {
          $('#id_pegawai_skpd2').append('<option value="'+index+'">'+value+'</option>');
        });
      });
    });
    $('#btn-tambah-opdjab').click(function() {
      $.get('/api/master/get-jabatan-opd-tersisa?id_skpd='+$('#id_skpd_opdjab_new').val(), function(data) {
        $('#id_jabatan_opdjab_new').html('');
        $.each(data, function( key, value ) {
          $('#id_jabatan_opdjab_new').append('<option value="'+key+'">'+value+'</option>');
        });
      });
    });
    $('#id_skpd_opdjab_new').change(function() {
      $.get('/api/master/get-jabatan-opd-tersisa?id_skpd='+$(this).val(), function(data) {
        $('#id_jabatan_opdjab_new').html('');
        $.each(data, function( key, value ) {
          $('#id_jabatan_opdjab_new').append('<option value="'+key+'">'+value+'</option>');
        });
      });
    });
    $('#id_skpd_opdjab').change(function() {
      $.get('/api/master/get-jabatan-opd-tersisa?id_skpd='+$(this).val()+'&id='+$('#id_opdjab').val(), function(data) {
        $('#id_jabatan_opdjab').html('');
        $.each(data, function( key, value ) {
          $('#id_jabatan_opdjab').append('<option value="'+key+'">'+value+'</option>');
        });
      });
    });
    $('#btn-filter-opdjab').click(function() {
      var search_opdjab = $('#table_search_opdjab').val();
      if(search_opdjab != "") {
        $('#hidden_search_opdjab').val(search_opdjab);
        $('#hidden_search_opdjab').attr('name', 'nama_opdjab');
      }
    });
    $('#select_filter_skpd').change(function() {
      var selected = $(this).select2('data');
      var text = "";
      $.each(selected, function(key, value) {
        text += value.id+",";
      });
      $('#selected').val(text);
    });
  });
</script>
@endsection