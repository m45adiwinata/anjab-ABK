@extends('layouts.main')

@section('title' , 'Master')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h5 class="m-0">MASTER</h5>
            <ol class="breadcrumb float-sm-left">
              <li class="breadcrumb-item"><a href="/master">All Menu</a></li>
              <li class="breadcrumb-item active">Prestasi Kerja</li>
            </ol>
          </div><!-- /.col -->
          <div class="col-sm-6">
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        @if(Session::has('success'))
            <div class="alert alert-success" style="padding: 5px 10px;background-color: #d4edda;border-color: #c3e6cb;color: #155724;" role="alert">
            <i class="far fa-check-circle" style="margin-right:5px;"></i>{{ Session::get('success') }}<a href="#" class="close" data-dismiss="alert" aria-label="close"><i class="far fa-times-circle"></i></a>
            </div>
        @endif
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header border-0">
                        <div class="card-tools">
                            <a href="#" class="btn btn-tool btn-sm" data-toggle="modal" data-target="#modalcreate">
                            <i class="fas fa-user-plus"></i> Tambah Prestasi Kerja
                            </a>
                        </div>
                    </div>
                    <div class="card-body table-responsive p-0">
                        <table class="table table-striped table-valign-middle">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Nama Prestasi Kerja</th>
                                <th>Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($prestasi_kerjas as $prestasi_kerja)
                            <tr>
                                <td>{{$prestasi_kerja->id}}</td>
                                <td>{{$prestasi_kerja->prestasi}}</td>
                                <td>
                                    <button class="btn" data-toggle="modal" data-target="#modalupdate" onclick="clickUpdate({{$hasil_kerja->id}})"><i class="fas fa-edit"></i></button>
                                    <button class="btn" data-toggle="modal" data-target="#modaldelete" onclick="clickDelete({{$hasil_kerja->id}})"><i class="far fa-trash-alt"></i></button>
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- /.card -->
            </div>
        </div>
        <!-- Modal Create -->
        <div class="modal fade" id="modalcreate" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Hasil Kerja Baru</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="card-body">
                    <form action="{{route('tambah-hasil-uraian-kerja')}}" class="form" method="POST">
                        @csrf
                        <div class="form-group">
                            <label for="nama_hasil_kerja">Nama Hasil Kerja</label>
                            <input type="text" class="form-control" id="nama_hasil_kerja" name="nama_hasil_kerja" placeholder="Nama Hasil Kerja" autocomplete="off">
                        </div>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-primary" type="submit">Simpan</button>
                    </form>
                    </div>
                    <!-- /.card-body -->
                </div>
                </div>
            </div>
        </div>
        <!-- /.Modal -->
        <!-- Modal Update -->
        <div class="modal fade" id="modalupdate" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Hasil Kerja Update</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="card-body">
                    <form action="{{route('update-hasil-uraian-kerja')}}" class="form" method="POST" id="form-update">
                        @csrf
                        <input type="hidden" class="form-control" id="id" name="id" value="">
                        <div class="form-group">
                            <label for="nama2">Nama Hasil Kerja</label>
                            <input type="text" class="form-control" id="nama2" name="nama_hasil_kerja" placeholder="Nama Hasil Kerja" autocomplete="off">
                        </div>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-primary" type="submit">Simpan</button>
                    </form>
                    </div>
                    <!-- /.card-body -->
                </div>
                </div>
            </div>
        </div>
        <!-- /.Modal -->

        <!-- Modal Delete -->
        <div class="modal fade" id="modaldelete" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Anda yakin ingin menghapus hasil kerja ini?<br>(<b id="temp_nama"></b>)</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="card-body">
                <form action="{{route('hapus-hasil-uraian-kerja')}}" id="form-delete" method="POST">
                    @csrf
                    <input type="hidden" class="form-control" id="id2" name="id" value="">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-primary" type="submit">Yakin</button>
                </form>
                </div>
                <!-- /.card-body -->
            </div>
            </div>
        </div>
        </div>
        <!-- /.Modal -->
      </div>
    </section>
</div>
@endsection
@section('script')
<script>
  function clickDelete(id) {
    $.get('/master/hasil-uraian-kerja/get-detail/'+id, function(data) {
        $('#id2').val(id);
        $('#temp_nama').html(data.nama_hasil_kerja);
    })
  }
  function clickUpdate(id) {
    $.get('/master/hasil-uraian-kerja/get-detail/'+id, function(data) {
        $('#id').val(id);  
      $('#nama2').val(data.nama_hasil_kerja);
    });
  }
</script>
@endsection