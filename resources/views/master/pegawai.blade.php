<!-- Modal Create -->
<div class="modal fade" id="modalcreatepegawai" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Pegawai Baru</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="card-body">
          <form action="{{route('create-pegawai')}}" class="form" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
              <label for="nama_pegawai">Nama</label>
              <input type="text" class="form-control" id="nama_pegawai" name="nama" placeholder="Nama" autocomplete="off">
            </div>
            <div class="form-group">
              <label for="nip">NIP</label>
              <input type="text" class="form-control" id="nip" name="nip" placeholder="NIP" autocomplete="off">
            </div>
            <div class="form-group">
              <label for="alamat">Alamat</label>
              <textarea class="form-control" id="alamat" name="alamat" rows="3"></textarea>
            </div>
            <div class="form-group">
              <label>SKPD</label>
              <select class="form-control select2" name="id_skpd" style="width: 100%;">
                @foreach($skpd_all as $key => $skpd)
                @if($key==0)
                <option value="{{$skpd->id}}" selected="selected">{{$skpd->nama_skpd}}</option>
                @else
                <option value="{{$skpd->id}}">{{$skpd->nama_skpd}}</option>
                @endif
                @endforeach
              </select>
            </div>
            <div class="form-group">
              <label>Eselon</label>
              <select class="form-control select2" name="id_eselon" style="width: 100%;">
                @foreach($eselons as $key => $eselon)
                @if($key==0)
                <option value="{{$eselon->id}}" selected="selected">{{$eselon->eselon}}</option>
                @else
                <option value="{{$eselon->id}}">{{$eselon->eselon}}</option>
                @endif
                @endforeach
              </select>
            </div>
            <div class="form-group">
                <label for="foto_pegawai">Foto Pegawai</label>
                <input type="file" class="form-control-file" id="foto_pegawai" name="foto">
            </div>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
            <button type="submit" class="btn btn-primary" type="submit">Simpan</button>
          </form>
        </div>
        <!-- /.card-body -->
      </div>
    </div>
  </div>
</div>
<!-- /.Modal -->

<!-- Modal Update -->
<div class="modal fade" id="modalupdatepegawai" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Pegawai Update</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="card-body">
          <form action="{{route('update-pegawai')}}" class="form" method="POST" enctype="multipart/form-data">
            @csrf
            <input type="hidden" id="id_pegawai2" name="id" value="">
            <div class="form-group">
              <label for="nama_pegawai2">Nama</label>
              <input type="text" class="form-control" id="nama_pegawai2" name="nama" placeholder="Nama" autocomplete="off">
            </div>
            <div class="form-group">
              <label for="nip2">NIP</label>
              <input type="text" class="form-control" id="nip2" name="nip" placeholder="NIP" autocomplete="off">
            </div>
            <div class="form-group">
              <label for="alamat2">Alamat</label>
              <textarea class="form-control" id="alamat2" name="alamat" rows="3"></textarea>
            </div>
            <div class="form-group">
              <label>SKPD</label>
              <select class="form-control select2" id="id_skpd_2" name="id_skpd" style="width: 100%;">
                @foreach($skpd_all as $key => $skpd)
                @if($key==0)
                <option value="{{$skpd->id}}" selected="selected">{{$skpd->nama_skpd}}</option>
                @else
                <option value="{{$skpd->id}}">{{$skpd->nama_skpd}}</option>
                @endif
                @endforeach
              </select>
            </div>
            <div class="form-group">
              <label>Eselon</label>
              <select class="form-control select2" id="id_eselon_2" name="id_eselon" style="width: 100%;">
                @foreach($eselons as $key => $eselon)
                @if($key==0)
                <option value="{{$eselon->id}}" selected="selected">{{$eselon->eselon}}</option>
                @else
                <option value="{{$eselon->id}}">{{$eselon->eselon}}</option>
                @endif
                @endforeach
              </select>
            </div>
            <div class="form-group">
                <label for="foto_pegawai">Foto Pegawai</label>
                <input type="file" class="form-control-file" id="foto_pegawai2" name="foto">
            </div>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
            <button type="submit" class="btn btn-primary" type="submit">Simpan</button>
          </form>
        </div>
        <!-- /.card-body -->
      </div>
    </div>
  </div>
</div>
<!-- /.Modal -->

<!-- Modal Delete -->
<div class="modal fade" id="modaldeletepegawai" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="msg_hapus_peg">Anda yakin ingin menghapus pegawai ini?<br><span id="span-pegawai"></span></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="card-body">
          <form action="{{route('delete-pegawai')}}" method="POST">
            @csrf
            <input type="hidden" id="id_pegawai3" name="id" value="">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
            <button type="submit" class="btn btn-primary" type="submit">Yakin</button>
          </form>
        </div>
        <!-- /.card-body -->
      </div>
    </div>
  </div>
</div>
<!-- /.Modal -->