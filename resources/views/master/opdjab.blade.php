<!-- Modal Update OPD Jabatan -->
<div class="modal fade" id="modalcreateopdjab" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">OPD Jabatan Baru</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="card-body">
          <form action="{{route('create-opdjab')}}" class="form" method="POST">
            @csrf
            <div class="form-group">
              <label>OPD</label>
              <select class="form-control select2" name="id_skpd" id="id_skpd_opdjab_new" style="width: 100%;">
                @foreach($skpd_all as $key => $skpd)
                <option value="{{$skpd->id}}">{{$skpd->nama_skpd}}</option>
                @endforeach
              </select>
            </div>
            <div class="form-group">
              <label>Jabatan</label>
              <select class="form-control select2" name="id_jabatan" id="id_jabatan_opdjab_new" style="width: 100%;">
              </select>
            </div>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
            <button type="submit" class="btn btn-primary" type="submit">Simpan</button>
          </form>
        </div>
        <!-- /.card-body -->
      </div>
    </div>
  </div>
</div>
<!-- /.Modal -->

<!-- Modal Update OPD Jabatan -->
<div class="modal fade" id="modalupdateopdjab" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">OPD Jabatan Update</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="card-body">
          <form action="{{route('update-opdjab')}}" class="form" method="POST">
            @csrf
            <input type="hidden" name="id" id="id_opdjab">
            <div class="form-group">
              <label>OPD</label>
              <select class="form-control select2" name="id_skpd" id="id_skpd_opdjab" style="width: 100%;">
                @foreach($skpd_all as $key => $skpd)
                <option value="{{$skpd->id}}">{{$skpd->nama_skpd}}</option>
                @endforeach
              </select>
            </div>
            <div class="form-group">
              <label>Jabatan</label>
              <select class="form-control select2" name="id_jabatan" id="id_jabatan_opdjab" style="width: 100%;">
              </select>
            </div>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
            <button type="submit" class="btn btn-primary" type="submit">Simpan</button>
          </form>
        </div>
        <!-- /.card-body -->
      </div>
    </div>
  </div>
</div>
<!-- /.Modal -->

<!-- Modal Delete OPD Jabatan -->
<div class="modal fade" id="modaldeleteopdjab" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Anda yakin ingin menghapus OPD Jabatan ini?<br><span id="span-opdjab"></span></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="card-body">
          <form action="{{route('hapus-opdjab')}}" method="POST">
            @csrf
            <input type="hidden" value="" id="id_opdjab2" name="id">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
            <button type="submit" class="btn btn-primary" type="submit">Yakin</button>
          </form>
        </div>
        <!-- /.card-body -->
      </div>
    </div>
  </div>
</div>
<!-- /.Modal -->