@extends('layouts.main')

@section('title' , 'Master')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h5 class="m-0">MASTER</h5>
            <ol class="breadcrumb float-sm-left">
              <li class="breadcrumb-item"><a href="/master">All Menu</a></li>
              <li class="breadcrumb-item active">Syarat Jabatan</li>
            </ol>
          </div><!-- /.col -->
          <div class="col-sm-6">
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        @if(Session::has('success'))
            <div class="alert alert-success" style="padding: 5px 10px;background-color: #d4edda;border-color: #c3e6cb;color: #155724;" role="alert">
            <i class="far fa-check-circle" style="margin-right:5px;"></i>{{ Session::get('success') }}<a href="#" class="close" data-dismiss="alert" aria-label="close"><i class="far fa-times-circle"></i></a>
            </div>
        @endif
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header border-0">
                    <div class="card-tools">
                        <a href="#" class="btn btn-tool btn-sm" data-toggle="modal" data-target="#modalcreatesyarat">
                        <i class="fas fa-user-plus"></i> Tambah Poin Syarat
                        </a>
                    </div>
                    </div>
                    <div class="card-body table-responsive p-0">
                    <table class="table table-striped table-valign-middle">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Poin Syarat</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($poin_syarats as $poin)
                        <tr style="cursor:pointer;">
                            <td>{{$poin->id}}</td>
                            <td>{{$poin->nama_sub_syarat}}</td>
                            <td>
                            <button class="btn" data-toggle="modal" data-target="#modalupdatesyarat" onclick="clickUpdateSyarat({{$poin->id}})"><i class="fas fa-edit"></i></button>
                            <button class="btn" data-toggle="modal" data-target="#modaldeletesyarat" onclick="clickDeleteSyarat({{$poin->id}})"><i class="far fa-trash-alt"></i></button>
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                    </div>
                </div>
                <!-- /.card -->
            </div>
        </div>
        <!-- Modal Create Syarat -->
        <div class="modal fade" id="modalcreatesyarat" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Poin Syarat Jabatan Baru</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="card-body">
                    <form action="{{route('tambah-syarat')}}" class="form" method="POST">
                        @csrf
                        <div class="form-group">
                            <label for="nama_sub_syarat">Nama Poin Syarat</label>
                            <input type="text" class="form-control" id="nama_sub_syarat" name="nama_sub_syarat" placeholder="Nama Point Syarat" autocomplete="off">
                        </div>
                        <br>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-primary" type="submit">Simpan</button>
                    </form>
                    </div>
                    <!-- /.card-body -->
                </div>
                </div>
            </div>
        </div>
        <!-- /.Modal -->
        <!-- Modal Update Syarat -->
        <div class="modal fade" id="modalupdatesyarat" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Syarat Update</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="card-body">
                    <form action="{{route('update-syarat')}}" class="form" method="POST">
                        @csrf
                        <input type="hidden" class="form-control" id="id" name="id" value="">
                        <div class="form-group">
                            <label for="nama2">Nama Poin Syarat</label>
                            <input type="text" class="form-control" id="nama2" name="nama_sub_syarat" placeholder="Nama Poin Syarat" autocomplete="off">
                        </div>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-primary" type="submit">Simpan</button>
                    </form>
                    </div>
                    <!-- /.card-body -->
                </div>
                </div>
            </div>
        </div>
        <!-- /.Modal -->

        <!-- Modal Delete Syarat -->
        <div class="modal fade" id="modaldeletesyarat" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Anda yakin ingin menghapus poin syarat jabatan ini?<br>(<b id="temp_nama"></b>)</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="card-body">
                    <form action="{{route('hapus-syarat')}}" method="POST">
                        @csrf
                        <input type="hidden" class="form-control" id="id2" name="id" value="">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-primary" type="submit">Yakin</button>
                    </form>
                    </div>
                    <!-- /.card-body -->
                </div>
                </div>
            </div>
        </div>
        <!-- /.Modal -->
      </div>
    </section>
</div>
@endsection
@section('script')
<script>
  function clickDeleteSyarat(id) {
    $.get('/master/syarat/get-detail/'+id, function(data) {
        $('#id2').val(id);
        $('#temp_nama').html(data.nama_sub_syarat);
    })
  }
  function clickUpdateSyarat(id) {
    $.get('/master/syarat/get-detail/'+id, function(data) {
        $('#id').val(id);  
        $('#nama2').val(data.nama_sub_syarat);
    });
  }
</script>
@endsection