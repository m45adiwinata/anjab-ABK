@extends('layouts.main')

@section('title' , 'Master')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h5 class="m-0">MASTER</h5>
            <ol class="breadcrumb float-sm-left">
              <li class="breadcrumb-item"><a href="/master">All Menu</a></li>
              <li class="breadcrumb-item active">Aspek Lingkungan Kerja</li>
            </ol>
          </div><!-- /.col -->
          <div class="col-sm-6">
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        @if(Session::has('success'))
            <div class="alert alert-success" style="padding: 5px 10px;background-color: #d4edda;border-color: #c3e6cb;color: #155724;" role="alert">
            <i class="far fa-check-circle" style="margin-right:5px;"></i>{{ Session::get('success') }}<a href="#" class="close" data-dismiss="alert" aria-label="close"><i class="far fa-times-circle"></i></a>
            </div>
        @endif
        <div class="row">
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-header border-0">
                    <div class="card-tools">
                        <a href="#" class="btn btn-tool btn-sm" data-toggle="modal" data-target="#modalcreateaspek">
                        <i class="fas fa-user-plus"></i> Tambah Aspek Lingkungan Kerja
                        </a>
                    </div>
                    </div>
                    <div class="card-body table-responsive p-0">
                    <table class="table table-striped table-valign-middle">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Nama Aspek Lingkungan Kerja</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($aspeks as $aspek)
                        <tr onclick="clickAspek({{$aspek->id}})" style="cursor:pointer;">
                            <td>{{$aspek->id}}</td>
                            <td>{{$aspek->nama_aspek}}</td>
                            <td>
                            <button class="btn" data-toggle="modal" data-target="#modalupdateaspek" onclick="clickUpdateAspek({{$aspek->id}})"><i class="fas fa-edit"></i></button>
                            <button class="btn" data-toggle="modal" data-target="#modaldeleteaspek" onclick="clickDeleteAspek({{$aspek->id}})"><i class="far fa-trash-alt"></i></button>
                            </td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                    </div>
                </div>
                <!-- /.card -->
                </div>
                <div class="col-lg-6">
                <div class="card">
                    <div class="card-header border-0">
                    <div class="card-tools">
                        <a href="#" class="btn btn-tool btn-sm" data-toggle="modal" data-target="#modalcreatefaktor">
                        <i class="fas fa-user-plus"></i> Tambah Faktor Aspek
                        </a>
                    </div>
                    </div>
                    <div class="card-body table-responsive p-0">
                    <table class="table table-striped table-valign-middle">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Nama Faktor</th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody id="list-faktor-aspek">
                        </tbody>
                    </table>
                    </div>
                </div>
                <!-- /.card -->
            </div>
        </div>
        <!-- Modal Create Aspek -->
        <div class="modal fade" id="modalcreateaspek" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Aspek Lingkungan Kerja Baru</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="card-body">
                    <form action="{{route('tambah-aspek-lingkungan-kerja')}}" class="form" method="POST">
                        @csrf
                        <div class="form-group">
                            <label for="nama_aspek">Nama Aspek</label>
                            <input type="text" class="form-control" id="nama_aspek" name="nama_aspek" placeholder="Nama Aspek" autocomplete="off">
                        </div>
                        <br>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-primary" type="submit">Simpan</button>
                    </form>
                    </div>
                    <!-- /.card-body -->
                </div>
                </div>
            </div>
        </div>
        <!-- /.Modal -->
        <!-- Modal Update -->
        <div class="modal fade" id="modalupdateaspek" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Aspek Lingkungan Kerja Update</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="card-body">
                    <form action="{{route('update-aspek-lingkungan-kerja')}}" class="form" method="POST">
                        @csrf
                        <input type="hidden" class="form-control" id="id" name="id" value="">
                        <div class="form-group">
                            <label for="nama2">Nama Aspek</label>
                            <input type="text" class="form-control" id="nama2" name="nama_aspek" placeholder="Nama Aspek" autocomplete="off">
                        </div>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-primary" type="submit">Simpan</button>
                    </form>
                    </div>
                    <!-- /.card-body -->
                </div>
                </div>
            </div>
        </div>
        <!-- /.Modal -->

        <!-- Modal Delete Aspek -->
        <div class="modal fade" id="modaldeleteaspek" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Anda yakin ingin menghapus aspek lingkungan kerja ini?<br>(<b id="temp_nama"></b>)</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="card-body">
                    <form action="{{route('hapus-aspek-lingkungan-kerja')}}" method="POST">
                        @csrf
                        <input type="hidden" class="form-control" id="id2" name="id" value="">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-primary" type="submit">Yakin</button>
                    </form>
                    </div>
                    <!-- /.card-body -->
                </div>
                </div>
            </div>
        </div>
        <!-- /.Modal -->
        <!-- Modal Create Faktor -->
        <div class="modal fade" id="modalcreatefaktor" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Faktor Aspek Baru</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="card-body">
                    <form action="{{route('tambah-faktor-aspek')}}" class="form" method="POST">
                        @csrf
                        <input type="hidden" id="id_aspek_lk" name="id_aspek_lk">
                        <div class="form-group">
                            <label for="nama_faktor">Nama Faktor</label>
                            <input type="text" class="form-control" id="nama_faktor" name="nama_faktor" placeholder="Nama Faktor" autocomplete="off">
                        </div>
                        <br>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-primary" type="submit">Simpan</button>
                    </form>
                    </div>
                    <!-- /.card-body -->
                </div>
                </div>
            </div>
        </div>
        <!-- /.Modal -->
        <!-- Modal Update Faktor -->
        <div class="modal fade" id="modalupdatefaktor" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Faktor Aspek Update</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="card-body">
                    <form action="{{route('update-faktor-aspek')}}" class="form" method="POST">
                        @csrf
                        <input type="hidden" class="form-control" id="id3" name="id" value="">
                        <div class="form-group">
                            <label for="nama3">Nama Faktor</label>
                            <input type="text" class="form-control" id="nama3" name="nama_faktor" placeholder="Nama Faktor" autocomplete="off">
                        </div>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-primary" type="submit">Simpan</button>
                    </form>
                    </div>
                    <!-- /.card-body -->
                </div>
                </div>
            </div>
        </div>
        <!-- /.Modal -->

        <!-- Modal Delete Faktor -->
        <div class="modal fade" id="modaldeletefaktor" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle">Anda yakin ingin menghapus faktor aspek ini?<br>(<b id="temp_nama2"></b>)</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="card-body">
                    <form action="{{route('hapus-faktor-aspek')}}" method="POST">
                        @csrf
                        <input type="hidden" class="form-control" id="id4" name="id" value="">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                        <button type="submit" class="btn btn-primary" type="submit">Yakin</button>
                    </form>
                    </div>
                    <!-- /.card-body -->
                </div>
                </div>
            </div>
        </div>
        <!-- /.Modal -->
      </div>
    </section>
</div>
@endsection
@section('script')
<script>
    function clickAspek(id) {
        $.get('/master/aspek-lingkungan-kerja/get-detail/'+id, function(data) {
            $('#list-faktor-aspek').html('');
            $.each(data.faktors, function(key,value) {
                $('#list-faktor-aspek').append(
                    '<tr>'+
                    '<td>'+key+'</td>'+
                    '<td>'+value+'</td>'+
                    '<td>'+
                    '<button class="btn" data-toggle="modal" data-target="#modalupdatefaktor" onclick="clickUpdateFaktor('+key+')"><i class="fas fa-edit"></i></button>'+
                    // '<button class="btn" data-toggle="modal" data-target="#modaldeletefaktor" onclick="clickDeleteFaktor('+key+')"><i class="far fa-trash-alt"></i></button>'+
                    '</td>'+
                    '</tr>'
                );
            });
            $('#id_aspek_lk').val(id);
        });
    }
  function clickDeleteAspek(id) {
    $.get('/master/aspek-lingkungan-kerja/get-detail/'+id, function(data) {
        $('#id2').val(id);
        $('#temp_nama').html(data.nama_aspek);
    })
  }
  function clickUpdateAspek(id) {
    $.get('/master/aspek-lingkungan-kerja/get-detail/'+id, function(data) {
        $('#id').val(id);  
      $('#nama2').val(data.nama_aspek);
    });
  }
  function clickDeleteFaktor(id) {
    $.get('/master/faktor-aspek/get-detail/'+id, function(data) {
        $('#id4').val(id);
        $('#temp_nama2').html(data.nama_faktor);
    })
  }
  function clickUpdateFaktor(id) {
    $.get('/master/faktor-aspek/get-detail/'+id, function(data) {
        $('#id3').val(id);  
        $('#nama3').val(data.nama_faktor);
    });
  }
</script>
@endsection