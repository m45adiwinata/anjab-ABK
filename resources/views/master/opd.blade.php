<!-- Modal Delete SKPD -->
<div class="modal fade" id="modaldeleteskpd" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Hapus Data SKPD</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form method="POST" id="action-del" action="">
        <div class="modal-body" id="delete-text"></div>
        @csrf
        <input type="hidden" id="id-delete" name="id" >
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
          <button type="submit" class="btn btn-primary">Iya, Hapus</button>
        </div>
      </form>
    </div>
  </div>
</div>