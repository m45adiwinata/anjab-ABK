<!-- Modal Create Jenis Jabatan -->
<div class="modal fade" id="modalcreatejenisjabatan" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Jenis Jabatan Baru</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="card-body">
          <form action="{{route('create-jenis-jab')}}" class="form" method="POST">
            @csrf
            <div class="form-group">
              <label for="nama_jenis_jabatan">Nama</label>
              <input type="text" class="form-control" id="nama_jenis_jabatan" name="nama_jenis_jabatan" placeholder="Nama Jenis Jabatan" autocomplete="off">
            </div>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
            <button type="submit" class="btn btn-primary" type="submit">Simpan</button>
          </form>
        </div>
        <!-- /.card-body -->
      </div>
    </div>
  </div>
</div>
<!-- /.Modal Create Jenis Jabatan -->
<!-- Modal Update Jenis Jabatan -->
<div class="modal fade" id="modalupdatejenisjabatan" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Jenis Jabatan Update</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="card-body">
          <form action="" class="form" method="POST" id="form-update-jenisjabatan">
            @csrf
            <div class="form-group">
              <label for="nama_jenis_jabatan2">Nama</label>
              <input type="text" class="form-control" id="nama_jenis_jabatan2" name="nama_jenis_jabatan" placeholder="Nama" autocomplete="off">
            </div>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
            <button type="submit" class="btn btn-primary" type="submit">Simpan</button>
          </form>
        </div>
        <!-- /.card-body -->
      </div>
    </div>
  </div>
</div>
<!-- /.Modal Update Jenis Jabatan -->
<!-- Modal Delete Jenis Jabatan -->
<div class="modal fade" id="modaldeletejenisjabatan" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Anda yakin ingin menghapus jenis jabatan ini?</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="card-body">
          <form action="" id="form-delete-jenisjabatan" method="POST">
            @csrf
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
            <button type="submit" class="btn btn-primary" type="submit">Yakin</button>
          </form>
        </div>
        <!-- /.card-body -->
      </div>
    </div>
  </div>
</div>
<!-- /.Modal Delete Jenis Jabatan -->

<!-- Modal Create Jabatan -->
<div class="modal fade" id="modalcreatejabatan" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Jabatan Baru</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="card-body">
          <form action="{{route('create-jabatan')}}" class="form" method="POST">
            @csrf
            <div class="form-group">
              <label>Jenis Jabatan</label>
              <select class="form-control select2" name="id_jenis_jabatan" style="width: 100%;">
                @foreach($jenisjabatans as $key => $jenisjab)
                @if($key==0)
                <option value="{{$jenisjab->id}}" selected="selected">{{$jenisjab->nama_jenis_jabatan}}</option>
                @else
                <option value="{{$jenisjab->id}}">{{$jenisjab->nama_jenis_jabatan}}</option>
                @endif
                @endforeach
              </select>
            </div>
            <div class="form-group">
              <label for="jabatan">Nama Jabatan</label>
              <input type="text" class="form-control" id="jabatan" name="jabatan" placeholder="Nama Jabatan" autocomplete="off">
            </div>
            <div class="form-group">
              <label for="is_asisten">Level Sekretaris?</label>
              <select class="form-control select2" name="is_asisten" style="width: 100%;">
                <option value="0">Tidak</option>
                <option value="1">Ya</option>
              </select>
            </div>
            <div class="form-group">
              <label>Eselon</label>
              <select class="form-control select2" name="id_eselon" id="id_eselon_jabatan" style="width: 100%;">
                @foreach($eselons as $eselon)
                <option value="{{$eselon->id}}">{{$eselon->eselon}}</option>
                @endforeach
              </select>
            </div>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
            <button type="submit" class="btn btn-primary" type="submit">Simpan</button>
          </form>
        </div>
        <!-- /.card-body -->
      </div>
    </div>
  </div>
</div>
<!-- /.Modal Create Jabatan -->
<!-- Modal Update Jabatan -->
<div class="modal fade" id="modalupdatejabatan" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Jabatan Update</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="card-body">
          <form action="" class="form" method="POST" id="form-update-jabatan">
            @csrf
            <div class="form-group">
              <label>Jenis Jabatan</label>
              <select class="form-control select2" name="id_jenis_jabatan" id="id_jenis_jabatan2" style="width: 100%;">
                @foreach($jenisjabatans as $key => $jenisjab)
                <option value="{{$jenisjab->id}}">{{$jenisjab->nama_jenis_jabatan}}</option>
                @endforeach
              </select>
            </div>
            <div class="form-group">
              <label for="jabatan2">Jabatan</label>
              <input type="text" class="form-control" id="jabatan2" name="jabatan" placeholder="Nama Jabatan" autocomplete="off">
            </div>
            <div class="form-group">
              <label>Level Sekretaris?</label>
              <select class="form-control select2" name="is_asisten" id="is_asisten" style="width: 100%;">
                <option value="0">Tidak</option>
                <option value="1">Ya</option>
              </select>
            </div>
            <div class="form-group">
              <label>Eselon</label>
              <select class="form-control select2" name="id_eselon" id="id_eselon_jabatan2" style="width: 100%;">
                @foreach($eselons as $eselon)
                <option value="{{$eselon->id}}">{{$eselon->eselon}}</option>
                @endforeach
              </select>
            </div>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
            <button type="submit" class="btn btn-primary" type="submit">Simpan</button>
          </form>
        </div>
        <!-- /.card-body -->
      </div>
    </div>
  </div>
</div>
<!-- /.Modal Update Jabatan -->
<!-- Modal Delete Jabatan -->
<div class="modal fade" id="modaldeletejabatan" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Anda yakin ingin menghapus jabatan ini?</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="card-body">
          <form action="" id="form-delete-jabatan" method="POST">
            @csrf
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
            <button type="submit" class="btn btn-primary" type="submit">Yakin</button>
          </form>
        </div>
        <!-- /.card-body -->
      </div>
    </div>
  </div>
</div>
<!-- /.Modal Delete Jabatan-->