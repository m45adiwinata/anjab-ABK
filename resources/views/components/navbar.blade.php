<!-- Navbar -->
<nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <p class="nav-link" style="margin:0px; text-transform"></p>
      </li>
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <!-- Notifications Dropdown Menu -->
      <li class="nav-item" style="display: flex;align-items: center;">
        <div class="user-panel d-flex">
            <div class="image">
            <!-- <img src="{{asset('main/dist/img/user8-128x128.jpg')}}" class="img-circle elevation-2" alt="User Image"> -->
            </div>
            <div class="info">
            <a href="/userprofile" class="d-block">{{Auth::user()->name}}</a>
            </div>
        </div>
      </li>
      <!-- <li class="nav-item">
        <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#" role="button">
          <i class="fas fa-th-large"></i>
        </a>
      </li> -->
    </ul>
  </nav>
  <!-- /.navbar -->