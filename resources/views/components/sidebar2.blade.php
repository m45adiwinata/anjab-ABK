<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4" style="background:linear-gradient(to right, #104c79, #4b7ec3)">
    <!-- Brand Logo -->
    <a href="/abk/detailskpd/{{$skpd_jab->id_skpd}}" class="brand-link" style="padding-left: 30px;">
      <!-- <img src="{{asset('main/dist/img/AdminLTELogo.png')}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8"> -->
      <i class="far fa-caret-square-left" style="margin-right:5px"></i> Simpan & Kembali
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="/uraian-tugas/{{$skpd_jab->id}}" class="nav-link">
                <span class="fa-stack fa-md">
                  @if($linkaktif == 1)
                    <i class="fas fa-circle fa-stack-2x" style="color:#e0ad2c;"></i>
                  @else
                    <i class="far fa-circle fa-stack-2x" style="color:#e0ad2c;"></i>
                  @endif
                    <b class="fa-stack-1x">1</b>
                </span>
              <p>
                Tugas Pokok
              </p>
            </a>
          </li>
          <li class="nav-item">
            @if($skpd_jab->status_laporan == '1,0,0,0,0,0,0,0,0,0,0,0' || explode(',', $skpd_jab->status_laporan)[1] == '1')
            <a href="/hasil-kerja/{{$skpd_jab->id}}" class="nav-link">
            @else
            <a href="#" class="nav-link disabled">
            @endif
                <span class="fa-stack fa-md">
                  @if($linkaktif == 2)
                    <i class="fas fa-circle fa-stack-2x" style="color:#e0ad2c;"></i>
                  @else
                    <i class="far fa-circle fa-stack-2x" style="color:#e0ad2c;"></i>
                  @endif
                    <b class="fa-stack-1x">2</b>
                </span>
              <p>
                Hasil Kerja
              </p>
            </a>
          </li>
          <li class="nav-item">
            @if($skpd_jab->status_laporan == '1,1,0,0,0,0,0,0,0,0,0,0' || explode(',', $skpd_jab->status_laporan)[2] == '1')
            <a href="/bahan-kerja/{{$skpd_jab->id}}" class="nav-link">
            @else
            <a href="#" class="nav-link disabled">
            @endif
                <span class="fa-stack fa-md">
                  @if($linkaktif == 3)
                    <i class="fas fa-circle fa-stack-2x" style="color:#e0ad2c;"></i>
                  @else
                    <i class="far fa-circle fa-stack-2x" style="color:#e0ad2c;"></i>
                  @endif
                    <b class="fa-stack-1x">3</b>
                </span>
              <p>
                Bahan Kerja
              </p>
            </a>
          </li>
          <li class="nav-item">
            @if($skpd_jab->status_laporan == '1,1,1,0,0,0,0,0,0,0,0,0' || explode(',', $skpd_jab->status_laporan)[3] == '1')
            <a href="/perangkat-kerja/{{$skpd_jab->id}}" class="nav-link">
            @else
            <a href="#" class="nav-link disabled">
            @endif
                <span class="fa-stack fa-md">
                  @if($linkaktif == 4)
                    <i class="fas fa-circle fa-stack-2x" style="color:#e0ad2c;"></i>
                  @else
                    <i class="far fa-circle fa-stack-2x" style="color:#e0ad2c;"></i>
                  @endif
                    <b class="fa-stack-1x">4</b>
                </span>
              <p>
                Perangkat Kerja
              </p>
            </a>
          </li>
          <li class="nav-item">
            @if($skpd_jab->status_laporan == '1,1,1,1,0,0,0,0,0,0,0,0' || explode(',', $skpd_jab->status_laporan)[4] == '1')
            <a href="/tanggung-jawab/{{$skpd_jab->id}}" class="nav-link">
            @else
            <a href="#" class="nav-link disabled">
            @endif
                <span class="fa-stack fa-md">
                  @if($linkaktif == 5)
                    <i class="fas fa-circle fa-stack-2x" style="color:#e0ad2c;"></i>
                  @else
                    <i class="far fa-circle fa-stack-2x" style="color:#e0ad2c;"></i>
                  @endif
                    <b class="fa-stack-1x">5</b>
                </span>
              <p>
                Tanggung Jawab
              </p>
            </a>
          </li>
          <li class="nav-item">
            @if($skpd_jab->status_laporan == '1,1,1,1,1,0,0,0,0,0,0,0' || explode(',', $skpd_jab->status_laporan)[5] == '1')
            <a href="/wewenang/{{$skpd_jab->id}}" class="nav-link">
            @else
            <a href="#" class="nav-link disabled">
            @endif
                <span class="fa-stack fa-md">
                  @if($linkaktif == 6)
                    <i class="fas fa-circle fa-stack-2x" style="color:#e0ad2c;"></i>
                  @else
                    <i class="far fa-circle fa-stack-2x" style="color:#e0ad2c;"></i>
                  @endif
                    <b class="fa-stack-1x">6</b>
                </span>
              <p>
                Wewenang
              </p>
            </a>
          </li>
          <li class="nav-item">
            @if($skpd_jab->status_laporan == '1,1,1,1,1,1,0,0,0,0,0,0' || explode(',', $skpd_jab->status_laporan)[6] == '1')
            <a href="/korelasi-jabatan/{{$skpd_jab->id}}" class="nav-link">
            @else
            <a href="#" class="nav-link disabled">
            @endif
                <span class="fa-stack fa-md">
                  @if($linkaktif == 7)
                    <i class="fas fa-circle fa-stack-2x" style="color:#e0ad2c;"></i>
                  @else
                    <i class="far fa-circle fa-stack-2x" style="color:#e0ad2c;"></i>
                  @endif
                    <b class="fa-stack-1x">7</b>
                </span>
              <p>
                Korelasi Jabatan
              </p>
            </a>
          </li>
          <li class="nav-item">
            @if($skpd_jab->status_laporan == '1,1,1,1,1,1,1,0,0,0,0,0' || explode(',', $skpd_jab->status_laporan)[7] == '1')
            <a href="/lingkungan-kerja/{{$skpd_jab->id}}" class="nav-link">
            @else
            <a href="#" class="nav-link disabled">
            @endif
                <span class="fa-stack fa-md">
                  @if($linkaktif == 8)
                    <i class="fas fa-circle fa-stack-2x" style="color:#e0ad2c;"></i>
                  @else
                    <i class="far fa-circle fa-stack-2x" style="color:#e0ad2c;"></i>
                  @endif
                    <b class="fa-stack-1x">8</b>
                </span>
              <p>
                Lingkungan Kerja
              </p>
            </a>
          </li>
          <li class="nav-item">
            @if($skpd_jab->status_laporan == '1,1,1,1,1,1,1,1,0,0,0,0' || explode(',', $skpd_jab->status_laporan)[8] == '1')
            <a href="/prestasi-kerja/{{$skpd_jab->id}}" class="nav-link">
            @else
            <a href="#" class="nav-link disabled">
            @endif
                <span class="fa-stack fa-md">
                  @if($linkaktif == 9)
                    <i class="fas fa-circle fa-stack-2x" style="color:#e0ad2c;"></i>
                  @else
                    <i class="far fa-circle fa-stack-2x" style="color:#e0ad2c;"></i>
                  @endif
                    <b class="fa-stack-1x">9</b>
                </span>
              <p>
                Prestasi Kerja
              </p>
            </a>
          </li>
          <li class="nav-item">
            @if($skpd_jab->status_laporan == '1,1,1,1,1,1,1,1,1,0,0,0' || explode(',', $skpd_jab->status_laporan)[9] == '1')
            <a href="/syarat-jabatan/{{$skpd_jab->id}}" class="nav-link">
            @else
            <a href="#" class="nav-link disabled">
            @endif
                <span class="fa-stack fa-md">
                  @if($linkaktif == 10)
                    <i class="fas fa-circle fa-stack-2x" style="color:#e0ad2c;"></i>
                  @else
                    <i class="far fa-circle fa-stack-2x" style="color:#e0ad2c;"></i>
                  @endif
                    <b class="fa-stack-1x">10</b>
                </span>
              <p>
                Syarat Jabatan
              </p>
            </a>
          </li>
          <li class="nav-item">
            @if($skpd_jab->status_laporan == '1,1,1,1,1,1,1,1,1,1,0,0' || explode(',', $skpd_jab->status_laporan)[10] == '1')
            <a href="/kualifikasi-jabatan/{{$skpd_jab->id}}" class="nav-link">
            @else
            <a href="#" class="nav-link disabled">
            @endif
                <span class="fa-stack fa-md">
                  @if($linkaktif == 11)
                    <i class="fas fa-circle fa-stack-2x" style="color:#e0ad2c;"></i>
                  @else
                    <i class="far fa-circle fa-stack-2x" style="color:#e0ad2c;"></i>
                  @endif
                    <b class="fa-stack-1x">11</b>
                </span>
              <p>
                Kualifikasi Jabatan
              </p>
            </a>
          </li>
          <li class="nav-item">
            @if($skpd_jab->status_laporan == '1,1,1,1,1,1,1,1,1,1,1,0' || explode(',', $skpd_jab->status_laporan)[11] == '1')
            <a href="/resiko-bahaya/{{$skpd_jab->id}}" class="nav-link">
            @else
            <a href="#" class="nav-link disabled">
            @endif
                <span class="fa-stack fa-md">
                  @if($linkaktif == 12)
                    <i class="fas fa-circle fa-stack-2x" style="color:#e0ad2c;"></i>
                  @else
                    <i class="far fa-circle fa-stack-2x" style="color:#e0ad2c;"></i>
                  @endif
                    <b class="fa-stack-1x">12</b>
                </span>
              <p>
                Resiko Bahaya
              </p>
            </a>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>