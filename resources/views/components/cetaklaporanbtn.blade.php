@php
$parameters = array([
    'Tugas Pokok', 'Hasil Kerja', 'Bahan Kerja', 'Perangkat Kerja', 'Tanggung Jawab',
    'Wewenang', 'Korelasi Jabatan', 'Lingkungan Kerja', 'Prestasi Kerja', 'Syarat Jabatan',
    'Kualifikasi Jabatan', 'Resiko Bahaya'
]);

$jab = $skpd_jab->jabatan()->get();

if($skpd_jab->status_laporan == '1,1,1,1,1,1,1,1,1,1,1,1') {
        $bisa_cetak = true;
}
else {
        $bisa_cetak = false;
}

if($bisa_cetak == true){
        $tugas = $skpd_jab->tugasPokok()->limit(1)->get();
        $date_created = $tugas[0]->created_at->format('d/m/Y');
}
@endphp
@if($bisa_cetak)
<div style="border: 2px solid #eaeaea;border-radius: 6px;padding: 13px 0px;">
    <div class="row margin-5">
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3" style="display:flex;flex-direction:column;justify-content:center;align-items:center">
                    <img src="{{asset('main/dist/img/dossier.png')}}" class="icon-doc"/>
                    @if($date_created)
                    <p class="date-abk">{{$date_created}}</p>
                    @endif
            </div>
            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="display:flex;flex-direction:column;justify-content: center;">
                    <h6>{{$jab[0]->jabatan}}</h6>
                    <h6 style="font-weight: 100;">{{$nama_skpd}}</h6>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3" style="display:flex;flex-direction:column;justify-content:center;align-items:center">
                    <a class="btn btn-success btn-success-custom" href="/uraian-tugas/{{$id_skpd_jab}}">Edit ABK</a>
            </div>
    </div>
    <hr style="width:85%;">
    <div class="row">
        <div class="col-lg-2 col-md-2 hidden-sm hidden-xs"></div>
        <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12" style="display:flex;flex-direction:column;justify-content:center;align-items:center">
                <a href="/abk/cetak-pdf/{{$skpd_jab->id}}" class="red-export-abk">Export Kemenpan</a>
        </div>
        <!-- <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6" style="display:flex;flex-direction:column;justify-content:center;align-items:center">
                <a href="/uraian-tugas/{{$id_skpd_jab}}" class="red-export-abk">Export Kemendagri</a>
        </div> -->
        <div class="col-lg-2 col-md-2 hidden-sm hidden-xs"></div>
    </div>
</div>
@else
<a class="btn btn-info mb-3" href="/uraian-tugas/{{$id_skpd_jab}}">Input Laporan Beban Kerja</a> 
@endif