<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-light-primary " style="box-shadow:0 2px 1px rgba(0,0,0,.15),0 10px 10px rgba(0,0,0,.12)!important">
<!-- Brand Logo -->
<a href="/" class="brand-link " id="sidebar-header">
    <img src="{{asset('main/dist/img/sgrjlogo.png')}}" class="brand-image img-circle elevation-3" style=" float:unset">
    <span class="brand-text font-weight-light">Anjab ABK</span>
</a>

<!-- Sidebar -->
<div class="sidebar">
    <!-- Sidebar Menu -->
    <nav class="mt-2">
    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
            with font-awesome or any other icon font library -->
        <li class="nav-header">M E N U</li>
        <li class="nav-item">
        @if ($sideaktif == 1)
            <a href="/" class="nav-link" id="nav-link-custom">
                <img  src="{{asset('main/dist/img/side-icon/Group-245-active.svg')}}" class="icon-sidebar">
                <p class="active">
                Dashboard
                </p>
            </a>
        @else
            <a href="/" class="nav-link" id="nav-link-custom">
                <img class="icon-sidebar" src="{{asset('main/dist/img/side-icon/Group-245.svg')}}">
                <p>
                Dashboard
                </p>
            </a>
        @endif
        </li>
        <li class="nav-item">
        @if ($sideaktif == 2)  
            @if(Auth::user()->role_id == 1)
                <a href="/abk" class="nav-link" id="nav-link-custom">
                    <img class="icon-sidebar" src="{{asset('main/dist/img/side-icon/Group-246-active.svg')}}">
                    <p class="active">
                    Anjab ABK
                    </p>
                </a>
            @elseif(Auth::user()->role_id == 3)
                <a href="/abk/detailskpd/{{Auth::user()->id_skpd}}" class="nav-link" id="nav-link-custom">
                    <img class="icon-sidebar" src="{{asset('main/dist/img/side-icon/Group-246-active.svg')}}">
                    <p class="active">
                    Anjab ABK
                    </p>
                </a>
            @endif
        </li>
        @else
            @if(Auth::user()->role_id == 1)
                <a href="/abk" class="nav-link" id="nav-link-custom">
                    <img class="icon-sidebar" src="{{asset('main/dist/img/side-icon/Group-246.svg')}}">
                    <p>
                    Anjab ABK
                    </p>
                </a>
            @elseif(Auth::user()->role_id == 3)
                <a href="/abk/detailskpd/{{Auth::user()->id_skpd}}" class="nav-link" id="nav-link-custom">
                    <img class="icon-sidebar" src="{{asset('main/dist/img/side-icon/Group-246.svg')}}">
                    <p>
                    Anjab ABK
                    </p>
                </a>
            @endif
        </li>
        @endif
        <!-- <li class="nav-item" >
            <a href="/" class="nav-link" id="nav-link-custom">
                <img class="icon-sidebar" src="{{asset('main/dist/img/side-icon/Group-247.svg')}}">
                <p>
                Laporan
                </p>
            </a>
        </li> -->
        @if(Auth::user()->role->master == 1)
        @if ($sideaktif == 3)  
        <li class="nav-item">
            <a href="/master" class="nav-link" id="nav-link-custom">
                <img class="icon-sidebar" src="{{asset('main/dist/img/side-icon/Group-248-active.svg')}}">
                <p class="active">
                Master
                </p>
            </a>
        </li>
        @else
        <li class="nav-item">
            <a href="/master" class="nav-link" id="nav-link-custom">
                <img class="icon-sidebar" src="{{asset('main/dist/img/side-icon/Group-248.svg')}}">
                <p>
                Master
                </p>
            </a>
        </li>
        @endif
        @endif

        @if(Auth::user()->role->master == 1)
        @if ($sideaktif == 4)  
        <li class="nav-item">
            <a href="/opdjab" class="nav-link" id="nav-link-custom">
                <img class="icon-sidebar" src="{{asset('main/dist/img/side-icon/Group-247-active.svg')}}">
                <p class="active">
                OPD Jabatan
                </p>
            </a>
        </li>
        @else
        <li class="nav-item">
            <a href="/opdjab" class="nav-link" id="nav-link-custom">
                <img class="icon-sidebar" src="{{asset('main/dist/img/side-icon/Group-247.svg')}}">
                <p>
                OPD Jabatan
                </p>
            </a>
        </li>
        @endif
        @endif

        <li class="nav-item">
            <a data-toggle="modal" data-target="#modal-xl-logout"  class="nav-link"  id="nav-link-custom">
                <img class="icon-sidebar" src="{{asset('main/dist/img/side-icon/Group-249.svg')}}">
                <p>
                Logout
                </p>
            </a>
        </li>
    </ul>
    </nav>
    <!-- /.sidebar-menu -->
</div>
<!-- /.sidebar -->
</aside>