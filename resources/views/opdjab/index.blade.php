@extends('layouts.main')

@section('title' , 'Master')
@section('style')
<meta name="csrf-token" content="{{ csrf_token() }}">
@endsection
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h5 class="m-0">OPD Jabatan</h5>
            <ol class="breadcrumb float-sm-left">
              <li class="breadcrumb-item active">Jenis Jabatan</li>
            </ol>
          </div><!-- /.col -->
          <div class="col-sm-6">
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        @if(Session::has('success'))
        <div class="alert alert-success" style="padding: 5px 10px;background-color: #d4edda;border-color: #c3e6cb;color: #155724;" role="alert">
            <i class="far fa-check-circle" style="margin-right:5px;"></i>{{ Session::get('success') }}<a href="#" class="close" data-dismiss="alert" aria-label="close"><i class="far fa-times-circle"></i></a>
        </div>
        @elseif(Session::has('fail'))
        <div class="alert alert-success" style="padding: 5px 10px;background-color: red;border-color: #c3e6cb;color: #155724;" role="alert">
            <i class="far fa-check-circle" style="margin-right:5px;"></i>{{ Session::get('fail') }}<a href="#" class="close" data-dismiss="alert" aria-label="close"><i class="far fa-times-circle"></i></a>
        </div>
        @endif
        <div class="row">
            @include('opdjab.sidebar3')
            <div class="col-xs-12 xol-sm-10 col-md-10 col-lg-10" id="sideright-master-abk">
                <div class="tab-content" id="nav-tabContent">
                    <!-- bodylist -->
                    <div class="tab-pane fade show active" id="list-tugas">
                        <div id="message-update1"></div>
                            <div class="row">
                                <div class="col-lg">
                                    <div class="card">
                                        <div class="card-header border-0">
                                            <div class="card-tools">
                                            <a href="#" class="btn btn-tool btn-sm" data-toggle="modal" data-target="#modalcreatejenisjabatan">
                                                <i class="fas fa-user-plus"></i> Tambah Jenis Jabatan
                                            </a>
                                            </div>
                                        </div>
                                        <div class="card-body table-responsive p-0">
                                            <table class="table table-striped table-valign-middle">
                                            <thead>
                                                <tr>
                                                <th>ID</th>
                                                <th>Nama</th>
                                                <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($jenisjabatans as $jenisjab)
                                                <tr>
                                                <td>{{$jenisjab->id}}</td>
                                                <td>{{$jenisjab->nama_jenis_jabatan}}</td>
                                                <td>
                                                    <button class="btn" data-toggle="modal" data-target="#modalupdatejenisjabatan" onclick="clickUpdateJenisJab({{$jenisjab->id}})"><i class="fas fa-edit"></i></button>
                                                    <button class="btn" data-toggle="modal" data-target="#modaldeletejenisjabatan" onclick="clickDeleteJenisJab({{$jenisjab->id}})"><i class="far fa-trash-alt"></i></button>
                                                </td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <!-- /.card -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      </div>
    </section>
    <!-- Modal Create Jenis Jabatan -->
    <div class="modal fade" id="modalcreatejenisjabatan" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">Jenis Jabatan Baru</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <div class="card-body">
            <form action="{{route('create-jenis-jab')}}" class="form" method="POST">
                @csrf
                <div class="form-group">
                <label for="nama_jenis_jabatan">Nama</label>
                <input type="text" class="form-control" id="nama_jenis_jabatan" name="nama_jenis_jabatan" placeholder="Nama Jenis Jabatan" autocomplete="off">
                </div>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-primary" type="submit">Simpan</button>
            </form>
            </div>
            <!-- /.card-body -->
        </div>
        </div>
    </div>
    </div>
    <!-- /.Modal Create Jenis Jabatan -->
    <!-- Modal Update Jenis Jabatan -->
    <div class="modal fade" id="modalupdatejenisjabatan" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">Jenis Jabatan Update</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <div class="card-body">
            <form action="" class="form" method="POST" id="form-update-jenisjabatan">
                @csrf
                <div class="form-group">
                <label for="nama_jenis_jabatan2">Nama</label>
                <input type="text" class="form-control" id="nama_jenis_jabatan2" name="nama_jenis_jabatan" placeholder="Nama" autocomplete="off">
                </div>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-primary" type="submit">Simpan</button>
            </form>
            </div>
            <!-- /.card-body -->
        </div>
        </div>
    </div>
    </div>
    <!-- /.Modal Update Jenis Jabatan -->
    <!-- Modal Delete Jenis Jabatan -->
    <div class="modal fade" id="modaldeletejenisjabatan" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">Anda yakin ingin menghapus jenis jabatan ini?</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <div class="card-body">
            <form action="" id="form-delete-jenisjabatan" method="POST">
                @csrf
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-primary" type="submit">Yakin</button>
            </form>
            </div>
            <!-- /.card-body -->
        </div>
        </div>
    </div>
    </div>
    <!-- /.Modal Delete Jenis Jabatan -->
</div>
@endsection

@section('script')
<script>
function clickDeleteJenisJab(id) {
    $('#form-delete-jenisjabatan').attr('action', '/opdjab/delete-jenis-jabatan/'+id);
}
function clickUpdateJenisJab(id) {
    $.get('/opdjab/get-jenis-jabatan-detail/'+id, function(data) {
        $('#form-update-jenisjabatan').attr('action', '/opdjab/update-jenis-jabatan/'+id);
        $('#nama_jenis_jabatan2').val(data.nama_jenis_jabatan);
    });
}
</script>
@endsection