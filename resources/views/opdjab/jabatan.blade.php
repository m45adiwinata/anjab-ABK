@extends('layouts.main')

@section('title' , 'Master')
@section('style')
<meta name="csrf-token" content="{{ csrf_token() }}">
@endsection
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h5 class="m-0">OPD Jabatan</h5>
            <ol class="breadcrumb float-sm-left">
              <li class="breadcrumb-item active">Jabatan</li>
            </ol>
          </div><!-- /.col -->
          <div class="col-sm-6">
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        @if(Session::has('success'))
        <div class="alert alert-success" style="padding: 5px 10px;background-color: #d4edda;border-color: #c3e6cb;color: #155724;" role="alert">
            <i class="far fa-check-circle" style="margin-right:5px;"></i>{{ Session::get('success') }}<a href="#" class="close" data-dismiss="alert" aria-label="close"><i class="far fa-times-circle"></i></a>
        </div>
        @elseif(Session::has('fail'))
        <div class="alert alert-success" style="padding: 5px 10px;background-color: red;border-color: #c3e6cb;color: white;" role="alert">
            <i class="far fa-check-circle" style="margin-right:5px;"></i>{{ Session::get('fail') }}<a href="#" class="close" data-dismiss="alert" aria-label="close"><i class="far fa-times-circle"></i></a>
        </div>
        @endif
        <div class="row">
            @include('opdjab.sidebar3')
            <div class="col-xs-12 xol-sm-10 col-md-10 col-lg-10" id="sideright-master-abk">
                <div class="tab-content" id="nav-tabContent">
                    <!-- bodylist -->
                    <div class="tab-pane fade show active" id="list-tugas">
                        <div id="message-update1"></div>
                            <div class="row">
                                <div class="col-lg">
                                    <div class="card">
                                        <div class="card-header border-0">
                                            <div class="card-tools">
                                                <div class="row">
                                                    <div class="col">
                                                        <div class="input-group input-group-sm" style="width: 150px;">
                                                            <input type="text" id="table_search_jabatan" class="form-control float-right" placeholder="Search" value="{{isset($_GET['nama_jabatan']) ? $_GET['nama_jabatan'] : ''}}">
                                                            <div class="input-group-append">
                                                                <button class="btn btn-default" id="submit-search-jabatan">
                                                                    <i class="fas fa-search"></i>
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col">
                                                        <a href="#" class="btn btn-tool btn-sm" data-toggle="modal" data-target="#modalcreatejabatan">
                                                            <i class="fas fa-user-plus"></i> Tambah Jabatan
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="card-body table-responsive p-0">
                                            <table class="table table-striped table-valign-middle">
                                                <thead>
                                                    <tr>
                                                    <th>ID</th>
                                                    <th>Jenis</th>
                                                    <th>Nama Jabatan</th>
                                                    <th>Lvl. Sekretaris</th>
                                                    <th>Eselon</th>
                                                    <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach($jabatans as $jab)
                                                    <tr>
                                                    <td>{{$jab->id}}</td>
                                                    <td>{{$jab->jenisJab()->first()->nama_jenis_jabatan}}</td>
                                                    <td>{{$jab->jabatan}}</td>
                                                    <td>{{$jab->is_asisten == 1 ? 'Ya': 'Tidak'}}</td>
                                                    <td>{{$jab->eselon()->first()->eselon}}</td>
                                                    <td>
                                                        <button class="btn" data-toggle="modal" data-target="#modalupdatejabatan" onclick="clickUpdateJabatan({{$jab->id}})"><i class="fas fa-edit"></i></button>
                                                        <button class="btn" data-toggle="modal" data-target="#modaldeletejabatan" onclick="clickDeleteJabatan({{$jab->id}})"><i class="far fa-trash-alt"></i></button>
                                                    </td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                            {{ $jabatans->onEachSide(2)->links() }}
                                        </div>
                                    </div>
                                    <!-- /.card -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      </div>
    </section>
    <!-- Modal Create Jabatan -->
    <div class="modal fade" id="modalcreatejabatan" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">Jabatan Baru</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <div class="card-body">
            <form action="{{route('create-jabatan')}}" class="form" method="POST">
                @csrf
                <div class="form-group">
                <label>Jenis Jabatan</label>
                <select class="form-control select2" name="id_jenis_jabatan" style="width: 100%;">
                    @foreach($jenisjabatans as $key => $jenisjab)
                    @if($key==0)
                    <option value="{{$jenisjab->id}}" selected="selected">{{$jenisjab->nama_jenis_jabatan}}</option>
                    @else
                    <option value="{{$jenisjab->id}}">{{$jenisjab->nama_jenis_jabatan}}</option>
                    @endif
                    @endforeach
                </select>
                </div>
                <div class="form-group">
                <label for="jabatan">Nama Jabatan</label>
                <input type="text" class="form-control" id="jabatan" name="jabatan" placeholder="Nama Jabatan" autocomplete="off">
                </div>
                <div class="form-group">
                <label for="is_asisten">Level Sekretaris?</label>
                <select class="form-control select2" name="is_asisten" style="width: 100%;">
                    <option value="0">Tidak</option>
                    <option value="1">Ya</option>
                </select>
                </div>
                <div class="form-group">
                <label>Eselon</label>
                <select class="form-control select2" name="id_eselon" id="id_eselon_jabatan" style="width: 100%;">
                    @foreach($eselons as $eselon)
                    <option value="{{$eselon->id}}">{{$eselon->eselon}}</option>
                    @endforeach
                </select>
                </div>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-primary" type="submit">Simpan</button>
            </form>
            </div>
            <!-- /.card-body -->
        </div>
        </div>
    </div>
    </div>
    <!-- /.Modal Create Jabatan -->
    <!-- Modal Update Jabatan -->
    <div class="modal fade" id="modalupdatejabatan" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">Jabatan Update</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <div class="card-body">
            <form action="" class="form" method="POST" id="form-update-jabatan">
                @csrf
                <div class="form-group">
                <label>Jenis Jabatan</label>
                <select class="form-control select2" name="id_jenis_jabatan" id="id_jenis_jabatan2" style="width: 100%;">
                    @foreach($jenisjabatans as $key => $jenisjab)
                    <option value="{{$jenisjab->id}}">{{$jenisjab->nama_jenis_jabatan}}</option>
                    @endforeach
                </select>
                </div>
                <div class="form-group">
                <label for="jabatan2">Jabatan</label>
                <input type="text" class="form-control" id="jabatan2" name="jabatan" placeholder="Nama Jabatan" autocomplete="off">
                </div>
                <div class="form-group">
                <label>Level Sekretaris?</label>
                <select class="form-control select2" name="is_asisten" id="is_asisten" style="width: 100%;">
                    <option value="0">Tidak</option>
                    <option value="1">Ya</option>
                </select>
                </div>
                <div class="form-group">
                <label>Eselon</label>
                <select class="form-control select2" name="id_eselon" id="id_eselon_jabatan2" style="width: 100%;">
                    @foreach($eselons as $eselon)
                    <option value="{{$eselon->id}}">{{$eselon->eselon}}</option>
                    @endforeach
                </select>
                </div>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-primary" type="submit">Simpan</button>
            </form>
            </div>
            <!-- /.card-body -->
        </div>
        </div>
    </div>
    </div>
    <!-- /.Modal Update Jabatan -->
    <!-- Modal Delete Jabatan -->
    <div class="modal fade" id="modaldeletejabatan" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLongTitle">Anda yakin ingin menghapus jabatan ini?</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <div class="card-body">
            <form action="" id="form-delete-jabatan" method="POST">
                @csrf
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-primary" type="submit">Yakin</button>
            </form>
            </div>
            <!-- /.card-body -->
        </div>
        </div>
    </div>
    </div>
    <!-- /.Modal Delete Jabatan-->
</div>
@endsection

@section('script')
<script>
function clickDeleteJabatan(id) {
    $('#form-delete-jabatan').attr('action', '/opdjab/delete-jabatan/'+id);
}
function clickUpdateJabatan(id) {
    $.get('/opdjab/get-jabatan-detail/'+id, function(data) {
        $('#form-update-jabatan').attr('action', '/opdjab/update-jabatan/'+id);
        $('#id_jenis_jabatan2').val(data.id_jenis_jabatan).trigger('change');
        $('#jabatan2').val(data.jabatan);
        $('#is_asisten').val(data.is_asisten).trigger('change');
        $('#id_eselon_jabatan2').val(data.id_eselon).trigger('change');
    });
}
$(document).ready(function() {
    $("#submit-search-jabatan").click(function() {
        if($('#table_search_jabatan').val() == "") {
            window.location.href = '/opdjab/jabatan';
        }
        else {
            window.location.href = '/opdjab/jabatan?nama_jabatan='+$('#table_search_jabatan').val();
        }
    });
    $('#table_search_jabatan').keypress(function (e) {
        var key = e.which;
        if(key == 13)  // the enter key code
        {
            if($(this).val() == "") {
                window.location.href = '/opdjab/jabatan';
            }
            else {
                window.location.href = '/opdjab/jabatan?nama_jabatan='+$(this).val();
            }
            return false;
        }
    });
});
</script>
@endsection