@extends('layouts.main')

@section('title' , 'Master')
@section('style')
<meta name="csrf-token" content="{{ csrf_token() }}">
@endsection
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h5 class="m-0">OPD Jabatan</h5>
            <ol class="breadcrumb float-sm-left">
              <li class="breadcrumb-item active">OPD Jabatan</li>
            </ol>
          </div><!-- /.col -->
          <div class="col-sm-6">
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        @if(Session::has('success'))
        <div class="alert alert-success" style="padding: 5px 10px;background-color: #d4edda;border-color: #c3e6cb;color: #155724;" role="alert">
            <i class="far fa-check-circle" style="margin-right:5px;"></i>{{ Session::get('success') }}<a href="#" class="close" data-dismiss="alert" aria-label="close"><i class="far fa-times-circle"></i></a>
        </div>
        @elseif(Session::has('fail'))
        <div class="alert alert-success" style="padding: 5px 10px;background-color: red;border-color: #c3e6cb;color: white;" role="alert">
            <i class="far fa-check-circle" style="margin-right:5px;"></i>{{ Session::get('fail') }}<a href="#" class="close" data-dismiss="alert" aria-label="close"><i class="far fa-times-circle"></i></a>
        </div>
        @endif
        <div class="row">
            @include('opdjab.sidebar3')
            <div class="col-xs-12 xol-sm-10 col-md-10 col-lg-10" id="sideright-master-abk">
                <div class="tab-content" id="nav-tabContent">
                    <!-- bodylist -->
                    <div class="tab-pane fade show active">
                        <div id="message-update1"></div>
                            <div class="row">
                                <div class="col-lg">
                                    <div class="card">
                                        <div class="card-header border-0">
                                            <div class="card-tools">
                                            <div class="row">
                                                <div class="col">
                                                <div class="input-group input-group-sm" style="width: 170px;">
                                                    <button class="btn btn-tool btn-sm" id="btn-filter-opdjab" data-toggle="modal" data-target="#modalfilterskpd">
                                                    <i class="fas fa-filter"></i>
                                                    </button>
                                                    <input type="text" id="table_search_opdjab" class="form-control float-right" placeholder="Search Jabatan" value="{{isset($_GET['nama_opdjab']) ? $_GET['nama_opdjab'] : ''}}">
                                                    <div class="input-group-append">
                                                    <button class="btn btn-default" id="submit-search-opdjab">
                                                        <i class="fas fa-search"></i>
                                                    </button>
                                                    </div>
                                                </div>
                                                </div>
                                                <div class="col">
                                                <a href="" class="btn btn-tool btn-sm" data-toggle="modal" data-target="#modalcreateopdjab" id="btn-tambah-opdjab">
                                                    <i class="fas fa-user-plus"></i> Tambah OPD Jab
                                                </a>
                                                </div>
                                            </div>
                                            </div>
                                        </div>
                                        <!-- /.card-header -->
                                        <div class="card-body table-responsive p-0" id="list-table-skpdjab">
                                            <table class="table table-striped table-valign-middle">
                                            <thead>
                                                <tr>
                                                <th>ID</th>
                                                <th>Nama Jabatan</th>
                                                <th>Nama OPD</th>
                                                <th>Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($opdjabs as $opdjab)
                                                <tr>
                                                <td>{{$opdjab->id}}</td>
                                                <td>{{$opdjab->jabatan()->first() ? $opdjab->jabatan()->first()->jabatan : ''}}</td>
                                                <td>{{$opdjab->skpd()->first() ? $opdjab->skpd()->first()->nama_skpd : ''}}</td>
                                                <td>
                                                    <button class="btn" data-toggle="modal" data-target="#modalupdateopdjab" onclick="clickUpdateOPDJab({{$opdjab->id}}, {{$opdjab->id_jabatan}})"><i class="fas fa-edit"></i></button>
                                                    <button class="btn" data-toggle="modal" data-target="#modaldeleteopdjab" onclick="clickDeleteOPDJab({{$opdjab->id}})"><i class="far fa-trash-alt"></i></button>
                                                </td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                            </table>
                                            {{ $opdjabs->onEachSide(2)->links() }}
                                        </div>
                                        <!-- /.card-body -->
                                    </div>
                                    <!-- /.card -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      </div>
    </section>
    <!-- Modal Create OPD Jabatan -->
    <div class="modal fade" id="modalcreateopdjab" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">OPD Jabatan Baru</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <div class="card-body">
            <form action="{{route('create-opdjab')}}" class="form" method="POST">
                @csrf
                <div class="form-group">
                <label>OPD</label>
                <select class="form-control select2" name="id_skpd" id="id_skpd_opdjab_new" style="width: 100%;">
                    @foreach($skpd_all as $key => $skpd)
                    <option value="{{$skpd->id}}">{{$skpd->nama_skpd}}</option>
                    @endforeach
                </select>
                </div>
                <div class="form-group">
                <label>Jabatan</label>
                <select class="form-control select2" name="id_jabatan" id="id_jabatan_opdjab_new" style="width: 100%;">
                </select>
                </div>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-primary" type="submit">Simpan</button>
            </form>
            </div>
            <!-- /.card-body -->
        </div>
        </div>
    </div>
    </div>
    <!-- /.Modal -->

    <!-- Modal Update OPD Jabatan -->
    <div class="modal fade" id="modalupdateopdjab" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">OPD Jabatan Update</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <div class="card-body">
            <form action="{{route('update-opdjab')}}" class="form" method="POST">
                @csrf
                <input type="hidden" name="id" id="id_opdjab">
                <div class="form-group">
                <label>OPD</label>
                <select class="form-control select2" name="id_skpd" id="id_skpd_opdjab" style="width: 100%;">
                    @foreach($skpd_all as $key => $skpd)
                    <option value="{{$skpd->id}}">{{$skpd->nama_skpd}}</option>
                    @endforeach
                </select>
                </div>
                <div class="form-group">
                <label>Jabatan</label>
                <select class="form-control select2" name="id_jabatan" id="id_jabatan_opdjab" style="width: 100%;">
                </select>
                </div>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-primary" type="submit">Simpan</button>
            </form>
            </div>
            <!-- /.card-body -->
        </div>
        </div>
    </div>
    </div>
    <!-- /.Modal -->

    <!-- Modal Delete OPD Jabatan -->
    <div class="modal fade" id="modaldeleteopdjab" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">Anda yakin ingin menghapus OPD Jabatan ini?<br><span id="span-opdjab"></span></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <div class="card-body">
            <form action="{{route('hapus-opdjab')}}" method="POST">
                @csrf
                <input type="hidden" value="" id="id_opdjab2" name="id">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                <button type="submit" class="btn btn-primary" type="submit">Yakin</button>
            </form>
            </div>
            <!-- /.card-body -->
        </div>
        </div>
    </div>
    </div>
    <!-- /.Modal -->
    @include('opdjab.filter')
</div>
@endsection

@section('script')
<script>
    function clickDeleteOPDJab(id) {
        $.get('/opdjab/get-opdjab-detail/'+id, function(data) {
            $('#span-opdjab').html(data.nama_jabatan+' - '+data.nama_skpd);
            $('#id_opdjab2').val(data.id);
        });
    }
    function clickUpdateOPDJab(id, id_jabatan) {
        $.get('/opdjab/get-opdjab-detail/'+id, function(data) {
            $('#id_skpd_opdjab').val(data.id_skpd).trigger('change');
            $('#id_opdjab').val(id);
            $.get('/api/opdjab/get-jabatan-opd-tersisa?id_skpd='+$('#id_skpd_opdjab').val()+'&id='+id, function(jabs) {
                $('#id_jabatan_opdjab').html('');
                $.each(jabs, function( key, value ) {
                    if(key == id_jabatan) {
                        $('#id_jabatan_opdjab').append('<option value="'+key+'" selected="selected">'+value+'</option>');
                    }
                    else {
                        $('#id_jabatan_opdjab').append('<option value="'+key+'">'+value+'</option>');
                    }
                });
            });
        });
    }
    $(document).ready(function() {
        $('#btn-tambah-opdjab').click(function() {
            $.get('/api/opdjab/get-jabatan-opd-tersisa?id_skpd='+$('#id_skpd_opdjab_new').val(), function(data) {
                $('#id_jabatan_opdjab_new').html('');
                $.each(data, function( key, value ) {
                    $('#id_jabatan_opdjab_new').append('<option value="'+key+'">'+value+'</option>');
                });
            });
        });
        $('#id_skpd_opdjab_new').change(function() {
            $.get('/api/opdjab/get-jabatan-opd-tersisa?id_skpd='+$(this).val(), function(data) {
                $('#id_jabatan_opdjab_new').html('');
                $.each(data, function( key, value ) {
                    $('#id_jabatan_opdjab_new').append('<option value="'+key+'">'+value+'</option>');
                });
            });
        });
        $('#id_skpd_opdjab').change(function() {
            $.get('/api/opdjab/get-jabatan-opd-tersisa?id_skpd='+$(this).val()+'&id='+$('#id_opdjab').val(), function(data) {
                $('#id_jabatan_opdjab').html('');
                $.each(data, function( key, value ) {
                    $('#id_jabatan_opdjab').append('<option value="'+key+'">'+value+'</option>');
                });
            });
        });
        $('#btn-filter-opdjab').click(function() {
            var search_opdjab = $('#table_search_opdjab').val();
            if(search_opdjab != "") {
                $('#hidden_search_opdjab').val(search_opdjab);
                $('#hidden_search_opdjab').attr('name', 'nama_opdjab');
            }
        });
        $('#select_filter_skpd').change(function() {
            var selected = $(this).select2('data');
            var text = "";
            $.each(selected, function(key, value) {
                text += value.id+",";
            });
            $('#selected').val(text);
        });
        $("#submit-search-opdjab").click(function() {
            if($('#table_search_opdjab').val() == "") {
                window.location.href = '/opdjab/opd-jabatan';
            }
            else {
                window.location.href = '/opdjab/opd-jabatan?nama_opdjab='+$('#table_search_opdjab').val();
            }
        });
        $('#table_search_opdjab').keypress(function (e) {
            var key = e.which;
            if(key == 13)  // the enter key code
            {
                if($(this).val() == "") {
                    window.location.href = '/opdjab/opd-jabatan';
                }
                else {
                    window.location.href = '/opdjab/opd-jabatan?nama_opdjab='+$(this).val();
                }
                return false;
            }
        });
    });
</script>
@endsection