@extends('layouts.main')

@section('title' , 'Master')
@section('style')
<meta name="csrf-token" content="{{ csrf_token() }}">
@endsection
@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h5 class="m-0">OPD Jabatan</h5>
            <ol class="breadcrumb float-sm-left">
              <li class="breadcrumb-item active">Jenis Jabatan</li>
            </ol>
          </div><!-- /.col -->
          <div class="col-sm-6">
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        @if(Session::has('success'))
        <div class="alert alert-success" style="padding: 5px 10px;background-color: #d4edda;border-color: #c3e6cb;color: #155724;" role="alert">
            <i class="far fa-check-circle" style="margin-right:5px;"></i>{{ Session::get('success') }}<a href="#" class="close" data-dismiss="alert" aria-label="close"><i class="far fa-times-circle"></i></a>
        </div>
        @elseif(Session::has('fail'))
        <div class="alert alert-success" style="padding: 5px 10px;background-color: red;border-color: #c3e6cb;color: #155724;" role="alert">
            <i class="far fa-check-circle" style="margin-right:5px;"></i>{{ Session::get('fail') }}<a href="#" class="close" data-dismiss="alert" aria-label="close"><i class="far fa-times-circle"></i></a>
        </div>
        @endif
        <div class="row">
            @include('opdjab.sidebar3')
            <div class="col-xs-12 xol-sm-10 col-md-10 col-lg-10" id="sideright-master-abk">
                <div class="tab-content" id="nav-tabContent">
                    <!-- bodylist -->
                    <div class="tab-pane fade show active">
                        <div id="message-update1"></div>
                            <div class="row">
                                <div class="col-lg">
                                    <div class="card">
                                        <div class="card-header border-0">
                                            <div class="card-tools">
                                            <div class="row">
                                                <div class="col">
                                                <div class="input-group input-group-sm" style="width: 150px;">
                                                    <input type="text" id="table_search_skpd" class="form-control float-right" placeholder="Search" value="{{isset($_GET['nama_skpd']) ? $_GET['nama_skpd'] : ''}}">
                                                    <div class="input-group-append">
                                                    <button class="btn btn-default" id="submit-search-skpd">
                                                        <i class="fas fa-search"></i>
                                                    </button>
                                                    </div>
                                                </div>
                                                </div>
                                                <div class="col">
                                                <a href="/abk/tambah-skpd" class="btn btn-tool btn-sm">
                                                    <i class="fas fa-user-plus"></i> Tambah OPD
                                                </a>
                                                </div>
                                            </div>
                                            </div>
                                        </div>
                                        <!-- /.card-header -->
                                        <div class="card-body table-responsive p-0" id="list-table-skpd">
                                            <table class="table table-striped table-valign-middle">
                                            <thead>
                                                <tr>
                                                <th>ID</th>
                                                <th>Nama OPD</th>
                                                <th>Jumlah Jabatan</th>
                                                <th>Actions</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($skpds as $skpd)
                                                <tr>
                                                <td>{{$skpd->id}}</td>
                                                <td>{{$skpd->nama_skpd}}</td>
                                                <td>{{$skpd->jabatan_count}}</td>
                                                <td>
                                                    <a class="btn" href="{{route('editskpd', $skpd->id)}}"><i class="fas fa-edit"></i></a>
                                                    <button class="btn" onclick="deleteForm({{$skpd->id}})"><i class="far fa-trash-alt"></i></button>
                                                </td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                            </table>
                                            {{ $skpds->onEachSide(2)->links() }}
                                        </div>
                                        <!-- /.card-body -->
                                    </div>
                                    <!-- /.card -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      </div>
    </section>
    <!-- Modal Delete SKPD -->
    <div class="modal fade" id="modaldeleteskpd" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Hapus Data SKPD</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form method="POST" id="action-del" action="">
                    <div class="modal-body" id="delete-text"></div>
                    @csrf
                    <input type="hidden" id="id-delete" name="id" >
                    <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                    <button type="submit" class="btn btn-primary">Iya, Hapus</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    function deleteForm(id) {
        $.get('/api/get-skpd/'+id, function(data) {
            $('#modaldeleteskpd #delete-text').html('Anda yakin ingin menghapus? '+data.nama_skpd);
            $('#modaldeleteskpd #id-delete').val();
            $('#modaldeleteskpd').modal('show');
            $('#action-del').attr('action', '/abk/hapus-skpd/'+id);
        });
    }
    $(document).ready(function() {
        $("#submit-search-skpd").click(function() {
            if($('#table_search_skpd').val() == "") {
                window.location.href = '/opdjab/opd';
            }
            else {
                window.location.href = '/opdjab/opd?nama_skpd='+$('#table_search_skpd').val();
            }
        });
        $('#table_search_skpd').keypress(function (e) {
            var key = e.which;
            if(key == 13)  // the enter key code
            {
                if($(this).val() == "") {
                    window.location.href = '/opdjab/opd';
                }
                else {
                    window.location.href = '/opdjab/opd?nama_skpd='+$(this).val();
                }
                return false;
            }
        });
    });
</script>
@endsection