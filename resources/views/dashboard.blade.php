@extends('layouts.main')

@section('title' , 'Dashboard Anjab ABK')

@section('content')
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
              <ol class="breadcrumb float-sm-left">
                <li class="breadcrumb-item"><a href="#">Home</a></li>
                <li class="breadcrumb-item active">Dashboard</li>
              </ol>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <!-- <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard</li>
            </ol> -->
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-light">
              <div class="inner">
              @if($count_pegawai_all)
                <h3>{{$count_pegawai_all}}</h3>
              @else
                <h3>0</h3>
              @endif

                <p>Pegawai</p>
              </div>
              <div class="icon">
              <i class="fas fa-users"></i>
              </div>
              <a href="/master#pegawai" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-light">
              <div class="inner">
                <h3>{{$file}}</h3>

                <p>Laporan ABK Selesai</p>
              </div>
              <div class="icon">
                <i class="ion ion-document"></i>
              </div>
              <a href="#" class="small-box-footer" style="height:28px">
                <!-- More info <i class="fas fa-arrow-circle-right"></i> -->
              </a>
            </div>
          </div>
          @if($user_role == 1)
              <!-- ./col -->
              <div class="col-lg-3 col-6">
                <!-- small box -->
                <div class="small-box bg-light">
                  <div class="inner">
                    <h3>{{$count_skpd_all}}</h3>

                    <p>OPD</p>
                  </div>
                  <div class="icon">
                    <i class="fas fa-sitemap"></i>
                  </div>
                  <a href="/abk" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                </div>
              </div>
          @elseif($user_role == 2)
            <!-- <div class="col-lg-3 col-6">
              <div class="small-box bg-light">
                <div class="inner">
                  <h3></h3>

                  <p>OPD</p>
                </div>
                <div class="icon">
                  <i class="fas fa-sitemap"></i>
                </div>
                <a href="/abk" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
              </div>
            </div> -->
          @endif

          @if($user_role == 1)
           <!-- ./col -->
           <div class="col-lg-3 col-6">
            <!-- small box -->
            <div class="small-box bg-light">
              <div class="inner">
                <h3>{{$count_users_all}}</h3>

                <p>User</p>
              </div>
              <div class="icon">
                <i class="fas fa-user-circle"></i>
              </div>
              <a href="/master#user" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          @endif
        </div>
        <!-- /.row -->
        <!-- Main row -->
        <div class="row">
          @if($user_role == 1)
            <div class="col-lg-6 connectedSortable">  
              <div class="card">
                <div class="card-header border-0">
                  <h3 class="card-title">OPD</h3><label class="total-label" data-toggle="tooltip" title="Total SKPD terdaftar">{{$count_skpd_all}}</label>
                  <div class="card-tools">
                    <a href="/abk" class="btn btn-tool btn-sm">
                      View All
                    </a>
                  </div>
                </div>
                <div class="card-body table-responsive p-0">
                  <table class="table table-striped table-valign-middle">
                    <tbody>
                    @if($skpd)
                        @foreach($skpd as $data)
                        <tr>
                          <td>
                            <img src="{{asset('main/dist/img/default-150x150.png')}}" alt="Product 1" class="img-circle img-size-32 mr-2">
                            {{$data->nama_skpd}}
                          </td>
                          <td style="text-align:right;">
                            <a href="/abk/detailskpd/{{$data->id}}" class="text-muted">
                              <i class="fas fa-search"></i>
                            </a>
                          </td>
                        </tr>
                        @endforeach
                    @endif
                    </tbody>
                  </table>
                </div>
              </div>
              <!-- /.card -->
            </div>
          @elseif($user_role == 3)
              <div class="col-lg-6 connectedSortable">  
                  <div class="card">
                    <div class="card-header border-0">
                      <h3 class="card-title">OPD Jabatan</h3><label class="total-label" data-toggle="tooltip" title="Total SKPD terdaftar">@if($jabatans_opd){{count($jabatans_opd)}}@endif</label>
                      <div class="card-tools">
                        <!-- <a href="/abk" class="btn btn-tool btn-sm">
                          View All
                        </a> -->
                      </div>
                    </div>
                    @if($jabatans_opd)
                    @if(count($jabatans_opd) > 0)
                    <div class="card-body table-responsive p-0" style="height:300px;">
                      <table class="table table-head-fixed">
                        <tbody>
                            @foreach($jabatans_opd as $data)
                            <tr>
                              <td>
                                <img src="{{asset('main/dist/img/default-150x150.png')}}" alt="Product 1" class="img-circle img-size-32 mr-2">
                                {{$data->jabatan}}
                              </td>
                            </tr>
                            @endforeach
                        </tbody>
                      </table>
                    </div>
                    @else
                    <div class="card-body table-responsive p-0" style="height:300px; display:flex;justify-content:center;align-items:center;color:#c8c5c2;">
                      Tidak ada Jabatan di OPD {{$skpd->nama_skpd}}
                    </div>
                    @endif
                    @endif
                  </div>
                  <!-- /.card -->
                </div>
          @endif
          <!-- right col (We are only adding the ID to make the widgets sortable)-->
          <div class="col-lg-6 connectedSortable">
            <!-- Jenis Jabatan card -->
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Jenis Jabatan</h3>
                <div class="card-tools">
                  <a href="/master#jabatan" class="btn btn-tool btn-sm">
                    View All
                  </a>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body p-0">
                <table class="table">
                  <tbody>
                  @if($jenis_jabatans)
                    @foreach($jenis_jabatans as $data)
                    <tr>
                      <td>{{$data->nama_jenis_jabatan}}</td>
                      <td style="width: 200px">
                        <!-- <div class="progress progress-xs progress-striped active">
                          <div class="progress-bar bg-success" style="width: 82%"></div>
                        </div> -->
                      </td>
                      <td style="width: 40px"><span class="badge bg-success">{{$data->jabatan_count}}</span></td>
                    </tr>
                    @endforeach
                  @endif
                    <tr>
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- right col -->
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

@endsection
@section('script')
<script>
      $(document).ready(function() {
        $('[data-toggle="tooltip"]').tooltip();   
      });
  </script>

  
  @endsection