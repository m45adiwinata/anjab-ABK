<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubSyaratJabatan extends Model
{
    protected $table = 'sub_syarat_jabatan';

    public function syaratjabatan()
    {
        return $this->hasMany('App\SyaratJabatan', 'id_sub_syarat');
    }

    public static function boot() {
        parent::boot();
        static::deleting(function($subsyarat) {
            $subsyarat->syaratjabatan()->delete();
        });
    }
}
