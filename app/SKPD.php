<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SKPD extends Model
{
    protected $table = 'skpd';

    public function jabatan()
    {
        return $this->belongsToMany('App\Jabatan', 'skpd_jabatan', 'id_skpd', 'id_jabatan')->withPivot('id','id_jab_atasan', 'id_eselon', 'kelas_jab', 'jml_pemangku', 'ikhtisar', 'boleh_edit', 'pegawai_boleh_edit', 'tampil_panduan');
    }

    public function pegawai()
    {
        return $this->hasMany('App\Pegawai', 'id_skpd');
    }

    public function opdjab()
    {
        return $this->hasMany('App\OPDJabatan', 'id_skpd');
    }
}
