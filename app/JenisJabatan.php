<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JenisJabatan extends Model
{
    protected $table = 'jenis_jabatan';
    public $timestamps = 'false';

    public function jabatan()
    {
        return $this->hasMany('App\Jabatan', 'id_jenis_jabatan');
    }
}
