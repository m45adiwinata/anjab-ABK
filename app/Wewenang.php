<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Wewenang extends Model
{
    protected $table = 'wewenang';

    public function SKPDJab()
    {
        return $this->belongsTo('App\SKPDJabatan', 'id_skpd_jab');
    }
}
