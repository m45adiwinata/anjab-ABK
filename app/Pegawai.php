<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pegawai extends Model
{
    protected $table = 'pegawai';

    public function skpdJab()
    {
        return $this->belongsTo('App\SKPDJabatan', 'id_skpd_jab');
    }

    public function skpd()
    {
        return $this->belongsTo('App\SKPD', 'id_skpd');
    }

    public function petaJabatan()
    {
        return $this->hasOne('App\PetaJabatan', 'id_pegawai');
    }

    public function golongan()
    {
        return $this->belongsTo('App\Golongan','id_golongan');
    }
}
