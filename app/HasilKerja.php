<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HasilKerja extends Model
{
    protected $table = 'hasil_uraian_kerja';
    public $timestamps = 'false';
}
