<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PerangkatKerja extends Model
{
    protected $table = 'perangkat_kerja';

    public function SKPDJab()
    {
        return $this->belongsTo('App\SKPDJabatan', 'id_skpd_jab');
    }
}
