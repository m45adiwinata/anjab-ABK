<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LingkunganKerja extends Model
{
    protected $table = 'lingkungan_kerja';
    protected $guarded = [];

    public function SKPDJab()
    {
        return $this->belongsTo('App\SKPDJab', 'id_skpd_jab');
    }

    public function aspek()
    {
        return $this->belongsTo('App\AspekLingKerja', 'id_aspek_lk');
    }
}
