<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PetaJabatan extends Model
{
    protected $table = 'peta_jabatan';

    public function skpdJab()
    {
        return $this->belongsTo('App\SKPDJabatan', 'id_skpd_jab');
    }

    public function pegawai() {
        return $this->belongsTo('App\Pegawai', 'id_pegawai');
    }
}
