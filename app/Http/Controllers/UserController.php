<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;
use App\SKPD;
use App\Pegawai;
use App\SKPDJabatan;
use App\JenisJabatan;
use App\OPDJabatan;
use App\Jabatan;
use DB;


class UserController extends Controller
{
    public function index()
    {
        if(Auth::user()) {
            $user = Auth::user();
            $data['users'] = $user;
            $data['user_role'] = $user->role_id;
            $data['sideaktif'] = 1;
            $data['file'] = 0;
            if($user->role_id == 1){
                $data['count_skpd_all'] = count(SKPD::all());
                $data['count_pegawai_all'] = count(Pegawai::all());
                $data['count_users_all'] = count(User::all());
                $data['skpd'] =  SKPD::withCount('jabatan')->orderBy('jabatan_count','DESC')->limit(5)->get();
                $data['jenis_jabatans'] = JenisJabatan::withCount('jabatan')->get();
                $skpd_jabs = SKPDJabatan::whereNotNull('kelas_jab')->get();
                foreach($skpd_jabs as $skpd_jab){
                    if ($skpd_jab->status_laporan == '1,1,1,1,1,1,1,1,1,1,1,1') {
                        $data['file'] = $data['file'] + 1;
                    }
                }
                
            }elseif($user->role_id == 3){
                $data['sideaktif'] = 1;
                $data['count_pegawai_all'] = count(Pegawai::all()->where('id_skpd',$user->id_skpd));
                $temp_jabatans = SKPDJabatan::where('id_skpd', $user->id_skpd)->get()->pluck('id_jabatan');
                $data['jenis_jabatans'] = JenisJabatan::get();
                foreach($data['jenis_jabatans'] as $jenisjab) {
                    $jumlahjabs = $jenisjab->jabatan()->whereIn('id', $temp_jabatans)->count();
                    $jenisjab->jabatan_count = $jumlahjabs;
                }
                $id = $user->id_skpd;
                $data['jabatans_opd'] = DB::table('opd_jabatan')->join('jabatan', function($join) use ($id)
                {
                    $join->on('opd_jabatan.id_jabatan', '=', 'jabatan.id');
                        
                })->where('opd_jabatan.id_skpd', '=', $id)->get();
                
                $skpd_jabs = SKPDJabatan::whereNotNull('kelas_jab')->where('id_skpd',$user->id_skpd)->get();
                foreach($skpd_jabs as $skpd_jab){
                    if ($skpd_jab->status_laporan == '1,1,1,1,1,1,1,1,1,1,1,1') {
                        $data['file'] = $data['file'] + 1;
                    }
                }
                $data['skpd'] = SKPD::find($user->id_skpd);
            }
            return view('dashboard',$data);
        }
        return redirect('/login');
    }

    public function loginForm()
    {
        if(Auth::user()) {
            return redirect('/');
        }
        return view('auth.login');
    }

    public function login(Request $request)
    {
        $validated = $request->validate([
            'username' => 'required',
            'password' => 'required'
        ]);
        $user = User::where('username', $request->username)->first();
        if($user) {
            if($user->password == md5($request->password)) {
                Auth::login($user);
                return redirect('/');
            }
            else {
                return redirect('/login')->with('fail', 'Password not match!');
            }
        }
        
        return redirect('/login')->with('fail', 'Username not registered!');
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/login');
    }

    public function profile()
    {
        $data['sideaktif'] = 1;
        return view('profile', $data);
    }

    public function updateUser(Request $request)
    {
        $validated = $request->validate([
            'nama' => 'required',
            'email' => 'required',
        ]);
        $data = Auth::user();
        $data->name = $request->nama;
        $data->email = $request->email;
        if(isset($request->password)) {
            $data->password = md5($request->password);
        }
        $data->save();

        return redirect('/userprofile')->with('success', 'User Profile berhasil disimpan.');
    }
}
