<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\HasilKerja;
use App\TugasPokok;
use App\SKPDJabatan;
use App\HasilKerjas;
use App\BahanKerja;
use App\PerangkatKerja;
use App\TanggungJawab;
use App\Wewenang;
use App\KorelasiJabatan;
use App\LingkunganKerja;
use App\AspekLingKerja;
use App\PrestasiKerja;
use App\SyaratJabatan;
use App\SKPD;
use App\Satuan;
use App\Panduan;
use App\SubSyaratJabatan;
use App\PetaJabatan;
use App\OPDJabatan;
use App\Jabatan;
use App\KualifikasiJabatan;
use App\ResikoBahaya;
use App\Pendidikan;
use App\Pegawai;
use Session;
use Auth;

class AnjabController extends Controller
{
    private function authenticateUser()
    {
        if(!Auth::user()) {
            return false;
        }
        
        return true;
    }

    public function janganTampilPanduan($id, $nomor_abk)
    {
        $skpd_jab = SKPDJabatan::find($id);
        $temp = $skpd_jab->tampil_panduan;
        $temp_arr = explode(',', $temp);
        $temp_arr[$nomor_abk-1] = '0';
        $temp = implode(',', $temp_arr);
        $skpd_jab->tampil_panduan = $temp;
        $skpd_jab->save();

        return $temp;
    }

    // <------------------------------ TUGAS POKOK ------------------------------------->
    public function uraianTugas($id_skpd_jab)
    {
        if(!$this->authenticateUser()) {
            return redirect('/');
        }
        $skpd_jab = SKPDJabatan::find($id_skpd_jab);
        $data['hasil_kerjas'] = HasilKerja::get();
        $data['tugas_pokoks'] = $skpd_jab->tugasPokok()->orderBy('urutan')->get();
        $data['total'] = count($data['tugas_pokoks']);
        $data['skpd_jab'] = $skpd_jab;
        $data['linkaktif'] = 1;
        $data['panduan'] = Panduan::where('nomor_abk', 1)->first();
        $panduan_arr = explode(',', $skpd_jab->tampil_panduan);
        if($panduan_arr[0] == '1') {
            $data['tampil_panduan'] = 1;
        }
        else {
            $data['tampil_panduan'] = 0;
        }

        return view('anjab.uraiantugas', $data);
    }
    
    public function getTugasPokok($id)
    {
        $tp = TugasPokok::find($id);

        return $tp;
    }

    private function sumPegawaiDibutuhkan($id_skpd_jab)
    {
        $xpd = SKPDJabatan::find($id_skpd_jab)->tugasPokok()->get()->sum('pegawai_dibutuhkan');
        $x = $xpd - intval($xpd);
        if($x > 0.5) {
            $pd = intval($xpd) + 1;
        }
        else {
            $pd = intval($xpd);
        }
        return $pd;
    }

    public function tambahTugasPokok(Request $request, $id_skpd_jab)
    {
        $skpd_jab = SKPDJabatan::find($id_skpd_jab);
        $gettgspokok = $skpd_jab->tugasPokok()->orderBy('urutan')->get();
        $pd_lama = $this->sumPegawaiDibutuhkan($id_skpd_jab);

        $data = new TugasPokok;
        $data->id_skpd_jab = $id_skpd_jab;
        $data->uraian_tugas = $request->ut_1;
        $data->id_hasil_kerja = $request->hk_1;
        $data->jumlah_hasil = $request->jh_1;
        $data->waktu_selesai = $request->ws_1;
        $data->pegawai_dibutuhkan = $request->pd_1;
        $total = count($gettgspokok) + 1;
        $data->urutan = (int)$total;
        $data->save();
        
        $pd = $this->sumPegawaiDibutuhkan($id_skpd_jab);
        if($pd_lama != $pd) {
            PetaJabatan::where('id_skpd_jab', $skpd_jab->id)->delete();
            Pegawai::where('id_skpd_jab', $skpd_jab->id)->update(['id_skpd_jab' => null]);
            for($i=0;$i<$pd;$i++) {
                $pj = new PetaJabatan;
                $pj->id_skpd_jab = $id_skpd_jab;
                $pj->id_parent_skpd_jab = $skpd_jab->id_jab_atasan;
                $pj->id_skpd = $skpd_jab->id_skpd;
                $pj->save();
            }
        }
        $status_laporan = $skpd_jab->status_laporan;
        $temp = explode(',',$status_laporan);
        if($temp[0] == 0) {
            $temp[0] = '1';
            $skpd_jab->status_laporan = implode(',',$temp);
            $skpd_jab->save();
        }

        Session::flash('success', 'Tambah data Tugas Pokok berhasil');

        return redirect('/uraian-tugas'.'/'.$id_skpd_jab);
    }

    public function deleteTugasPokok(Request $request)
    {
        $tp = TugasPokok::find($request->id);
        $id_skpd_jab = $tp->id_skpd_jab;
        $skpd_jab = SKPDJabatan::find($id_skpd_jab);
        $pd_lama = $this->sumPegawaiDibutuhkan($id_skpd_jab);
        $tp->delete();

        $pd = $this->sumPegawaiDibutuhkan($id_skpd_jab);
        if($pd_lama != $pd) {
            PetaJabatan::where('id_skpd_jab', $skpd_jab->id)->delete();
            Pegawai::where('id_skpd_jab', $skpd_jab->id)->update(['id_skpd_jab' => null]);
            for($i=0;$i<$pd;$i++) {
                $pj = new PetaJabatan;
                $pj->id_skpd_jab = $id_skpd_jab;
                $pj->id_parent_skpd_jab = $skpd_jab->id_jab_atasan;
                $pj->id_skpd = $skpd_jab->id_skpd;
                $pj->save();
            }
        }
        $status_laporan = $skpd_jab->status_laporan;
        $temp = explode(',',$status_laporan);
        if(count($skpd_jab->tugasPokok()->get()) == 0) {
            $temp[0] = '0';
            $skpd_jab->status_laporan = implode(',',$temp);
            $skpd_jab->save();
        }

        Session::flash('success', 'Hapus data Tugas Pokok berhasil');

        return redirect('/uraian-tugas'.'/'.$id_skpd_jab);
    }

    public function updateTugasPokok(Request $request, $id)
    {
        $data = TugasPokok::find($id);
        $skpd_jab = SKPDJabatan::find($data->id_skpd_jab);
        $pd_lama = $this->sumPegawaiDibutuhkan($data->id_skpd_jab);
        $data->uraian_tugas = $request->ut_2;
        $data->id_hasil_kerja = $request->hk_2;
        $data->jumlah_hasil = $request->jh_2;
        $data->waktu_selesai = $request->ws_2;
        $data->pegawai_dibutuhkan = $request->pd_2;
        $data->save();

        $pd = $this->sumPegawaiDibutuhkan($data->id_skpd_jab);
        if($pd_lama != $pd) {
            PetaJabatan::where('id_skpd_jab', $skpd_jab->id)->delete();
            Pegawai::where('id_skpd_jab', $skpd_jab->id)->update(['id_skpd_jab' => null]);
            for($i=0;$i<$pd;$i++) {
                $pj = new PetaJabatan;
                $pj->id_skpd_jab = $skpd_jab->id;
                $pj->id_parent_skpd_jab = $skpd_jab->id_jab_atasan;
                $pj->id_skpd = $skpd_jab->id_skpd;
                $pj->save();
            }
        }

        Session::flash('success', 'Update data Tugas Pokok berhasil');

        return redirect('/uraian-tugas'.'/'.$data->id_skpd_jab);
    }

    public function ubahUrutanTugasPokok(Request $request, $id)
    {
        foreach($request->pos as $pos) {
            $data = TugasPokok::find($pos[0]);
            $data->urutan = $pos[1];
            $data->save();
        }

        return "sukses";
    }

    

    // <------------------------------ HASIL KERJA ------------------------------------->
    public function hasilKerja($id_skpd_jab)
    {
        if(!$this->authenticateUser()) {
            return redirect('/');
        }
        $skpd_jab = SKPDJabatan::find($id_skpd_jab);
        $data['skpd_jab'] = $skpd_jab;
        $data['hasil_kerjas'] = $skpd_jab->hasilKerja()->get();
        $data['satuans'] = Satuan::get();
        // $data['tugas_pokoks'] = $skpd_jab->tugasPokok()->whereNotIn('id', $skpd_jab->hasilKerja()->get()->pluck('id_tugas_pokok'))->get();
        $data['linkaktif'] = 2;
        $data['panduan'] = Panduan::where('nomor_abk', 2)->first();
        $panduan_arr = explode(',', $skpd_jab->tampil_panduan);
        if($panduan_arr[1] == '1') {
            $data['tampil_panduan'] = 1;
        }
        else {
            $data['tampil_panduan'] = 0;
        }

        return view('anjab.hasilkerja', $data);
    }
    
    public function getHasilKerja($id)
    {
        $hk = HasilKerjas::find($id);
        // $hk->uraian_tugas_pokok = substr($hk->tugasPokok()->first()->uraian_tugas,0,50);

        return $hk;
    }

    public function tambahHasilKerja(Request $request, $id)
    {
        $validated = $request->validate([
            'hk_1' => 'required',
            'st_1' => 'required',
        ]);
        $skpd_jab = SKPDJabatan::find($id);
        $data = new HasilKerjas;
        $data->hasil_kerja = $request->hk_1;
        $data->id_satuan = $request->st_1;
        $skpd_jab->hasilKerja()->save($data);

        $status_laporan = $skpd_jab->status_laporan;
        $temp = explode(',',$status_laporan);
        if($temp[1] == '0') {
            $temp[1] = '1';
            $skpd_jab->status_laporan = implode(',',$temp);
            $skpd_jab->save();
        }

        Session::flash('success', 'Tambah data Hasil Kerja berhasil');

        return redirect('/hasil-kerja'.'/'.$id);
    }

    public function updateHasilKerja(Request $request, $id)
    {
        $validated = $request->validate([
            'hk_2' => 'required',
            'st_2' => 'required',
        ]);
        $data = HasilKerjas::find($id);
        $data->hasil_kerja = $request->hk_2;
        $data->id_satuan = $request->st_2;
        $data->save();
        Session::flash('success', 'Update data Hasil Kerja berhasil');

        return redirect('/hasil-kerja'.'/'.$data->id_skpd_jab);
    }

    public function deleteHasilKerja(Request $request)
    {
        $hk = HasilKerjas::find($request->id);
        $id_skpd_jab = $hk->id_skpd_jab;
        $hk->delete();

        $skpd_jab = SKPDJabatan::find($id_skpd_jab);
        $status_laporan = $skpd_jab->status_laporan;
        $temp = explode(',',$status_laporan);
        if(count($skpd_jab->hasilKerja()->get()) == 0) {
            $temp[1] = '0';
            $skpd_jab->status_laporan = implode(',',$temp);
            $skpd_jab->save();
        }

        Session::flash('success', 'Hapus data Hasil Kerja berhasil');

        return redirect('/hasil-kerja'.'/'.$id_skpd_jab);
    }

    // <------------------------------ BAHAN KERJA ------------------------------------->
    public function bahanKerja($id_skpd_jab)
    {
        if(!$this->authenticateUser()) {
            return redirect('/');
        }
        $skpd_jab = SKPDJabatan::find($id_skpd_jab);
        $data['skpd_jab'] = $skpd_jab;
        $data['bahankerjas'] = $skpd_jab->bahanKerja()->get();
        $data['linkaktif'] = 3;
        $data['panduan'] = Panduan::where('nomor_abk', 3)->first();
        $panduan_arr = explode(',', $skpd_jab->tampil_panduan);
        if($panduan_arr[2] == '1') {
            $data['tampil_panduan'] = 1;
        }
        else {
            $data['tampil_panduan'] = 0;
        }

        return view('anjab.bahankerja', $data);
    }
    
    public function getBahanKerja($id)
    {
        $hk = BahanKerja::find($id);

        return $hk;
    }

    public function tambahBahanKerja(Request $request, $id)
    {
        $validated = $request->validate([
            'bk_1' => 'required',
            'pg_1' => 'required',
        ]);
        $skpd_jab = SKPDJabatan::find($id);
        $data = new BahanKerja;
        $data->bahan_kerja = $request->bk_1;
        $data->penggunaan = $request->pg_1;
        $skpd_jab->bahanKerja()->save($data);

        $status_laporan = $skpd_jab->status_laporan;
        $temp = explode(',',$status_laporan);
        if($temp[2] == '0') {
            $temp[2] = '1';
            $skpd_jab->status_laporan = implode(',',$temp);
            $skpd_jab->save();
        }

        Session::flash('success', 'Tambah data Bahan Kerja berhasil');

        return redirect('/bahan-kerja'.'/'.$id);
    }

    public function updateBahanKerja(Request $request, $id)
    {
        $validated = $request->validate([
            'bk_2' => 'required',
            'pg_2' => 'required',
        ]);
        $data = BahanKerja::find($id);
        $data->bahan_kerja = $request->bk_2;
        $data->penggunaan = $request->pg_2;
        $data->save();
        Session::flash('success', 'Update data Bahan Kerja berhasil');

        return redirect('/bahan-kerja'.'/'.$data->id_skpd_jab);
    }

    public function deleteBahanKerja(Request $request)
    {
        $bk = BahanKerja::find($request->id);
        $id_skpd_jab = $bk->id_skpd_jab;
        $bk->delete();

        $skpd_jab = SKPDJabatan::find($id_skpd_jab);
        $status_laporan = $skpd_jab->status_laporan;
        $temp = explode(',',$status_laporan);
        if(count($skpd_jab->bahanKerja()->get()) == 0) {
            $temp[2] = '0';
            $skpd_jab->status_laporan = implode(',',$temp);
            $skpd_jab->save();
        }

        Session::flash('success', 'Hapus data Bahan Kerja berhasil');

        return redirect('/bahan-kerja'.'/'.$id_skpd_jab);
    }

    // <------------------------------ PERANGKAT KERJA ------------------------------------->
    public function perangkatKerja($id_skpd_jab)
    {
        if(!$this->authenticateUser()) {
            return redirect('/');
        }
        $skpd_jab = SKPDJabatan::find($id_skpd_jab);
        $data['skpd_jab'] = $skpd_jab;
        $data['perangkat_kerjas'] = $skpd_jab->perangkatKerja()->get();
        $data['linkaktif'] = 4;
        $data['panduan'] = Panduan::where('nomor_abk', 4)->first();
        $panduan_arr = explode(',', $skpd_jab->tampil_panduan);
        if($panduan_arr[3] == '1') {
            $data['tampil_panduan'] = 1;
        }
        else {
            $data['tampil_panduan'] = 0;
        }

        return view('anjab.perangkatkerja', $data);
    }
    
    public function getPerangkatKerja($id)
    {
        $hk = PerangkatKerja::find($id);

        return $hk;
    }

    public function tambahPerangkatKerja(Request $request, $id)
    {
        $validated = $request->validate([
            'pk_1' => 'required',
            'pg_1' => 'required',
        ]);
        $skpd_jab = SKPDJabatan::find($id);
        $data = new PerangkatKerja;
        $data->perangkat_kerja = $request->pk_1;
        $data->penggunaan = $request->pg_1;
        $skpd_jab->perangkatKerja()->save($data);

        $status_laporan = $skpd_jab->status_laporan;
        $temp = explode(',',$status_laporan);
        if($temp[3] == '0') {
            $temp[3] = '1';
            $skpd_jab->status_laporan = implode(',',$temp);
            $skpd_jab->save();
        }

        Session::flash('success', 'Tambah data Perangkat Kerja berhasil');

        return redirect('/perangkat-kerja'.'/'.$id);
    }

    public function updatePerangkatKerja(Request $request, $id)
    {
        $validated = $request->validate([
            'pk_2' => 'required',
            'pg_2' => 'required',
        ]);
        $data = PerangkatKerja::find($id);
        $data->perangkat_kerja = $request->pk_2;
        $data->penggunaan = $request->pg_2;
        $data->save();
        Session::flash('success', 'Update data Perangkat Kerja berhasil');

        return redirect('/perangkat-kerja'.'/'.$data->id_skpd_jab);
    }

    public function deletePerangkatKerja(Request $request)
    {
        $pk = PerangkatKerja::find($request->id);
        $id_skpd_jab = $pk->id_skpd_jab;
        $pk->delete();

        $skpd_jab = SKPDJabatan::find($id_skpd_jab);
        $status_laporan = $skpd_jab->status_laporan;
        $temp = explode(',',$status_laporan);
        if(count($skpd_jab->perangkatKerja()->get()) == 0) {
            $temp[3] = '0';
            $skpd_jab->status_laporan = implode(',',$temp);
            $skpd_jab->save();
        }

        Session::flash('success', 'Hapus data Perangkat Kerja berhasil');

        return redirect('/perangkat-kerja'.'/'.$id_skpd_jab);
    }

    // <------------------------------ TANGGUNG JAWAB ------------------------------------->
    public function tanggungJawab($id_skpd_jab)
    {
        if(!$this->authenticateUser()) {
            return redirect('/');
        }
        $skpd_jab = SKPDJabatan::find($id_skpd_jab);
        $data['skpd_jab'] = $skpd_jab;
        $data['tanggung_jawabs'] = $skpd_jab->tanggungJawab()->get();
        $data['linkaktif'] = 5;
        $data['panduan'] = Panduan::where('nomor_abk', 5)->first();
        $panduan_arr = explode(',', $skpd_jab->tampil_panduan);
        if($panduan_arr[4] == '1') {
            $data['tampil_panduan'] = 1;
        }
        else {
            $data['tampil_panduan'] = 0;
        }

        return view('anjab.tanggungjawab', $data);
    }
    
    public function getTanggungJawab($id)
    {
        $tj = TanggungJawab::find($id);

        return $tj;
    }

    public function tambahTanggungJawab(Request $request, $id)
    {
        $validated = $request->validate([
            'tj_1' => 'required',
        ]);
        $skpd_jab = SKPDJabatan::find($id);
        $data = new TanggungJawab;
        $data->tanggung_jawab = $request->tj_1;
        $skpd_jab->perangkatKerja()->save($data);

        $status_laporan = $skpd_jab->status_laporan;
        $temp = explode(',',$status_laporan);
        if($temp[4] == '0') {
            $temp[4] = '1';
            $skpd_jab->status_laporan = implode(',',$temp);
            $skpd_jab->save();
        }

        Session::flash('success', 'Tambah data Tanggung Jawab berhasil');

        return redirect('/tanggung-jawab'.'/'.$id);
    }

    public function updateTanggungJawab(Request $request, $id)
    {
        $validated = $request->validate([
            'tj_2' => 'required',
        ]);
        $data = TanggungJawab::find($id);
        $data->tanggung_jawab = $request->tj_2;
        $data->save();
        Session::flash('success', 'Update data Tanggung Jawab berhasil');

        return redirect('/tanggung-jawab'.'/'.$data->id_skpd_jab);
    }

    public function deleteTanggungJawab(Request $request)
    {
        $tj = TanggungJawab::find($request->id);
        $id_skpd_jab = $tj->id_skpd_jab;
        $tj->delete();

        $skpd_jab = SKPDJabatan::find($id_skpd_jab);
        $status_laporan = $skpd_jab->status_laporan;
        $temp = explode(',',$status_laporan);
        if(count($skpd_jab->tanggungJawab()->get()) == 0) {
            $temp[4] = '0';
            $skpd_jab->status_laporan = implode(',',$temp);
            $skpd_jab->save();
        }

        Session::flash('success', 'Hapus data Tanggung Jawab berhasil');

        return redirect('/tanggung-jawab'.'/'.$id_skpd_jab);
    }

    // <------------------------------ WEWENANG ------------------------------------->
    public function wewenang($id_skpd_jab)
    {
        if(!$this->authenticateUser()) {
            return redirect('/');
        }
        $skpd_jab = SKPDJabatan::find($id_skpd_jab);
        $data['skpd_jab'] = $skpd_jab;
        $data['wewenangs'] = $skpd_jab->wewenang()->get();
        $data['linkaktif'] = 6;
        $data['panduan'] = Panduan::where('nomor_abk', 6)->first();
        $panduan_arr = explode(',', $skpd_jab->tampil_panduan);
        if($panduan_arr[5] == '1') {
            $data['tampil_panduan'] = 1;
        }
        else {
            $data['tampil_panduan'] = 0;
        }

        return view('anjab.wewenang', $data);
    }
    
    public function getWewenang($id)
    {
        $ww = Wewenang::find($id);

        return $ww;
    }

    public function tambahWewenang(Request $request, $id)
    {
        $validated = $request->validate([
            'ww_1' => 'required',
        ]);
        $skpd_jab = SKPDJabatan::find($id);
        $data = new Wewenang;
        $data->wewenang = $request->ww_1;
        $skpd_jab->wewenang()->save($data);

        $status_laporan = $skpd_jab->status_laporan;
        $temp = explode(',',$status_laporan);
        if($temp[5] == '0') {
            $temp[5] = '1';
            $skpd_jab->status_laporan = implode(',',$temp);
            $skpd_jab->save();
        }

        Session::flash('success', 'Tambah data Wewenang berhasil');

        return redirect('/wewenang'.'/'.$id);
    }

    public function updateWewenang(Request $request, $id)
    {
        $validated = $request->validate([
            'ww_2' => 'required',
        ]);
        $data = Wewenang::find($id);
        $data->wewenang = $request->ww_2;
        $data->save();
        Session::flash('success', 'Update data Wewenang berhasil');

        return redirect('/wewenang'.'/'.$data->id_skpd_jab);
    }

    public function deleteWewenang(Request $request)
    {
        $ww = Wewenang::find($request->id);
        $id_skpd_jab = $ww->id_skpd_jab;
        $ww->delete();

        $skpd_jab = SKPDJabatan::find($id_skpd_jab);
        $status_laporan = $skpd_jab->status_laporan;
        $temp = explode(',',$status_laporan);
        if(count($skpd_jab->wewenang()->get()) == 0) {
            $temp[5] = '0';
            $skpd_jab->status_laporan = implode(',',$temp);
            $skpd_jab->save();
        }

        Session::flash('success', 'Hapus data Wewenang berhasil');

        return redirect('/wewenang'.'/'.$id_skpd_jab);
    }

    // <------------------------------ KORELASI JABATAN ------------------------------------->
    public function korelasiJabatan($id_skpd_jab)
    {
        if(!$this->authenticateUser()) {
            return redirect('/');
        }
        $skpd_jab = SKPDJabatan::find($id_skpd_jab);
        $skpd = SKPD::find($skpd_jab->id_skpd);
        // $temp_id_jabs = OPDJabatan::where('id_skpd', $skpd->id)->get()->pluck('id_jabatan');
        $data['jabatans'] = Jabatan::limit(10)->get(['id', 'jabatan']);
        $data['skpd_jab'] = $skpd_jab;
        $data['korelasi_jabs'] = $skpd_jab->korelasiJabatan()->get();
        $data['linkaktif'] = 7;
        $data['panduan'] = Panduan::where('nomor_abk', 7)->first();
        $panduan_arr = explode(',', $skpd_jab->tampil_panduan);
        if($panduan_arr[6] == '1') {
            $data['tampil_panduan'] = 1;
        }
        else {
            $data['tampil_panduan'] = 0;
        }

        return view('anjab.koreljab', $data);
    }
    
    public function getKorelasiJabatan($id)
    {
        $kj = KorelasiJabatan::find($id);
        $kj->jabatan = $kj->jabatan()->first()->jabatan;

        return $kj;
    }

    public function getJabatanKorelJab()
    {
        if(!isset($_GET['nama_jabatan'])) {
            $jabatans = Jabatan::limit(10)->get(['id', 'jabatan AS text']);
        }
        else {
            $jabatans = Jabatan::where('jabatan','like', '%'.$_GET['nama_jabatan'].'%')->limit(10)->get(['id', 'jabatan AS text']);
        }
        
        return json_encode($jabatans);
    }

    public function tambahKorelasiJabatan(Request $request, $id)
    {
        $validated = $request->validate([
            'ij_1' => 'required',
            'uk_1' => 'required',
            'dh_1' => 'required',
        ]);
        $skpd_jab = SKPDJabatan::find($id);
        $data = new KorelasiJabatan;
        $data->id_jabatan = $request->ij_1;
        $data->unit_kerja = $request->uk_1;
        $data->dalam_hal = $request->dh_1;
        $skpd_jab->korelasiJabatan()->save($data);

        $status_laporan = $skpd_jab->status_laporan;
        $temp = explode(',',$status_laporan);
        if($temp[6] == '0') {
            $temp[6] = '1';
            $skpd_jab->status_laporan = implode(',',$temp);
            $skpd_jab->save();
        }

        Session::flash('success', 'Tambah data Korelasi Jabatan berhasil');

        return redirect('/korelasi-jabatan'.'/'.$id);
    }

    public function updateKorelasiJabatan(Request $request, $id)
    {
        $validated = $request->validate([
            'ij_2' => 'required',
            'uk_2' => 'required',
            'dh_2' => 'required',
        ]);
        $data = KorelasiJabatan::find($id);
        $data->id_jabatan = $request->ij_2;
        $data->unit_kerja = $request->uk_2;
        $data->dalam_hal = $request->dh_2;
        $data->save();
        Session::flash('success', 'Update data Korelasi Jabatan berhasil');

        return redirect('/korelasi-jabatan'.'/'.$data->id_skpd_jab);
    }

    public function deleteKorelasiJabatan(Request $request)
    {
        $kj = KorelasiJabatan::find($request->id);
        $id_skpd_jab = $kj->id_skpd_jab;
        $kj->delete();

        $skpd_jab = SKPDJabatan::find($id_skpd_jab);
        $status_laporan = $skpd_jab->status_laporan;
        $temp = explode(',',$status_laporan);
        if(count($skpd_jab->korelasiJabatan()->get()) == 0) {
            $temp[6] = '0';
            $skpd_jab->status_laporan = implode(',',$temp);
            $skpd_jab->save();
        }

        Session::flash('success', 'Hapus data Korelasi Jabatan berhasil');

        return redirect('/korelasi-jabatan'.'/'.$id_skpd_jab);
    }

    // <------------------------------ LINGKUNGAN KERJA ------------------------------------->
    public function lingkunganKerja($id_skpd_jab)
    {
        if(!$this->authenticateUser()) {
            return redirect('/');
        }
        $skpd_jab = SKPDJabatan::find($id_skpd_jab);
        $all_aspek = AspekLingKerja::get();
        $data['skpd_jab'] = $skpd_jab;
        foreach($all_aspek as $aspek) {
            if(count($skpd_jab->lingkunganKerja()->where('id_aspek_lk', $aspek->id)->get()) == 0) {
                $lk = new LingkunganKerja;
                $lk->id_aspek_lk = $aspek->id;
                $lk->selected = 0;
                $skpd_jab->lingkunganKerja()->save($lk);
            }
        }
        $data['ling_kerjas'] = $skpd_jab->lingkunganKerja()->get();
        $data['linkaktif'] = 8;
        $data['panduan'] = Panduan::where('nomor_abk', 8)->first();
        $panduan_arr = explode(',', $skpd_jab->tampil_panduan);
        if($panduan_arr[7] == '1') {
            $data['tampil_panduan'] = 1;
        }
        else {
            $data['tampil_panduan'] = 0;
        }

        return view('anjab.lingkerja', $data);
    }
    
    public function getLingkunganKerja($id)
    {
        $lk = LingkunganKerja::find($id);
        $lk->nama_aspek = $lk->aspek->nama_aspek;

        return $lk;
    }

    public function tambahLingkunganKerja(Request $request, $id)
    {
        $skpd_jab = SKPDJabatan::find($id);
        foreach($skpd_jab->lingkunganKerja()->get() as $ling_kerja) {
            if($request['cb_'.$ling_kerja->id]) { $ling_kerja->selected = 1;}
            else { 
                $ling_kerja->selected = 0;
                $ling_kerja->id_faktor = null;
            }
            $ling_kerja->save();
        }

        $status_laporan = $skpd_jab->status_laporan;
        $temp = explode(',',$status_laporan);
        if($temp[7] == '0') {
            $temp[7] = '1';
            $skpd_jab->status_laporan = implode(',',$temp);
            $skpd_jab->save();
        }

        Session::flash('success', 'Modifikasi data Lingkungan Kerja berhasil');

        return redirect('/lingkungan-kerja'.'/'.$id);
    }

    public function updateLingkunganKerja(Request $request, $id)
    {
        $validated = $request->validate([
            'id_ling_kerja' => 'required',
            'id_faktor' => 'required'
        ]);
        $data = LingkunganKerja::find($request->id_ling_kerja);
        $data->id_faktor = $request->id_faktor;
        $data->save();

        return "success";
    }

    public function deleteLingkunganKerja(Request $request)
    {
        $lk = LingkunganKerja::find($request->id);
        $lk->selected = 0;
        $lk->id_faktor = null;
        $lk->save();

        $skpd_jab = SKPDJabatan::find($lk->id_skpd_jab);
        $status_laporan = $skpd_jab->status_laporan;
        $temp = explode(',',$status_laporan);
        if(count($skpd_jab->lingkunganKerja()->get()) == 0) {
            $temp[7] = '0';
            $skpd_jab->status_laporan = implode(',',$temp);
            $skpd_jab->save();
        }

        Session::flash('success', 'Hapus data Lingkungan Kerja berhasil');

        return redirect('/lingkungan-kerja'.'/'.$lk->id_skpd_jab);
    }

    // <------------------------------ PRESTASI KERJA ------------------------------------->
    public function prestasiKerja($id_skpd_jab)
    {
        if(!$this->authenticateUser()) {
            return redirect('/');
        }
        $skpd_jab = SKPDJabatan::find($id_skpd_jab);
        $data['skpd_jab'] = $skpd_jab;
        $data['pres_kerjas'] = $skpd_jab->prestasiKerja()->get();
        $data['hasil_kerjas'] = $skpd_jab->hasilKerja()->whereNotIn('hasil_kerja', $skpd_jab->prestasiKerja()->get()->pluck('prestasi'))->get();
        $data['linkaktif'] = 9;
        $data['panduan'] = Panduan::where('nomor_abk', 9)->first();
        $panduan_arr = explode(',', $skpd_jab->tampil_panduan);
        if($panduan_arr[8] == '1') {
            $data['tampil_panduan'] = 1;
        }
        else {
            $data['tampil_panduan'] = 0;
        }

        return view('anjab.preskerja', $data);
    }
    
    public function getPrestasiKerja($id)
    {
        $pk = PrestasiKerja::find($id);

        return $pk;
    }

    public function getWaktuPenyelesaian($prestasi)
    {
        if($prestasi == "-") {
            return 0;
        }
        $hk = HasilKerjas::where('hasil_kerja', $prestasi)->first();
        $waktu = $hk->tugasPokok()->first()->waktu_selesai;

        return $waktu;
    }

    public function tambahPrestasiKerja(Request $request, $id)
    {
        $validated = $request->validate([
            'pk_1' => 'required',
            'wp_1' => 'required',
            'vo_1' => 'required',
        ]);
        $skpd_jab = SKPDJabatan::find($id);
        $data = new PrestasiKerja;
        $data->prestasi = $request->pk_1;
        $data->waktu_penyelesaian = $request->wp_1;
        $data->volume = $request->vo_1;
        $skpd_jab->prestasiKerja()->save($data);

        $status_laporan = $skpd_jab->status_laporan;
        $temp = explode(',',$status_laporan);
        if($temp[8] == '0') {
            $temp[8] = '1';
            $skpd_jab->status_laporan = implode(',',$temp);
            $skpd_jab->save();
        }

        Session::flash('success', 'Tambah data Prestasi Kerja berhasil');

        return redirect('/prestasi-kerja'.'/'.$id);
    }

    public function updatePrestasiKerja(Request $request, $id)
    {
        $validated = $request->validate([
            'pk_2' => 'required',
            'wp_2' => 'required',
            'vo_2' => 'required',
        ]);
        $data = PrestasiKerja::find($id);
        $data->prestasi = $request->pk_2;
        $data->waktu_penyelesaian = $request->wp_2;
        $data->volume = $request->vo_2;
        $data->save();
        Session::flash('success', 'Update data Prestasi Kerja berhasil');

        return redirect('/prestasi-kerja'.'/'.$data->id_skpd_jab);
    }

    public function deletePrestasiKerja(Request $request)
    {
        $pk = PrestasiKerja::find($request->id);
        $id_skpd_jab = $pk->id_skpd_jab;
        $pk->delete();

        $skpd_jab = SKPDJabatan::find($id_skpd_jab);
        $status_laporan = $skpd_jab->status_laporan;
        $temp = explode(',',$status_laporan);
        if(count($skpd_jab->prestasiKerja()->get()) == 0) {
            $temp[8] = '0';
            $skpd_jab->status_laporan = implode(',',$temp);
            $skpd_jab->save();
        }

        Session::flash('success', 'Hapus data Prestasi Kerja berhasil');

        return redirect('/prestasi-kerja'.'/'.$id_skpd_jab);
    }

    // <------------------------------ SYARAT JABATAN ------------------------------------->
    public function syaratJabatan($id_skpd_jab)
    {
        if(!$this->authenticateUser()) {
            return redirect('/');
        }
        $skpd_jab = SKPDJabatan::find($id_skpd_jab);
        $data['skpd_jab'] = $skpd_jab;
        $data['syarat_jabatans'] = $skpd_jab->syaratJabatan()->get();
        $data['linkaktif'] = 10;
        $data['panduan'] = Panduan::where('nomor_abk', 10)->first();
        $data['subsyarats'] = SubSyaratJabatan::get();
        $panduan_arr = explode(',', $skpd_jab->tampil_panduan);
        if($panduan_arr[9] == '1') {
            $data['tampil_panduan'] = 1;
        }
        else {
            $data['tampil_panduan'] = 0;
        }

        return view('anjab.syaratjab', $data);
    }
    
    public function getSyaratJabatan($id)
    {
        $kt = SyaratJabatan::find($id);

        return $kt;
    }

    public function tambahSyaratJabatan(Request $request, $id)
    {
        $validated = $request->validate([
            'ij_1' => 'required',
            'kt_1' => 'required',
        ]);
        $skpd_jab = SKPDJabatan::find($id);
        $data = new SyaratJabatan;
        $data->id_sub_syarat = $request->ij_1;
        $data->syarat = $request->kt_1;
        $skpd_jab->syaratJabatan()->save($data);

        $status_laporan = $skpd_jab->status_laporan;
        $temp = explode(',',$status_laporan);
        if($temp[9] == '0') {
            $temp[9] = '1';
            $skpd_jab->status_laporan = implode(',',$temp);
            $skpd_jab->save();
        }

        Session::flash('success', 'Tambah data Syarat Jabatan berhasil');

        return redirect('/syarat-jabatan'.'/'.$id);
    }

    public function updateSyaratJabatan(Request $request, $id)
    {
        $validated = $request->validate([
            'ij_2' => 'required',
            'kt_2' => 'required',
        ]);
        $data = SyaratJabatan::find($id);
        $data->id_sub_syarat = $request->ij_2;
        $data->syarat = $request->kt_2;
        $data->save();
        Session::flash('success', 'Update data Syarat Jabatan berhasil');

        return redirect('/syarat-jabatan'.'/'.$data->id_skpd_jab);
    }

    public function deleteSyaratJabatan(Request $request)
    {
        $validated = $request->validate([
            'id' => 'required',
        ]);
        $kt = SyaratJabatan::find($request->id);
        $id_skpd_jab = $kt->id_skpd_jab;
        $kt->delete();

        $skpd_jab = SKPDJabatan::find($id_skpd_jab);
        $status_laporan = $skpd_jab->status_laporan;
        $temp = explode(',',$status_laporan);
        if(count($skpd_jab->syaratJabatan()->get()) == 0) {
            $temp[9] = '0';
            $skpd_jab->status_laporan = implode(',',$temp);
            $skpd_jab->save();
        }

        Session::flash('success', 'Hapus data Syarat Jabatan berhasil');

        return redirect('/syarat-jabatan'.'/'.$id_skpd_jab);
    }

    // <------------------------------ KLASIFIKASI JABATAN ------------------------------------->
    public function kualifikasiJabatan($id_skpd_jab)
    {
        if(!$this->authenticateUser()) {
            return redirect('/');
        }
        $skpd_jab = SKPDJabatan::find($id_skpd_jab);
        $data['skpd_jab'] = $skpd_jab;
        $data['kualifikasis'] = $skpd_jab->kualifikasiJabatan()->get();
        $data['pendidikans'] = Pendidikan::get();
        $data['linkaktif'] = 11;
        $data['panduan'] = Panduan::where('nomor_abk', 10)->first();
        $panduan_arr = explode(',', $skpd_jab->tampil_panduan);
        if($panduan_arr[10] == '1') {
            $data['tampil_panduan'] = 1;
        }
        else {
            $data['tampil_panduan'] = 0;
        }

        return view('anjab.kualjab', $data);
    }
    
    public function getKualifikasiJabatan($id)
    {
        $data = KualifikasiJabatan::find($id);

        return $data;
    }

    public function tambahKualifikasiJabatan(Request $request, $id)
    {
        $validated = $request->validate([
            'ip_1' => 'required',
            'bp_1' => 'required',
            'pl_1' => 'required',
            'th_1' => 'required',
            'kt_1' => 'required',
        ]);
        $skpd_jab = SKPDJabatan::find($id);
        $data = new KualifikasiJabatan;
        $data->id_pendidikan = $request->ip_1;
        $data->bidang_pendidikan = $request->bp_1;
        $data->pelatihan = $request->pl_1;
        $data->tahun = $request->th_1;
        $data->keterangan = $request->kt_1;
        $skpd_jab->kualifikasiJabatan()->save($data);

        $status_laporan = $skpd_jab->status_laporan;
        $temp = explode(',',$status_laporan);
        if($temp[10] == '0') {
            $temp[10] = '1';
            $skpd_jab->status_laporan = implode(',',$temp);
            $skpd_jab->save();
        }

        Session::flash('success', 'Tambah data Kualifikasi Jabatan berhasil');

        return redirect('/kualifikasi-jabatan'.'/'.$id);
    }

    public function updateKualifikasiJabatan(Request $request)
    {
        $validated = $request->validate([
            'id' => 'required',
            'ip_2' => 'required',
            'bp_2' => 'required',
            'pl_2' => 'required',
            'th_2' => 'required',
        ]);
        $data = KualifikasiJabatan::find($request->id);
        $data->id_pendidikan = $request->ip_2;
        $data->bidang_pendidikan = $request->bp_2;
        $data->pelatihan = $request->pl_2;
        $data->tahun = $request->th_2;
        $data->keterangan = $request->kt_2;
        $data->save();
        Session::flash('success', 'Update data Kualifikasi Jabatan berhasil');

        return redirect('/kualifikasi-jabatan'.'/'.$data->id_skpd_jab);
    }

    public function deleteKualifikasiJabatan(Request $request)
    {
        $validated = $request->validate([
            'id' => 'required',
        ]);
        $kt = KualifikasiJabatan::find($request->id);
        $id_skpd_jab = $kt->id_skpd_jab;
        $kt->delete();

        $skpd_jab = SKPDJabatan::find($id_skpd_jab);
        $status_laporan = $skpd_jab->status_laporan;
        $temp = explode(',',$status_laporan);
        if(count($skpd_jab->kualifikasiJabatan()->get()) == 0) {
            $temp[10] = '0';
            $skpd_jab->status_laporan = implode(',',$temp);
            $skpd_jab->save();
        }

        Session::flash('success', 'Hapus data Kualifikasi Jabatan berhasil');

        return redirect('/kualifikasi-jabatan'.'/'.$id_skpd_jab);
    }

    // <------------------------------ RESIKO BAHAYA ------------------------------------->
    public function resikoBahaya($id_skpd_jab)
    {
        if(!$this->authenticateUser()) {
            return redirect('/');
        }
        $skpd_jab = SKPDJabatan::find($id_skpd_jab);
        $data['skpd_jab'] = $skpd_jab;
        $data['resikos'] = $skpd_jab->resikoBahaya()->get();
        $data['linkaktif'] = 12;
        $data['panduan'] = Panduan::where('nomor_abk', 11)->first();
        $panduan_arr = explode(',', $skpd_jab->tampil_panduan);
        if($panduan_arr[11] == '1') {
            $data['tampil_panduan'] = 1;
        }
        else {
            $data['tampil_panduan'] = 0;
        }

        return view('anjab.resiko', $data);
    }
    
    public function getResikoBahaya($id)
    {
        $data = ResikoBahaya::find($id);

        return $data;
    }

    public function tambahResikoBahaya(Request $request, $id)
    {
        $validated = $request->validate([
            'nr_1' => 'required',
            'py_1' => 'required',
        ]);
        $skpd_jab = SKPDJabatan::find($id);
        $data = new ResikoBahaya;
        $data->nama_resiko = $request->nr_1;
        $data->penyebab = $request->py_1;
        $skpd_jab->resikoBahaya()->save($data);

        $status_laporan = $skpd_jab->status_laporan;
        $temp = explode(',',$status_laporan);
        if($temp[11] == '0') {
            $temp[11] = '1';
            $skpd_jab->status_laporan = implode(',',$temp);
            $skpd_jab->save();
        }

        Session::flash('success', 'Tambah data Resiko Bahaya berhasil');

        return redirect('/resiko-bahaya'.'/'.$id);
    }

    public function updateResikoBahaya(Request $request)
    {
        $validated = $request->validate([
            'id' => 'required',
            'nr_2' => 'required',
            'py_2' => 'required',
        ]);
        $data = ResikoBahaya::find($request->id);
        $data->nama_resiko = $request->nr_2;
        $data->penyebab = $request->py_2;
        $data->save();
        Session::flash('success', 'Update data Resiko Bahaya berhasil');

        return redirect('/resiko-bahaya'.'/'.$data->id_skpd_jab);
    }

    public function deleteResikoBahaya(Request $request)
    {
        $validated = $request->validate([
            'id' => 'required',
        ]);
        $kt = ResikoBahaya::find($request->id);
        $id_skpd_jab = $kt->id_skpd_jab;
        $kt->delete();

        $skpd_jab = SKPDJabatan::find($id_skpd_jab);
        $status_laporan = $skpd_jab->status_laporan;
        $temp = explode(',',$status_laporan);
        if(count($skpd_jab->resikoBahaya()->get()) == 0) {
            $temp[11] = '0';
            $skpd_jab->status_laporan = implode(',',$temp);
            $skpd_jab->save();
        }

        Session::flash('success', 'Hapus data Resiko Bahaya berhasil');

        return redirect('/resiko-bahaya'.'/'.$id_skpd_jab);
    }
}
