<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SKPD;
use App\Jabatan;
use App\SKPDJabatan;
use App\JenisJabatan;
use App\Eselon;
use App\Pegawai;
use App\FaktorAspek;
use App\PetaJabatan;
use App\OPDJabatan;
use DB;
use PDF;
use Auth;
use Session;


class ABKController extends Controller
{
    private function authenticateUser()
    {
        if(!Auth::user()) {
            return false;
        }
        
        return true;
    }

    public function index()
    {
        if(!$this->authenticateUser()) {
            return redirect('/');
        }
        if(isset($_GET['table_search'])) {
            $data['var_search'] = $_GET['table_search'];
            if(isset($_GET['sortjumlah'])) {
                if($_GET['sortjumlah'] == 'ASC') {
                    $data['skpds'] = SKPD::where('nama_skpd', 'like', '%'.$_GET['table_search'].'%')->withCount('jabatan')->orderBy('jabatan_count')->paginate(20)->appends(request()->query());
                }
                else if($_GET['sortjumlah'] == 'DESC') {
                    $data['skpds'] = SKPD::where('nama_skpd', 'like', '%'.$_GET['table_search'].'%')->withCount('jabatan')->orderBy('jabatan_count','DESC')->paginate(20)->appends(request()->query());
                }
            }
            else {
                $data['skpds'] = SKPD::where('nama_skpd', 'like', '%'.$_GET['table_search'].'%')->withCount('jabatan')->orderBy('jabatan_count','ASC')->paginate(20)->appends(request()->query());
            }
        }
        else {
            if(isset($_GET['sortjumlah'])) {
                if($_GET['sortjumlah'] == 'ASC') {
                    $data['skpds'] = SKPD::withCount('jabatan')->orderBy('jabatan_count')->paginate(20);
                }
                else if($_GET['sortjumlah'] == 'DESC') {
                    $data['skpds'] = SKPD::withCount('jabatan')->orderBy('jabatan_count','DESC')->paginate(20);
                }
            }
            else {
                $data['skpds'] = SKPD::withCount('jabatan')->paginate(20);
            }
        }
        
        $data['sideaktif'] = 2;
        return view('abk.index', $data);
    }

    public function detailSKPD($id)
    {
        if(!$this->authenticateUser()) {
            return redirect('/');
        }
        $data['sideaktif'] = 2;
        $skpd = SKPD::find($id);
        $data['skpd'] = $skpd;
        if(isset($_GET['table_search'])) {
            $data['jabatans'] = $skpd->jabatan()->where('jabatan', 'like', '%'.$_GET['table_search'].'%')->get();
        }
        else {
            $data['jabatans'] = $skpd->jabatan()->get();
        }
        $data['route'] = url('/');
        
        foreach($data['jabatans'] as $jab) {
            if(count(SKPDJabatan::find($jab->pivot->id)->child()->get()) > 0) {
                $jab->has_child = 1;
            }
            else {
                $jab->has_child = 0;
            }
            if($jab->pivot->id_jab_atasan != 0) {
                $jab->jab_atasan = SKPDJabatan::find($jab->pivot->id_jab_atasan)->jabatan()->first();
            }
        }

        return view('abk.detailskpd', $data);
    }

    private function buildTree(array $elements, $parentId = 0) {
        $branch = array();
    
        foreach ($elements as $element) {
            if ($element['parent_id'] == $parentId) {
                $children = $this->buildTree($elements, $element['id']);
                if ($children) {
                    $element['children'] = $children;
                }
                $branch[] = $element;
            }
        }
    
        return $branch;
    }

    public function getNodeDetail($id)
    {
        $data = PetaJabatan::find($id);
        $data->nama_jabatan = $data->skpdJab()->first()->jabatan()->first()->jabatan;

        return $data;
    }

    public function lihatPetaJabatan($id)
    {
        if(!Auth::user()) {
            return redirect('login');
        }
        $user = Auth::user();
        $user->boleh_edit_petajabatan = $user->role()->first()->edit_petajabatan;
        $data['user'] = $user;
        $data['sideaktif'] = 2;
        $data['pegawais'] = Pegawai::where('id_skpd', $id)->get();
        $nodes = PetaJabatan::where('id_skpd', $id)->get();
        $rows = array();
        foreach($nodes as $node) {
            $pegawai = Pegawai::find($node->id_pegawai);
            if($pegawai) {
                $nama = $pegawai->nama;
                $nip = $pegawai->nip;
                if($pegawai->foto) {
                    $linkfoto = asset($pegawai->foto);
                }
                else {
                    $linkfoto = asset('images/empty-img-white.svg');
                }
                $golongan = $pegawai->golongan()->first()->golongan;
            }
            else {
                $nama = '-';
                $nip = '-';
                $linkfoto = asset('images/empty-img-white.svg');
                $golongan = '';
            }
            $jabatan = $node->skpdJab()->first()->jabatan()->first();
            if($node->id_parent_skpd_jab != 0) {
                $parent_id = PetaJabatan::where('id_skpd_jab', $node->id_parent_skpd_jab)->first()->id;
                if($jabatan->is_asisten == 1) {
                    array_push($rows, array(
                        'id' => $node->id, 
                        'name' => $nama,
                        'nip' => $nip,
                        'title' => $jabatan->jabatan, 
                        'pid' => $parent_id, 
                        'img' => $linkfoto, 
                        'id_skpd_jab' => $node->id_skpd_jab,
                        'eselon' => $node->skpdJab()->first()->eselon()->first()->eselon,
                        'golongan' => $golongan,
                        'strip1' => '-',
                        'tags' => ["assistant"]
                    ));
                }
                else {
                    array_push($rows, array(
                        'id' => $node->id, 
                        'name' => $nama,
                        'nip' => $nip,
                        'title' => $jabatan->jabatan, 
                        'pid' => $parent_id, 
                        'img' => $linkfoto, 
                        'id_skpd_jab' => $node->id_skpd_jab,
                        'golongan' => $golongan,
                        'strip1' => '-',
                        'eselon' => $node->skpdJab()->first()->eselon()->first()->eselon
                    ));
                }
            }
            else {
                $parent_id = 0;
                array_push($rows, array(
                    'id' => $node->id, 
                    'name' => $nama,
                    'nip' => $nip,
                    'title' => $jabatan->jabatan, 
                    'img' => $linkfoto, 
                    'id_skpd_jab' => $node->id_skpd_jab,
                    'golongan' => $golongan,
                    'strip1' => '-',
                    'eselon' => $node->skpdJab()->first()->eselon()->first()->eselon
                ));
            }
        }
        // $tree = $this->buildTree($rows);
        $data['struktur'] = $rows;
        $data['skpd'] = SKPD::find($id);
        $temp_child0 = array();
        $temp_child1 = array();
        $data['layout'] = false;
        if(count($rows) > 0) {
            $temp_head = PetaJabatan::whereIn('id_skpd_jab', SKPDJabatan::where('id_skpd', $id)->get()->pluck('id'))->where('id_parent_skpd_jab', 0)->first()->id_skpd_jab;
            if($temp_head) {
                $temp_child0 = PetaJabatan::where('id_parent_skpd_jab', $temp_head)->get()->pluck('id_skpd_jab');
                if(count($temp_child0) > 0) {
                    $temp_child1 = PetaJabatan::whereIn('id_parent_skpd_jab', $temp_child0)->get()->pluck('id_skpd_jab');
                }
            }
        }
        if(count($temp_child1) > 0) {
            $data['layout'] = true;
        }

        return view('abk.petajabatan', $data);
    }

    public function updateNodePetaJabatan($node_id, $id_pegawai)
    {
        if($node_id && $id_pegawai) {
            $data = PetaJabatan::find($node_id);
            if($data->id_pegawai != 0) {
                $pegawai_lama = $data->pegawai()->first();  #cari data pegawai lama
                $pegawai_lama->id_skpd_jab = null;          #ubah id_skpd_jab ke null
                $pegawai_lama->save();
            }
            $data->id_pegawai = $id_pegawai;            #simpan id_pegawai yg baru di peta jabatan
            $data->save();
            $pegawai = $data->pegawai()->first();
            $pegawai->id_skpd_jab = $data->id_skpd_jab;
            $pegawai->save();
            $data->nama_peg = $pegawai->nama;
            $data->nip = $pegawai->nip;
            return $data;
        }
        return '';
    }

    public function getPegawaiPetaJabatan($id_node)
    {
        $pj = PetaJabatan::find($id_node);
        $skpd_jab = SKPDJabatan::find($pj->id_skpd_jab);
        $pegawais = Pegawai::where('id_skpd', $skpd_jab->id_skpd)->doesntHave('petaJabatan')->get();

        return $pegawais;
    }

    public function getSKPD($id)
    {
        return SKPD::find($id);
    }

    public function tambahSKPD()
    {
        if(!$this->authenticateUser()) {
            return redirect('/');
        }
        $data['sideaktif'] = 3;
        $redirect = redirect()->back()->getTargetUrl();
        session(['redirect_url' => $redirect]);
        return view('abk.tambahskpd', $data);
    }

    public function createSKPD(Request $request)
    {
        $data = new SKPD;
        $data->kode_skpd = $request->kode_skpd;
        $data->nama_skpd = $request->nama_skpd;
        $data->save();
        Session::flash('success', 'Tambah data SKPD berhasil');

        return redirect(session('redirect_url'));
    }

    public function editSKPD($id)
    {
        $data['skpd'] = SKPD::find($id);
        $data['sideaktif'] = 2;
        $redirect = redirect()->back()->getTargetUrl();
        session(['redirect_url' => $redirect]);
        return view('abk.editskpd', $data);
    }

    public function updateSKPD(Request $request, $id)
    {
        $skpd = SKPD::find($id);
        $skpd->kode_skpd = $request->kode_skpd;
        $skpd->nama_skpd = $request->nama_skpd;
        $skpd->save();
        Session::flash('success', 'SKPD berhasil diupdate');
        
        return redirect(session('redirect_url'));
    }

    public function hapusSKPD(Request $request, $id)
    {
        $skpd = SKPD::find($id);
        $skpdjabs = SKPDJabatan::where('id_skpd', $id)->get();
        if(count($skpdjabs) > 0) {
            Session::flash('fail', 'SKPD gagal dihapus. harap menghapus SKPD Jabatan di SKPD terkait.');
            return redirect('/opdjab/opd');
        }
        Pegawai::where('id_skpd', $id)->update(['id_skpd' => null, 'id_skpd_jab' => null]);
        OPDJabatan::where('id_skpd',$id)->delete();
        $skpd->delete();
        Session::flash('success', 'SKPD berhasil dihapus');
        
        return redirect('/opdjab/opd');
    }

    public function detailJab($id){
        if(!$this->authenticateUser()) {
            return redirect('/');
        }
        $skpds = DB::table('skpd_jabatan')->join('jabatan', function($join) use ($id)
        {
            $join->on('skpd_jabatan.id_jabatan', '=', 'jabatan.id');
                
        })->join('jenis_jabatan', function($join) use ($id)
        {
            $join->on('jabatan.id_jenis_jabatan', '=', 'jenis_jabatan.id');
                
        })->where('skpd_jabatan.id', '=', $id)->get();

        $data['skpd'] = $skpds;
        $data['id_skpd_jab'] = $id;
        $skpd_id = $skpds[0]->id_skpd;
        // $data['jabatan_all'] = Jabatan::get();
        $data['eselons'] = Eselon::get();
        $data['id_skpd_jab'] = $id; 
        $data['sideaktif'] = 2;
        $data['skpd_jab'] = SKPDJabatan::find($id);
        $data['jabatan_atasans'] = SKPD::find($skpd_id)->jabatan()->wherePivot('id_jabatan', '!=', $data['skpd_jab']->id_jabatan)->get();
        $skpd_tab = SKPD::find($skpd_id);
        $data['nama_skpd'] = $skpd_tab->nama_skpd;
        // $jab_tersedia = SKPD::find($skpd_id)->jabatan()->wherePivot('id', '!=', $id)->get()->pluck('id');
        // $data['jabatan_all'] = DB::table('jabatan')->whereNotIn('id', $jab_tersedia)->get();
        $jabatan_tersedia = SKPDJabatan::where('id_skpd', $skpd_id)->where('id_jabatan', '!=', $data['skpd_jab']->id_jabatan)->get()->pluck('id_jabatan');
        $data['jabatan_all'] = Jabatan::whereIn('id', OPDJabatan::where('id_skpd', $skpd_id)->get()->pluck('id_jabatan'))->whereNotIn('id', $jabatan_tersedia)->get();

        return view('abk.detailjab', $data);
    }

    public function formTambahJab($id)
    {
        $data['sideaktif'] = 2;
        $skpd = SKPD::find($id);
        $data['skpd'] = $skpd;
        $data['jabatans'] = $skpd->jabatan()->get();
        $jabatan_tersedia = SKPDJabatan::where('id_skpd', $id)->get()->pluck('id_jabatan');
        if(count($jabatan_tersedia) == 0) {
            $data['jabatan_all'] = Jabatan::whereIn('id', OPDJabatan::where('id_skpd', $id)->get()->pluck('id_jabatan'))->get();
        }
        else {
            $data['jabatan_all'] = Jabatan::whereIn('id', OPDJabatan::where('id_skpd', $id)->get()->pluck('id_jabatan'))->whereNotIn('id', $jabatan_tersedia)->get();
        }
        $data['jenis_jab'] = Jabatan::first()->jenisJab()->first();
        $data['eselons'] = Eselon::get();

        return view('abk.tambahjab', $data);
    }

    public function tambahJab(Request $request, $id)
    {
        $validated = $request->validate([
            'id_jab_atasan' => 'required',
            'id_jabatan' => 'required',
        ]);

        $data = new SKPDJabatan;
        $data->id_skpd = $id;
        if($request->id_jab_atasan != '-') {
            $data->id_jab_atasan = $request->id_jab_atasan;
        }
        $data->id_jabatan = $request->id_jabatan;
        $data->id_eselon = $request->id_eselon;
        $data->kelas_jab = $request->kelas_jab;
        $data->jml_pemangku = $request->jml_pemangku;
        $data->ikhtisar = $request->ikhtisar;
        $data->save();

        $peta_jabatan = new PetaJabatan;
        $peta_jabatan->id_skpd_jab = $data->id;
        if($data->id_jab_atasan) {
            $peta_jabatan->id_parent_skpd_jab = $data->id_jab_atasan;
        }
        else {
            $peta_jabatan->id_parent_skpd_jab = 0;
        }
        $peta_jabatan->id_skpd = $data->id_skpd;
        $peta_jabatan->id_pegawai = 0;
        $peta_jabatan->save();
        
        Session::flash('success', 'Tambah data SKPD Jabatan berhasil');

        if($request->lanjut == '1') {
            return redirect('/uraian-tugas'.'/'.$data->id);
        }

        return redirect('/abk/detailskpd/'.$id);
    }

    public function updateJab(Request $request)
    {
        $validated = $request->validate([
            'id_skpd_jab' => 'required',
        ]);
        $jabatan = SKPDJabatan::find($request->id_skpd_jab);
        if($request->id_jab_atasan == '-') {
            $jabatan->id_jab_atasan = 0;
        }
        else {
            $jabatan->id_jab_atasan = $request->id_jab_atasan;
        }
        $jabatan->id_jabatan = $request->id_jabatan;
        $jabatan->kelas_jab = $request->kelas_jab;
        $jabatan->jml_pemangku = $request->jml_pemangku;
        $jabatan->ikhtisar = $request->ikhtisar;
        $jabatan->id_eselon = $request->id_eselon;
        $jabatan->save();

        $peta_jabatan = PetaJabatan::where('id_skpd_jab', $jabatan->id)->update(['id_parent_skpd_jab' => $jabatan->id_jab_atasan]);

        Session::flash('success', 'Update data SKPD Jabatan berhasil');

        return redirect('/abk/detailskpd/'.$jabatan->id_skpd);
    }

    public function deleteJab(Request $request, $id_skpd)
    {
        SKPDJabatan::where('id_skpd', $id_skpd)->where('id_jabatan', $request->id_jabatan)->delete();
        
        return redirect('/abk/detailskpd/'.$id_skpd)->with('message', 'Sukses menghapus jabatan dari SKPD.');
    }

    public function getJenisJab($id)
    {
        $jenis_jab = Jabatan::find($id)->jenisJab()->first();
        return $jenis_jab;
    }

    public function getEselonJab($id)
    {
        $data = Jabatan::find($id);
        return $data;
    }


    public function deleteJabSkpd($id){
        $skpd_jab = SKPDJabatan::find($id);
        $skpd_id = $skpd_jab->id_skpd;
        $skpd_jab->delete();

        Session::flash('success', 'Data SKPD Jabatan berhasil dihapus');

        return redirect('/abk/detailskpd/'.$skpd_id);
    }

    public function cetakPDF($id)
    {
        $skpd_jab = SKPDJabatan::find($id);
        $data['skpd_jab'] = $skpd_jab;
        $data['skpd'] = SKPD::find($skpd_jab->id_skpd);
        $data['jabatan'] = $skpd_jab->jabatan()->first();
        $data['atasans'] = [];
        array_push($data['atasans'], SKPDJabatan::find($skpd_jab->id_jab_atasan));
        if(!empty($data['atasans'][count($data['atasans'])-1]->id_jab_atasan)) {
            array_push($data['atasans'], SKPDJabatan::find($data['atasans'][count($data['atasans'])-1]->id_jab_atasan));
            if(!empty($data['atasans'][count($data['atasans'])-1]->id_jab_atasan)) {
                array_push($data['atasans'], SKPDJabatan::find($data['atasans'][count($data['atasans'])-1]->id_jab_atasan));
            }
        }
        $data['tugas_pokoks'] = $skpd_jab->tugasPokok()->orderBy('urutan')->get();
        $data['jumlah_tugas_pokok'] = $skpd_jab->tugasPokok()->sum('pegawai_dibutuhkan');
        if($data['jumlah_tugas_pokok'] - intval($data['jumlah_tugas_pokok']) > 0.5) {
            $data['jumlah_tugas_pokok_dibulatkan'] = intval($data['jumlah_tugas_pokok']) + 1;
        }
        else {
            $data['jumlah_tugas_pokok_dibulatkan'] = intval($data['jumlah_tugas_pokok']);
        }
        $data['hasil_kerjas'] = $skpd_jab->hasilKerja()->get();
        $data['bahan_kerjas'] = $skpd_jab->bahanKerja()->get();
        $data['perangkat_kerjas'] = $skpd_jab->perangkatKerja()->get();
        $data['tanggung_jawabs'] = $skpd_jab->tanggungJawab()->get();
        $data['wewenangs'] = $skpd_jab->wewenang()->get();
        $data['korelasi_jabs'] = $skpd_jab->korelasiJabatan()->get();
        foreach($data['korelasi_jabs'] as $d) {
            $d->nama_jabatan = Jabatan::find($d->id_jabatan)->jabatan;
        }
        $data['lingkungan_kerjas'] = $skpd_jab->lingkunganKerja()->where('selected', 1)->whereNotNull('id_faktor')->get();
        foreach($data['lingkungan_kerjas'] as $d) {
            $d->nama_aspek = $d->aspek()->first()->nama_aspek;
            $fa = FaktorAspek::find($d->id_faktor);
            $d->nilai_aspek = is_null($fa) ? '' : $fa->nama_faktor;
        }
        $data['prestasi_kerjas'] = $skpd_jab->prestasiKerja()->get();
        $data['syarat_jabatans'] = $skpd_jab->syaratJabatan()->orderBy('id_sub_syarat')->get();
        $data['kualifikasi_jabatans'] = $skpd_jab->kualifikasiJabatan()->get();
        $data['resiko_bahayas'] = $skpd_jab->resikoBahaya()->get();
        // dd(str_replace("\n", " ", $data['jabatan']->jabatan));

        // return view('abk.laporan1', $data);
        $pdf = PDF::loadView('abk.laporan1', $data);
        // return $pdf->stream("Analisis Jabatan.pdf", array("Attachment" => false));
        set_time_limit(300);
        return $pdf->stream("Analisis Jabatan ".str_replace("\n", " ", $data['jabatan']->jabatan)." - ".str_replace("\n", " ", $data['skpd']->nama_skpd).".pdf", array("Attachment" => false));
    }

    public function getDetailSKPDJabatan($id)
    {
        $data = SKPDJabatan::find($id);
        $data->nama_jabatan = $data->jabatan()->first()->jabatan;
        $data->nama_skpd = $data->skpd()->first()->nama_skpd;

        return $data;
    }
}
