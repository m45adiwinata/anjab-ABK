<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\JenisJabatan;
use App\Jabatan;
use App\SKPD;
use App\Eselon;
use App\RoleUser;
use App\Pegawai;
use App\HasilKerja;
use App\Satuan;
use App\AspekLingKerja;
use App\FaktorAspek;
use App\Panduan;
use App\SKPDJabatan;
use App\SubSyaratJabatan;
use App\SyaratJabatan;
use App\PetaJabatan;
use App\OPDJabatan;
use App\Pendidikan;
use Auth;

class MasterController extends Controller
{

    private function authenticateUser()
    {
        if(!Auth::user()) {
            return false;
        }
        else if(Auth::user()->role()->first()->master == 1) {
            return true;
        }
        
        return false;
    }

    public function index()
    {
        if(!$this->authenticateUser()) {
            return redirect('/');
        }
        $data['sideaktif'] = 3;
        $data['users'] = User::get();
        $data['jenisjabatans'] = JenisJabatan::get();
        $data['jabatans'] = Jabatan::get();
        $data['eselons'] = Eselon::get();
        $data['roles'] = RoleUser::get();
        $data['panduans'] = Panduan::get();
        $data['skpd_all'] = SKPD::get();
        if(isset($_GET['nama_pegawai'])) {
            $data['pegawais'] = Pegawai::where('nama', 'like', '%'.$_GET['nama_pegawai'].'%')->paginate(20)->appends(request()->query());
        }
        else {
            $data['pegawais'] = Pegawai::paginate(20);
        }
        foreach($data['pegawais'] as $pegawai) {
            if($pegawai->id_skpd_jab) {
                $sj = $pegawai->skpdJab()->first();
                if($sj){
                    $pegawai->jabatan = $sj->jabatan()->first()->jabatan;
                }else{
                    $pegawai->jabatan = '';
                }
            }
            else {
                $pegawai->jabatan = '';
            }
        }
        

        return view('master.index', $data);
    }

    // ===============================================     USER AKUN       ====================================================

    public function createAkun(Request $request)
    {
        if(!$this->authenticateUser()) {
            return redirect('/');
        }
        $validated = $request->validate([
            'nama' => 'required',
            'username' => 'required',
            'password' => 'required',
            'role_id' => 'required',
        ]);
        $data = new User;
        $data->name = $request->nama;
        $data->username = $request->username;
        $data->password = md5($request->password);
        $data->role_id = $request->role_id;
        // if(isset($request->id_pegawai)) {
        //     $data->id_pegawai = $request->id_pegawai;
        // }
        if(isset($request->id_skpd)) {
            $data->id_skpd = $request->id_skpd;
        }
        $data->email = $request->username.'@email.com';
        $data->save();

        return redirect('/master');
    }

    public function deleteAkun(Request $request, $id)
    {
        if(!$this->authenticateUser()) {
            return redirect('/');
        }
        User::find($id)->delete();

        return redirect('/master');
    }

    public function getDetailAkun($id)
    {
        if(!$this->authenticateUser()) {
            return redirect('/');
        }
        $data = User::find($id);

        return $data;
    }

    public function getPegawaiBySKPD($id_skpd)
    {
        $pegawais = Pegawai::where('id_skpd', $id_skpd)->get()->pluck('nama','id');
        return $pegawais;
    }

    public function updateAkun(Request $request, $id)
    {
        if(!$this->authenticateUser()) {
            return redirect('/');
        }
        $validated = $request->validate([
            'nama' => 'required',
            'username' => 'required',
            'role_id' => 'required',
        ]);
        $data = User::find($id);
        $data->name = $request->nama;
        $data->username = $request->username;
        if(isset($request->password)) {
            $data->password = md5($request->password);
        }
        $data->role_id = $request->role_id;
        // if(isset($request->id_pegawai)) {
        //     $data->id_pegawai = $request->id_pegawai;
        // }
        if(isset($request->id_skpd)) {
            $data->id_skpd = $request->id_skpd;
        }
        $data->email = $request->username.'@email.com';
        $data->save();

        return redirect('/master');
    }

    public function toggleActiveAkun(Request $request)
    {
        $validated = $request->validate([
            'id' => 'required',
        ]);
        $data = User::find($request->id);
        if($data->status == 0) {
            $data->status = 1;
        }
        else {
            $data->status = 0;
        }
        $data->save();

        return 1;
    }

    // ===============================================     PEGAWAI       ====================================================

    public function createPegawai(Request $request)
    {
        if(!$this->authenticateUser()) {
            return redirect('/');
        }
        $validated = $request->validate([
            'nama' => 'required',
            'nip' => 'required',
            'alamat' => 'required',
            'id_skpd' => 'required',
            'id_eselon' => 'required',
        ]);
        // dd($request);
        $file = $request->file('foto');
        $nama_foto = time()."-".$file->getClientOriginalName();
        $file->move('images/photo_pegawai',$nama_foto);
        $data = new Pegawai;
        $data->nama = $request->nama;
        $data->nip = $request->nip;
        $data->alamat = $request->alamat;
        $data->id_skpd = $request->id_skpd;
        $data->id_eselon = $request->id_eselon;
        $data->foto = 'images/photo_pegawai/'.$nama_foto;
        $data->save();

        return redirect()->back();
    }

    public function deletePegawai(Request $request)
    {
        if(!$this->authenticateUser()) {
            return redirect('/');
        }
        $validated = $request->validate([
            'id' => 'required',
        ]);
        $pegawai = Pegawai::find($request->id);
        if(file_exists($pegawai->foto)){
            unlink($pegawai->foto);
        }
        $pegawai->delete();

        return redirect()->back();
    }

    public function getDetailPegawai($id)
    {
        $data = Pegawai::find($id);

        return $data;
    }

    public function updatePegawai(Request $request)
    {
        if(!$this->authenticateUser()) {
            return redirect('/');
        }
        $validated = $request->validate([
            'id' => 'required',
            'nama' => 'required',
            'nip' => 'required',
            'alamat' => 'required',
            'id_eselon' => 'required',
            'id_skpd' => 'required',
        ]);
        $data = Pegawai::find($request->id);
        $data->nama = $request->nama;
        $data->nip = $request->nip;
        $data->alamat = $request->alamat;
        $data->id_eselon = $request->id_eselon;
        $data->id_skpd = $request->id_skpd;
        if(isset($request->foto)) {
            if(isset($data->foto)) {
                if(file_exists($data->foto)){
                    unlink($data->foto);
                }
            }
            $file = $request->file('foto');
            $nama_foto = time()."-".$file->getClientOriginalName();
            $file->move('images/photo_pegawai',$nama_foto);
            $data->foto = 'images/photo_pegawai/'.$nama_foto;
        }
        $data->save();
        $redirect = redirect()->back()->getTargetUrl() . "#pegawai";
        return redirect($redirect);
    }

    // ===============================================     HASIL URAIAN KERJA       ====================================================

    public function indexHasilUraianKerja()
    {
        if(!$this->authenticateUser()) {
            return redirect('/');
        }
        $data['sideaktif'] = 3;
        $data['hasil_kerjas'] = HasilKerja::get();

        return view('master.hasilkerja', $data);
    }

    public function tambahHasilUraianKerja(Request $request)
    {
        if(!$this->authenticateUser()) {
            return redirect('/');
        }
        $data = new HasilKerja;
        $data->nama_hasil_kerja = $request->nama_hasil_kerja;
        $data->save();

        return redirect()->back()->with('success', 'Berhasil menambahkan hasil uraian kerja.');
    }
    public function hapusHasilUraianKerja(Request $request)
    {
        if(!$this->authenticateUser()) {
            return redirect('/');
        }
        $data = HasilKerja::find($request->id);
        $msg = $data->nama_hasil_kerja;
        $data->delete();

        return redirect()->back()->with('success', 'Berhasil menghapus hasil uraian kerja.');
    }
    public function updateHasilUraianKerja(Request $request)
    {
        if(!$this->authenticateUser()) {
            return redirect('/');
        }
        $data = HasilKerja::find($request->id);
        $data->nama_hasil_kerja = $request->nama_hasil_kerja;
        $data->save();

        return redirect()->back()->with('success', 'Berhasil mengedit hasil uraian kerja.');
    }
    public function getDetailHasilUraianKerja($id)
    {
        $data = HasilKerja::find($id);
        return $data;
    }

    // ===============================================     SATUAN      ====================================================

    public function indexSatuan()
    {
        if(!$this->authenticateUser()) {
            return redirect('/');
        }
        $data['sideaktif'] = 3;
        $data['satuans'] = Satuan::get();

        return view('master.satuan', $data);
    }

    public function tambahSatuan(Request $request)
    {
        if(!$this->authenticateUser()) {
            return redirect('/');
        }
        $data = new Satuan;
        $data->nama_satuan = $request->nama_satuan;
        $data->save();

        return redirect()->back()->with('success', 'Berhasil menambahkan satuan.');
    }
    
    public function hapusSatuan(Request $request)
    {
        if(!$this->authenticateUser()) {
            return redirect('/');
        }
        $data = Satuan::find($request->id);
        $msg = $data->nama_satuan;
        $data->delete();

        return redirect()->back()->with('success', 'Berhasil menghapus satuan.');
    }

    public function updateSatuan(Request $request)
    {
        if(!$this->authenticateUser()) {
            return redirect('/');
        }
        $data = Satuan::find($request->id);
        $data->nama_satuan = $request->nama_satuan;
        $data->save();

        return redirect()->back()->with('success', 'Berhasil mengedit satuan.');
    }

    public function getDetailSatuan($id)
    {
        $data = Satuan::find($id);
        return $data;
    }

    // ===============================================     ASPEK LINGKUNGAN KERJA      ====================================================

    public function indexLingkunganKerja()
    {
        if(!$this->authenticateUser()) {
            return redirect('/');
        }
        $data['sideaktif'] = 3;
        $data['aspeks'] = AspekLingKerja::get();

        return view('master.aspek_LK', $data);
    }

    public function tambahLingkunganKerja(Request $request)
    {
        if(!$this->authenticateUser()) {
            return redirect('/');
        }
        $data = new AspekLingKerja;
        $data->nama_aspek = $request->nama_aspek;
        $data->save();

        return redirect()->back()->with('success', 'Berhasil menambahkan aspek lingkungan kerja.');
    }
    
    public function hapusLingkunganKerja(Request $request)
    {
        if(!$this->authenticateUser()) {
            return redirect('/');
        }
        $data = AspekLingKerja::find($request->id);
        $msg = $data->nama_aspek;
        $data->faktor()->delete();
        $data->delete();

        return redirect()->back()->with('success', 'Berhasil menghapus aspek lingkungan kerja.');
    }

    public function updateLingkunganKerja(Request $request)
    {
        if(!$this->authenticateUser()) {
            return redirect('/');
        }
        $data = AspekLingKerja::find($request->id);
        $data->nama_aspek = $request->nama_aspek;
        $data->save();

        return redirect()->back()->with('success', 'Berhasil mengedit aspek lingkungan kerja.');
    }

    public function getDetailLingkunganKerja($id)
    {
        $data = AspekLingKerja::find($id);
        $data->faktors = $data->faktor()->pluck('nama_faktor','id');
        return $data;
    }

    public function tambahFaktorAspek(Request $request)
    {
        if(!$this->authenticateUser()) {
            return redirect('/');
        }
        $validated = $request->validate([
            'id_aspek_lk' => 'required',
            'nama_faktor' => 'required',
        ]);
        $data = new FaktorAspek;
        $data->id_aspek_lk = $request->id_aspek_lk;
        $data->nama_faktor = $request->nama_faktor;
        $data->save();

        return redirect()->back()->with('success', 'Berhasil menambahkan faktor aspek.');
    }
    
    public function hapusFaktorAspek(Request $request)
    {
        if(!$this->authenticateUser()) {
            return redirect('/');
        }
        $data = FaktorAspek::find($request->id);
        $msg = $data->nama_faktor;
        $data->delete();

        return redirect()->back()->with('success', 'Berhasil menghapus faktor aspek.');
    }

    public function updateFaktorAspek(Request $request)
    {
        if(!$this->authenticateUser()) {
            return redirect('/');
        }
        $data = FaktorAspek::find($request->id);
        $data->nama_faktor = $request->nama_faktor;
        $data->save();

        return redirect()->back()->with('success', 'Berhasil mengedit faktor aspek.');
    }

    public function getDetailFaktorAspek($id)
    {
        $data = FaktorAspek::find($id);
        return $data;
    }

    // ===============================================     SYARAT JABATAN      ====================================================

    public function indexSyarat()
    {
        if(!$this->authenticateUser()) {
            return redirect('/');
        }
        $data['sideaktif'] = 3;
        $data['poin_syarats'] = SubSyaratJabatan::get();
        $data['skpds'] = SKPD::get();

        return view('master.syarat', $data);
    }

    public function tambahSyarat(Request $request)
    {
        if(!$this->authenticateUser()) {
            return redirect('/');
        }
        $validated = $request->validate([
            'nama_sub_syarat' => 'required',
        ]);
        $data = new SubSyaratJabatan;
        $data->nama_sub_syarat = $request->nama_sub_syarat;
        $data->save();

        return redirect()->back()->with('success', 'Berhasil menambahkan poin syarat jabatan.');
    }
    
    public function hapusSyarat(Request $request)
    {
        if(!$this->authenticateUser()) {
            return redirect('/');
        }
        $data = SubSyaratJabatan::find($request->id);
        $msg = $data->nama_sub_syarat;
        $data->syaratjabatan()->delete();
        $data->delete();

        return redirect()->back()->with('success', 'Berhasil menghapus poin syarat jabatan.');
    }

    public function updateSyarat(Request $request)
    {
        if(!$this->authenticateUser()) {
            return redirect('/');
        }
        $validated = $request->validate([
            'nama_sub_syarat' => 'required',
        ]);
        $data = SubSyaratJabatan::find($request->id);
        $data->nama_sub_syarat = $request->nama_sub_syarat;
        $data->save();

        return redirect()->back()->with('success', 'Berhasil mengedit poin syarat jabatan.');
    }

    public function getDetailSyarat($id)
    {
        $data = SubSyaratJabatan::find($id);
        
        return $data;
    }

    // public function tambahSyaratJabatan(Request $request)
    // {
    //     if(!$this->authenticateUser()) {
    //         return redirect('/');
    //     }
    //     $validated = $request->validate([
    //         'id_skpd_jab' => 'required',
    //         'id_sub_syarat' => 'required',
    //         'syarat' => 'required',
    //     ]);
    //     $data = new SyaratJabatan;
    //     $data->id_skpd_jab = $request->id_skpd_jab;
    //     $data->id_sub_syarat = $request->id_sub_syarat;
    //     $data->syarat = $request->syarat;
    //     $data->save();

    //     return redirect()->back()->with('success', 'Berhasil menambahkan syarat jabatan.');
    // }
    
    // public function hapusSyaratJabatan(Request $request)
    // {
    //     if(!$this->authenticateUser()) {
    //         return redirect('/');
    //     }
    //     $data = SyaratJabatan::find($request->id);
    //     $msg = $data->syarat;
    //     $data->delete();

    //     return redirect()->back()->with('success', 'Berhasil menghapus syarat jabatan.');
    // }

    // public function updateSyaratJabatan(Request $request)
    // {
    //     if(!$this->authenticateUser()) {
    //         return redirect('/');
    //     }
    //     $data = SyaratJabatan::find($request->id);
    //     $data->id_skpd_jab = $request->id_skpd_jab;
    //     $data->id_sub_syarat = $request->id_sub_syarat;
    //     $data->syarat = $request->syarat;
    //     $data->save();

    //     return redirect()->back()->with('success', 'Berhasil mengedit syarat jabatan.');
    // }

    // public function getDetailSyaratJabatan($id)
    // {
    //     $data = SyaratJabatan::find($id);
    //     return $data;
    // }

    // ===============================================     PENDIDIKAN      ====================================================

    public function indexPendidikan()
    {
        if(!$this->authenticateUser()) {
            return redirect('/');
        }
        $data['sideaktif'] = 3;
        $data['pendidikans'] = Pendidikan::get();

        return view('master.pendidikan', $data);
    }

    public function tambahPendidikan(Request $request)
    {
        if(!$this->authenticateUser()) {
            return redirect('/');
        }
        $validated = $request->validate([
            'nama_pendidikan' => 'required',
        ]);
        $data = new Pendidikan;
        $data->nama_pendidikan = $request->nama_pendidikan;
        $data->save();

        return redirect()->back()->with('success', 'Berhasil menambahkan pendidikan.');
    }
    
    public function hapusPendidikan(Request $request)
    {
        if(!$this->authenticateUser()) {
            return redirect('/');
        }
        $data = Pendidikan::find($request->id);
        $msg = $data->nama_pendidikan;
        $data->delete();

        return redirect()->back()->with('success', 'Berhasil menghapus pendidikan.');
    }

    public function updatePendidikan(Request $request)
    {
        if(!$this->authenticateUser()) {
            return redirect('/');
        }
        $validated = $request->validate([
            'nama_pendidikan' => 'required',
        ]);
        $data = Pendidikan::find($request->id);
        $data->nama_pendidikan = $request->nama_pendidikan;
        $data->save();

        return redirect()->back()->with('success', 'Berhasil mengedit pendidikan.');
    }

    public function getDetailPendidikan($id)
    {
        $data = Pendidikan::find($id);
        return $data;
    }

    // ===============================================     PANDUAN      ====================================================

    public function PanduanTugasPokok(){
        $text = $_GET['text'];
        $id = 1;
        $data = Panduan::find($id);
        $data->text_panduan = $text;
        $data->save();
        $data['message'] = 1;

        return $data;
    }
    public function PanduanHasilKerja(){
        $text = $_GET['text'];
        $id = 2;
        $data = Panduan::find($id);
        $data->text_panduan = $text;
        $data->save();
        $data['message'] = 1;

        return $data;
    }
    public function PanduanBahanKerja(){
        $text = $_GET['text'];
        $id = 3;
        $data = Panduan::find($id);
        $data->text_panduan = $text;
        $data->save();
        $data['message'] = 1;

        return $data;
    }

    public function PanduanTanggungJawab(){
        $text = $_GET['text'];
        $id = 4;
        $data = Panduan::find($id);
        $data->text_panduan = $text;
        $data->save();
        $data['message'] = 1;

        return $data;
    }
    public function PanduanPerangkatKerja(){
        $text = $_GET['text'];
        $id = 5;
        $data = Panduan::find($id);
        $data->text_panduan = $text;
        $data->save();
        $data['message'] = 1;

        return $data;
    }
    public function PanduanWewenang(){
        $text = $_GET['text'];
        $id = 6;
        $data = Panduan::find($id);
        $data->text_panduan = $text;
        $data->save();
        $data['message'] = 1;

        return $data;
    }

    public function PanduanKorelasiJabatan(){
        $text = $_GET['text'];
        $id = 7;
        $data = Panduan::find($id);
        $data->text_panduan = $text;
        $data->save();
        $data['message'] = 1;

        return $data;
    }

    public function PanduanLingkunganKerja(){
        $text = $_GET['text'];
        $id = 8;
        $data = Panduan::find($id);
        $data->text_panduan = $text;
        $data->save();
        $data['message'] = 1;

        return $data;
    }

    public function PanduanPrestasiKerja(){
        $text = $_GET['text'];
        $id = 9;
        $data = Panduan::find($id);
        $data->text_panduan = $text;
        $data->save();
        $data['message'] = 1;

        return $data;
    }

    public function PanduanSyaratJabatan(){
        $text = $_GET['text'];
        $id = 10;
        $data = Panduan::find($id);
        $data->text_panduan = $text;
        $data->save();
        $data['message'] = 1;

        return $data;
    }

    public function PanduanKualifikasiJabatan(){
        $text = $_GET['text'];
        $id = 11;
        $data = Panduan::find($id);
        $data->text_panduan = $text;
        $data->save();
        $data['message'] = 1;

        return $data;
    }

    public function PanduanResikoBahaya(){
        $text = $_GET['text'];
        $id = 12;
        $data = Panduan::find($id);
        $data->text_panduan = $text;
        $data->save();
        $data['message'] = 1;

        return $data;
    }
}
