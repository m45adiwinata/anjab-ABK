<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Jabatan;
use App\JenisJabatan;
use App\Eselon;
use App\SKPD;
use App\SKPDJabatan;
use App\OPDJabatan;
use App\Pegawai;
use Auth;

class OPDJabatanController extends Controller
{
    private function authenticateUser()
    {
        if(!Auth::user()) {
            return false;
        }
        else if(Auth::user()->role()->first()->master == 1) {
            return true;
        }
        
        return false;
    }

    public function index()
    {
        if(!$this->authenticateUser()) {
            return redirect('/');
        }
        $data['sideaktif'] = 4;
        $data['sideaktif2'] = 1;
        $data['jenisjabatans'] = JenisJabatan::get();

        return view('opdjab.index', $data);
    }

    // ===============================================     JENIS JABATAN       ====================================================

    public function createJenisJab(Request $request)
    {
        if(!$this->authenticateUser()) {
            return redirect('/');
        }
        $validated = $request->validate([
            'nama_jenis_jabatan' => 'required',
        ]);
        $data = new JenisJabatan;
        $data->nama_jenis_jabatan = $request->nama_jenis_jabatan;
        $data->save();

        return redirect('/opdjab');
    }

    public function deleteJenisJab(Request $request, $id)
    {
        if(!$this->authenticateUser()) {
            return redirect('/');
        }
        Jabatan::where('id_jenis_jabatan', $id)->delete();
        JenisJabatan::find($id)->delete();

        return redirect('/opdjab');
    }

    public function getDetailJenisJab($id)
    {
        if(!$this->authenticateUser()) {
            return redirect('/');
        }
        $data = JenisJabatan::find($id);

        return $data;
    }

    public function updateJenisJab(Request $request, $id)
    {
        if(!$this->authenticateUser()) {
            return redirect('/');
        }
        $validated = $request->validate([
            'nama_jenis_jabatan' => 'required',
        ]);
        $data = JenisJabatan::find($id);
        $data->nama_jenis_jabatan = $request->nama_jenis_jabatan;
        $data->save();

        return redirect('/opdjab');
    }

    public function jabatan()
    {
        if(!$this->authenticateUser()) {
            return redirect('/');
        }
        $data['sideaktif'] = 4;
        $data['sideaktif2'] = 2;
        $data['jenisjabatans'] = JenisJabatan::get();
        if(isset($_GET['nama_jabatan'])) {
            $data['jabatans'] = Jabatan::where('jabatan', 'like', '%'.$_GET['nama_jabatan'].'%')->paginate(20)->appends(request()->query());
        }
        else {
            $data['jabatans'] = Jabatan::paginate(20)->appends(request()->query());
        }
        $data['eselons'] = Eselon::get();

        return view('opdjab.jabatan', $data);
    }

    // ===============================================     JABATAN       ====================================================

    public function createJabatan(Request $request)
    {
        if(!$this->authenticateUser()) {
            return redirect('/');
        }
        $validated = $request->validate([
            'id_jenis_jabatan' => 'required',
            'jabatan' => 'required',
            'is_asisten' => 'required',
            'id_eselon' => 'required',
        ]);
        $data = new Jabatan;
        $data->id_jenis_jabatan = $request->id_jenis_jabatan;
        $data->jabatan = $request->jabatan;
        $data->is_asisten = $request->is_asisten;
        $data->id_eselon = $request->id_eselon;
        $data->save();

        return redirect('/opdjab/jabatan');
    }

    public function deleteJabatan(Request $request, $id)
    {
        if(!$this->authenticateUser()) {
            return redirect('/');
        }
        $skpd_jabs = SKPDJabatan::where('id_jabatan', $id)->get();
        $temp_jabatan = Jabatan::find($id);
        if(count($skpd_jabs) == 0) {
            $temp_jabatan->delete();
            $skpd_jabs->delete();
            OPDJabatan::where('id_jabatan', $id)->delete();
        }
        else {
            return redirect('/opdjab/jabatan')->with('fail', 'Tidak diizinkan: Jabatan '.$temp_jabatan->jabatan.' sudah ada di ABK.');
        }
        
        
        return redirect('/opdjab/jabatan');
    }

    public function getDetailJabatan($id)
    {
        $data = Jabatan::find($id);

        return $data;
    }

    public function updateJabatan(Request $request, $id)
    {
        if(!$this->authenticateUser()) {
            return redirect('/');
        }
        $validated = $request->validate([
            'id_jenis_jabatan' => 'required',
            'jabatan' => 'required',
            'is_asisten' => 'required',
            'id_eselon' => 'required',
        ]);
        $data = Jabatan::find($id);
        $data->id_jenis_jabatan = $request->id_jenis_jabatan;
        $data->jabatan = $request->jabatan;
        $data->is_asisten = $request->is_asisten;
        Pegawai::where('id_eselon', $data->id_eselon)->update(['id_eselon' => $request->id_eselon]);
        SKPDJabatan::where('id_eselon', $data->id_eselon)->update(['id_eselon' => $request->id_eselon]);
        $data->id_eselon = $request->id_eselon;
        $data->save();
        
        return redirect('/opdjab/jabatan');
    }

    // ===============================================     OPD       ====================================================

    public function OPD()
    {
        if(!$this->authenticateUser()) {
            return redirect('/');
        }
        $data['sideaktif'] = 4;
        $data['sideaktif2'] = 3;
        if(isset($_GET['nama_skpd'])) {
            $data['skpds'] = SKPD::where('nama_skpd', 'like', '%'.$_GET['nama_skpd'].'%')->withCount('opdjab as jabatan_count')->paginate(20)->appends(request()->query());
        }
        else {
            $data['skpds'] = SKPD::withCount('opdjab as jabatan_count')->paginate(20)->appends(request()->query());
        }

        return view('opdjab.opd', $data);
    }
    
    public function OPDJabatan()
    {
        if(!$this->authenticateUser()) {
            return redirect('/');
        }
        $data['sideaktif'] = 4;
        $data['sideaktif2'] = 4;
        if(isset($_GET['nama_opdjab'])) {
            $temp_idjab = Jabatan::where('jabatan', 'like', '%'.$_GET['nama_opdjab'].'%')->get()->pluck('id');
            if (isset($_GET['filter_skpd'])) {
                $id_skpd_selected = explode(",", $_GET['filter_skpd']);
                array_pop($id_skpd_selected);
                $data['opdjabs'] = OPDJabatan::whereIn('id_jabatan', $temp_idjab)->whereIn('id_skpd', $id_skpd_selected)->paginate(20)->appends(request()->query());
            }
            else {
                $data['opdjabs'] = OPDJabatan::whereIn('id_jabatan', $temp_idjab)->paginate(20)->appends(request()->query());
            }
        }
        else {
            if (isset($_GET['filter_skpd'])) {
                $id_skpd_selected = explode(",", $_GET['filter_skpd']);
                array_pop($id_skpd_selected);
                $data['opdjabs'] = OPDJabatan::whereIn('id_skpd', $id_skpd_selected)->paginate(20)->appends(request()->query());
            }
            else {
                $data['opdjabs'] = OPDJabatan::paginate(20)->appends(request()->query());
            }
        }
        $data['skpd_all'] = SKPD::get();

        return view('opdjab.opdjab', $data);
    }

    // ===============================================     OPD Jabatan      ====================================================

    public function getOPDJabDetail($id)
    {
        $data = OPDJabatan::find($id);
        if ($data->jabatan()->first()) {
            $data->nama_jabatan = $data->jabatan()->first()->jabatan;
        }
        else {
            $data->nama_jabatan = '';
        }
        if ($data->skpd()->first()) {
            $data->nama_skpd = $data->skpd()->first()->nama_skpd;
        }
        else {
            $data->nama_skpd = '';
        }
        return $data;
    }

    public function createOPDJab(Request $request)
    {
        if(!$this->authenticateUser()) {
            return redirect('/');
        }
        $validated = $request->validate([
            'id_skpd' => 'required',
            'id_jabatan' => 'required',
        ]);
        $redirect = redirect()->back()->getTargetUrl();
        $exists = OPDJabatan::where('id_skpd', $request->id_skpd)->where('id_jabatan', $request->id_jabatan)->get();
        $temp_jabatan = Jabatan::find($request->id_jabatan);
        $temp_skpd = SKPD::find($request->id_skpd);
        if(count($exists) > 0) {
            return redirect($redirect)->with('fail', 'Double Input: Jabatan '.$temp_jabatan->jabatan.' di OPD '.$temp_skpd->nama_skpd.' sudah ada.');
        }
        $data = new OPDJabatan;
        $data->id_skpd = $request->id_skpd;
        $data->id_jabatan = $request->id_jabatan;
        $data->save();

        return redirect($redirect)->with('success','Berhasil menambahkan OPD Jabatan.');
    }

    public function updateOPDJab(Request $request)
    {
        if(!$this->authenticateUser()) {
            return redirect('/');
        }
        $validated = $request->validate([
            'id' => 'required',
            'id_skpd' => 'required',
            'id_jabatan' => 'required',
        ]);
        $redirect = redirect()->back()->getTargetUrl();
        $exists = OPDJabatan::where('id_skpd', $request->id_skpd)->where('id_jabatan', $request->id_jabatan)->get();
        $temp_jabatan = Jabatan::find($request->id_jabatan);
        $temp_skpd = SKPD::find($request->id_skpd);
        if(count($exists) > 0) {
            return redirect($redirect)->with('fail', 'Double Input: Jabatan '.$temp_jabatan->jabatan.' di OPD '.$temp_skpd->nama_skpd.' sudah ada.');
        }
        $data = OPDJabatan::find($request->id);
        $temp_skpdjab = SKPDJabatan::where('id_skpd', $data->id_skpd)->where('id_jabatan', $data->id_jabatan)->first();
        // if($temp_skpdjab && $temp_skpdjab->status_laporan == '1,1,1,1,1,1,1,1,1,1') {
        if($temp_skpdjab) {
            return redirect($redirect)->with('fail', 'Tidak diizinkan: Jabatan '.$data->jabatan()->first()->jabatan.' di OPD '.$data->skpd()->first()->nama_skpd.' sudah masuk di ABK.');
        }
        else if($temp_skpdjab) {
            $temp_skpdjab->id_skpd = $request->id_skpd;
            $temp_skpdjab->id_jabatan = $request->id_jabatan;
            $temp_skpdjab->save();
        }
        
        $data->id_skpd = $request->id_skpd;
        $data->id_jabatan = $request->id_jabatan;
        $data->save();

        return redirect($redirect)->with('success','Berhasil mengedit OPD Jabatan.');
    }

    public function hapusOPDJab(Request $request)
    {
        if(!$this->authenticateUser()) {
            return redirect('/');
        }
        $validated = $request->validate([
            'id' => 'required',
        ]);
        $redirect = redirect()->back()->getTargetUrl();
        $data = OPDJabatan::find($request->id);
        $temp_skpdjab = SKPDJabatan::where('id_skpd', $data->id_skpd)->where('id_jabatan', $data->id_jabatan)->first();
        // if($temp_skpdjab && $temp_skpdjab->status_laporan == '1,1,1,1,1,1,1,1,1,1') {
        if($temp_skpdjab) {
            return redirect($redirect)->with('fail', 'Tidak diizinkan: Jabatan '.$data->jabatan()->first()->jabatan.' di OPD '.$data->skpd()->first()->nama_skpd.' sudah masuk di ABK.');
        }
        // else if($temp_skpdjab) {
        //     $temp_skpdjab->delete();
        // }

        $data->delete();

        return redirect($redirect)->with('success','Berhasil menghapus OPD Jabatan.');
    }

    public function getJabatanOPDTersisa()
    {
        if(isset($_GET['id'])) {
            $jabs = OPDJabatan::where('id', '!=', $_GET['id'])->where('id_skpd', $_GET['id_skpd'])->get()->pluck('id_jabatan');
        }
        else {
            $jabs = OPDJabatan::where('id_skpd', $_GET['id_skpd'])->get()->pluck('id_jabatan');
        }
        $data = Jabatan::whereNotIn('id', $jabs)->get()->pluck('jabatan','id');
        
        return $data;
    }
}
