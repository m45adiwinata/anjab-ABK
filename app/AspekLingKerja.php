<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AspekLingKerja extends Model
{
    protected $table = 'aspek_lk';

    public function faktor()
    {
        return $this->hasMany('App\FaktorAspek', 'id_aspek_lk');
    }
}
