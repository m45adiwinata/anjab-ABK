<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PrestasiKerja extends Model
{
    protected $table = 'prestasi_kerja';

    public function SKPDJab()
    {
        return $this->belongsTo('App\SKPDJabatan', 'id_skpd_jab');
    }
}
