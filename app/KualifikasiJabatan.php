<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KualifikasiJabatan extends Model
{
    protected $table = 'kualifikasi_jabatan';
    
    public function pendidikan()
    {
        return $this->belongsTo('App\Pendidikan', 'id_pendidikan');
    }
}
