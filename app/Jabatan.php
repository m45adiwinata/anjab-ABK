<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jabatan extends Model
{
    protected $table = 'jabatan';
    public $timestamps = 'false';

    public function skpd()
    {
        return $this->belongsToMany('App\SKPD', 'skpd_jabatan', 'id_jabatan', 'id_skpd');
    }

    public function jenisJab()
    {
        return $this->belongsTo('App\JenisJabatan', 'id_jenis_jabatan');
    }

    public function eselon()
    {
        return $this->belongsTo('App\Eselon', 'id_eselon');
    }
}
