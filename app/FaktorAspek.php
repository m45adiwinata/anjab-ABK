<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FaktorAspek extends Model
{
    protected $table = 'faktor_aspek';

    public function aspek()
    {
        return $this->belongsTo('App\AspekLingKerja', 'id_aspek_lk');
    }
}
