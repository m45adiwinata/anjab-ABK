<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KorelasiJabatan extends Model
{
    protected $table = "korelasi_jabatan";

    public function SKPDJab()
    {
        return $this->belongsTo('App\SKPDJabatan', 'id_skpd_jab');
    }

    public function jabatan()
    {
        return $this->belongsTo('App\Jabatan', 'id_jabatan');
    }

    public function aspek()
    {
        return $this->belongsTo('App\AspekLingKerja', 'id_aspek_lk');
    }
}
