<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TanggungJawab extends Model
{
    protected $table = 'tanggung_jawab';

    public function SKPDJab()
    {
        return $this->belongsTo('App\SKPDJabatan', 'id_skpd_jab');
    }
}
