<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BahanKerja extends Model
{
    protected $table = "bahan_kerja";

    public function SKPDJab()
    {
        return $this->belongsTo('App\SKPDJabatan', 'id_skpd_jab');
    }
}
