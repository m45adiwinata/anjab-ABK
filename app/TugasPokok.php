<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TugasPokok extends Model
{
    protected $table = 'tugas_pokok';

    public function hasilkerja()
    {
        return $this->belongsTo('App\HasilKerja', 'id_hasil_kerja');
    }

    public function skpdjab()
    {
        return $this->belongsTo('App\SKPDJabatan', 'id_skpd_jab');
    }
}
