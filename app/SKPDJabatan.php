<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SKPDJabatan extends Model
{
    protected $table = 'skpd_jabatan';
    public $timestamps = 'false';

    // public function detailjabatan()
    // {
    //     return $this->belongsTo('App\Jabatan','jabatan', 'id_jenis_jabatan');
    // }

    public function skpd()
    {
        return $this->belongsTo('App\SKPD', 'id_skpd');
    }

    public function jabatan()
    {
        return $this->belongsTo('App\Jabatan', 'id_jabatan');
    }

    public function child()
    {
        return $this->hasMany('App\SKPDJabatan', 'id_jab_atasan');
    }
    
    public function tugasPokok()
    {
        return $this->hasMany('App\TugasPokok', 'id_skpd_jab');
    }

    public function hasilKerja()
    {
        return $this->hasMany('App\HasilKerjas', 'id_skpd_jab');
    }

    public function bahanKerja()
    {
        return $this->hasMany('App\BahanKerja', 'id_skpd_jab');
    }

    public function perangkatKerja()
    {
        return $this->hasMany('App\PerangkatKerja', 'id_skpd_jab');
    }

    public function tanggungJawab()
    {
        return $this->hasMany('App\TanggungJawab', 'id_skpd_jab');
    }

    public function wewenang()
    {
        return $this->hasMany('App\Wewenang', 'id_skpd_jab');
    }

    public function korelasiJabatan()
    {
        return $this->hasMany('App\KorelasiJabatan', 'id_skpd_jab');
    }

    public function lingkunganKerja()
    {
        return $this->hasMany('App\LingkunganKerja', 'id_skpd_jab');
    }

    public function prestasiKerja()
    {
        return $this->hasMany('App\PrestasiKerja', 'id_skpd_jab');
    }

    public function syaratJabatan()
    {
        return $this->hasMany('App\SyaratJabatan', 'id_skpd_jab');
    }
    
    public function kualifikasiJabatan()
    {
        return $this->hasMany('App\KualifikasiJabatan', 'id_skpd_jab');
    }

    public function resikoBahaya()
    {
        return $this->hasMany('App\ResikoBahaya', 'id_skpd_jab');
    }

    public function pegawai()
    {
        return $this->hasMany('App\Pegawai', 'id_skpd_jab');
    }

    public function petaJabatan()
    {
        return $this->hasMany('App\PetaJabatan', 'id_skpd_jab');
    }

    public function eselon()
    {
        return $this->belongsTo('App\Eselon', 'id_eselon');
    }

    public static function boot() {
        parent::boot();

        static::deleting(function($skpd_jab) {
            $skpd_jab->tugasPokok()->delete();
            $skpd_jab->hasilKerja()->delete();
            $skpd_jab->bahanKerja()->delete();
            $skpd_jab->perangkatKerja()->delete();
            $skpd_jab->tanggungJawab()->delete();
            $skpd_jab->wewenang()->delete();
            $skpd_jab->korelasiJabatan()->delete();
            $skpd_jab->lingkunganKerja()->delete();
            $skpd_jab->prestasiKerja()->delete();
            $skpd_jab->syaratJabatan()->delete();
            $skpd_jab->kualifikasiJabatan()->delete();
            $skpd_jab->resikoBahaya()->delete();
            $skpd_jab->petaJabatan()->delete();
            $skpd_jab->pegawai()->update(['id_skpd_jab' => null]);
        });
    }
}
