<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HasilKerjas extends Model
{
    protected $table = 'hasil_kerja';
    
    public function SKPDJab()
    {
        return $this->belongsTo('App\SKPDJabatan', 'id_skpd_jab');
    }

    public function satuan()
    {
        return $this->belongsTo('App\Satuan', 'id_satuan');
    }
}
