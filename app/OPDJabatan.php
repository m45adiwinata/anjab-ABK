<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OPDJabatan extends Model
{
    protected $table = 'opd_jabatan';

    public function skpd()
    {
        return $this->belongsTo('App\SKPD', 'id_skpd');
    }

    public function jabatan()
    {
        return $this->belongsTo('App\Jabatan', 'id_jabatan');
    }
}
