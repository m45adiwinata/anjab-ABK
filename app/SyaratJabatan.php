<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SyaratJabatan extends Model
{
    protected $table = 'syarat_jabatan';

    public function SKPDJab()
    {
        return $this->belongsTo('App\SKPDJab', 'id_skpd_jab');
    }

    public function point()
    {
        return $this->belongsTo('App\SubSyaratJabatan', 'id_sub_syarat');
    }
}
