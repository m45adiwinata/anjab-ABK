# -*- coding: utf-8 -*-
"""
Created on Mon Feb 15 21:06:56 2021

@author: m45ad
"""


import requests
import mysql.connector

db = mysql.connector.connect(
        host = "localhost",
        user = "root",
        password = "",
        database = "anjab"
)
csr = db.cursor()

#GET DATA SKPD
'''
response = requests.get("http://simpeg.badungkab.go.id/api.php?c=skpd")
data = response.json()['data']
for d in data:
    sql = "INSERT INTO skpd (kode_skpd, nama_skpd, created_at, updated_at) VALUES (%s, %s, NOW(), NOW())"
    csr.execute(sql, (str(d['id_skpd']), str(d['nama_skpd'])))
    db.commit()
'''

#GET DATA GOLONGAN
'''
response = requests.get("http://simpeg.badungkab.go.id/api.php?c=golongan")
data = response.json()['data']
for d in data:
    sql = "INSERT INTO golongan (id_golongan, golongan, keterangan) VALUES (%s, %s, %s)"
    csr.execute(sql, (str(d['id_golongan']), str(d['golongan']), str(d['keterangan'])))
    db.commit()
'''

#GET DATA ESELON
'''
response = requests.get("http://simpeg.badungkab.go.id/api.php?c=eselon")
data = response.json()['data']
for d in data:
    sql = "INSERT INTO eselon (id_eselon, eselon) VALUES (%s, %s)"
    csr.execute(sql, (str(d['id_eselon']), str(d['eselon'])))
    db.commit()
'''

#GET DATA PEGAWAI

response = requests.get("http://simpeg.badungkab.go.id/api.php?c=pegawai")
data = response.json()['data']
for d in data:
    sql = "INSERT INTO pegawai (nip, nama, alamat, id_skpd_jab) VALUES (%s, %s, %s, %s)"
    csr.execute(sql, (str(d['nip']), str(d['nama_pegawai']), str(d['alamat']), str(d['id_skpdjab'])))
    db.commit()


#GET DATA SKPD JABATAN
'''
response = requests.get("http://simpeg.badungkab.go.id/api.php?c=skpdjabatan")
data = response.json()['data']
for d in data:
    sql = "INSERT INTO skpd_jabatan (id_skpd, id_jabatan) VALUES (%s, %s)"
    csr.execute(sql, (str(d['id_skpd']), str(d['id_jabatan'])))
    db.commit()
'''
csr.close()
db.close()