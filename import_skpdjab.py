# -*- coding: utf-8 -*-
"""
Created on Mon May 10 19:39:28 2021

@author: m45ad
"""


import time
import mysql.connector
import pandas as pd
import numpy as np

start_time = time.time()

db = mysql.connector.connect(
        host = "localhost",
        port = "3306",
        user = "root",
        password = "",
        database = "anjab"
)
csr = db.cursor()

data = np.array(pd.read_excel('skpdjab.xlsx', keep_default_na=False, dtype='str'), dtype='str')
temp_nama_jabatan = ''
kode_skpd = 1
temp_id_jabatan = 0
for d in data:
    if d[0] != '':
        sql = "INSERT INTO skpd (kode_skpd, nama_skpd) VALUES (%s, %s)"
        csr.execute(sql, (str(kode_skpd), str(d[0])))
        db.commit()
        kode_skpd += 1
    sql = "SELECT id, count(id) FROM jabatan WHERE jabatan = %s"
    csr.execute(sql, (str(d[1]),))
    found = csr.fetchone()
    if d[1] != temp_nama_jabatan and found[1] == 0:
        sql = "INSERT INTO jabatan (jabatan) VALUES (%s)"
        csr.execute(sql, (str(d[1]),))
        db.commit()
        temp_id_jabatan += 1
    if found[0] == None:
        id_jabatan = temp_id_jabatan
    else:
        id_jabatan = found[0]
    sql = "INSERT INTO opd_jabatan (id_skpd, id_jabatan) VALUES (%s, %s)"
    csr.execute(sql, (str(kode_skpd-1),str(id_jabatan)))
    db.commit()

csr.close()
db.close()
print("--- Waktu eksekusi program: %s detik ---" % (time.time() - start_time))