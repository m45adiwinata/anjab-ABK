<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::get('/get-skpd/{id}', 'ABKController@getSKPD');
Route::get('/get-skpd-jabatan/{id}', 'ABKController@getDetailSKPDJabatan');

Route::get('/get-jenis-jab/{id}', 'ABKController@getJenisJab');
Route::get('/get-eselon-jab/{id}', 'ABKController@getEselonJab');
Route::get('/get-tugaspokok/{id}', 'AnjabController@getTugasPokok');
Route::get('/get-hasilkerja/{id}', 'AnjabController@getHasilKerja');
Route::get('/get-bahankerja/{id}', 'AnjabController@getBahanKerja');
Route::get('/get-perangkatkerja/{id}', 'AnjabController@getPerangkatKerja');
Route::get('/get-tanggungjawab/{id}', 'AnjabController@getTanggungJawab');
Route::get('/get-wewenang/{id}', 'AnjabController@getWewenang');
Route::get('/get-korelasijabatan/{id}', 'AnjabController@getKorelasiJabatan');
Route::get('/get-lingkungankerja/{id}', 'AnjabController@getLingkunganKerja');
Route::get('/get-prestasikerja/{id}', 'AnjabController@getPrestasiKerja');
Route::get('/get-waktupenyelesaian-hasil-kerja/{prestasi}', 'AnjabController@getWaktuPenyelesaian');
Route::get('/get-syaratjabatan/{id}', 'AnjabController@getSyaratJabatan');
Route::get('/get-kualifikasijabatan/{id}', 'AnjabController@getKualifikasiJabatan');
Route::get('/get-resikobahaya/{id}', 'AnjabController@getResikoBahaya');
Route::get('/get-jabatan-koreljab', 'AnjabController@getJabatanKorelJab');

Route::post('/master/toggle-active-akun', 'MasterController@toggleActiveAkun');
Route::get('/opdjab/get-jabatan-opd-tersisa', 'OPDJabatanController@getJabatanOPDTersisa');