<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'UserController@index');

// Auth::routes();
// Route::get('/home', 'HomeController@index')->name('home');
Route::get('/login', 'UserController@loginForm');
Route::post('/login', 'UserController@login')->name('login');
Route::get('/logout', 'UserController@logout')->name('logout');
Route::get('/userprofile', 'UserController@profile');
Route::post('/update-userprofile', 'UserController@updateUser')->name('updateuser');

Route::get('/abk', 'ABKController@index');
Route::get('/abk/tambah-skpd', 'ABKController@tambahSKPD');
Route::post('/abk/tambah-skpd', 'ABKController@createSKPD')->name('createskpd');
Route::get('/abk/edit-skpd/{id}', 'ABKController@editSKPD')->name('editskpd');
Route::post('/abk/update-skpd/{id}', 'ABKController@updateSKPD')->name('updateskpd');
Route::post('/abk/hapus-skpd/{id}', 'ABKController@hapusSKPD')->name('hapusskpd');
Route::get('/abk/detailskpd/{id}', 'ABKController@detailSKPD');
Route::get('/abk/detailskpd/{id}/peta-jabatan', 'ABKController@lihatPetaJabatan');
Route::post('/abk/peta-jabatan/update/{id_node}/{id_pegawai}', 'ABKController@updateNodePetaJabatan');
Route::get('/abk/peta-jabatan/get-opsi-pegawai/{id_node}', 'ABKController@getPegawaiPetaJabatan');

Route::get('/abk/{id_skpd}/tambah-jabatan', 'ABKController@formTambahJab');
Route::post('/abk/{id_skpd}/tambah-jabatan', 'ABKController@tambahJab')->name('tambahjab');
Route::get('/abk/detailjab/{id}', 'ABKController@detailJab');
Route::post('/abk/detailjab', 'ABKController@updateJab')->name('updatejab');
Route::post('/abk/deletejabskpd/{id}', 'ABKController@deleteJabSkpd')->name('deletejabskpd');
Route::post('/abk/deletejab/{id_skpd}', 'ABKController@deleteJab');
Route::get('/abk/cetak-pdf/{id_skpd}', 'ABKController@cetakPDF');
Route::get('/abk/peta-jabatan/get-node-detail/{id}', 'ABKController@getNodeDetail');
Route::get('/abk/peta-jabatan/simpan-pegawai/{id_skpdjab}/{id_pegawai}', 'ABKController@simpanPegawai');


Route::get('/skpd-jab/{id}/jangan-tampil-panduan/{nomor_abk}', 'AnjabController@janganTampilPanduan');

Route::get('/uraian-tugas/{id}', 'AnjabController@uraianTugas');
Route::post('/uraian-tugas/{id_skpd_jab}/tambah-tugas-pokok', 'AnjabController@tambahTugasPokok')->name('tambahtgspokok');
Route::post('/uraian-tugas/{id}/delete', 'AnjabController@deleteTugasPokok')->name('deletetugaspokok');
Route::post('/uraian-tugas/update/{id}', 'AnjabController@updateTugasPokok')->name('updatetgspokok');
Route::post('/uraian-tugas/{id}/ubah-urutan', 'AnjabController@ubahUrutanTugasPokok')->name('ubahurutantgspokok');


Route::get('/hasil-kerja/{id}', 'AnjabController@hasilKerja');
Route::post('/hasil-kerja/{id_skpd_jab}/tambah-hasil-kerja', 'AnjabController@tambahHasilKerja')->name('tambahhasilkerja');
Route::post('/hasil-kerja/{id}/delete', 'AnjabController@deleteHasilKerja')->name('deletehasilkerja');
Route::post('/hasil-kerja/update/{id}', 'AnjabController@updateHasilKerja')->name('updatehasilkerja');

Route::get('/bahan-kerja/{id}', 'AnjabController@bahanKerja');
Route::post('/bahan-kerja/{id_skpd_jab}/tambah-bahan-kerja', 'AnjabController@tambahBahanKerja')->name('tambahbahankerja');
Route::post('/bahan-kerja/{id}/delete', 'AnjabController@deleteBahanKerja')->name('deletebahankerja');
Route::post('/bahan-kerja/update/{id}', 'AnjabController@updateBahanKerja')->name('updatebahankerja');

Route::get('/perangkat-kerja/{id}', 'AnjabController@perangkatKerja');
Route::post('/perangkat-kerja/{id_skpd_jab}/tambah-perangkat-kerja', 'AnjabController@tambahPerangkatKerja')->name('tambahperangkatkerja');
Route::post('/perangkat-kerja/{id}/delete', 'AnjabController@deletePerangkatKerja')->name('deleteperangkatkerja');
Route::post('/perangkat-kerja/update/{id}', 'AnjabController@updatePerangkatKerja')->name('updateperangkatkerja');

Route::get('/tanggung-jawab/{id}', 'AnjabController@tanggungJawab');
Route::post('/tanggung-jawab/{id_skpd_jab}/tambah-tanggung-jawab', 'AnjabController@tambahTanggungJawab')->name('tambahtanggungjawab');
Route::post('/tanggung-jawab/{id}/delete', 'AnjabController@deleteTanggungJawab')->name('deletetanggungjawab');
Route::post('/tanggung-jawab/update/{id}', 'AnjabController@updateTanggungJawab')->name('updatetanggungjawab');

Route::get('/wewenang/{id}', 'AnjabController@wewenang');
Route::post('/wewenang/{id_skpd_jab}/tambah-wewenang', 'AnjabController@tambahWewenang')->name('tambahwewenang');
Route::post('/wewenang/{id}/delete', 'AnjabController@deleteWewenang')->name('deletewewenang');
Route::post('/wewenang/update/{id}', 'AnjabController@updateWewenang')->name('updatewewenang');

Route::get('/korelasi-jabatan/{id}', 'AnjabController@korelasijabatan');
Route::post('/korelasi-jabatan/{id_skpd_jab}/tambah-korelasi-jabatan', 'AnjabController@tambahKorelasiJabatan')->name('tambahkorelasijabatan');
Route::post('/korelasi-jabatan/{id}/delete', 'AnjabController@deleteKorelasiJabatan')->name('deletekorelasijabatan');
Route::post('/korelasi-jabatan/update/{id}', 'AnjabController@updateKorelasiJabatan')->name('updatekorelasijabatan');

Route::get('/lingkungan-kerja/{id}', 'AnjabController@lingkunganKerja');
Route::post('/lingkungan-kerja/{id_skpd_jab}/tambah-lingkungan-kerja', 'AnjabController@tambahLingkunganKerja')->name('tambahlingkungankerja');
Route::post('/lingkungan-kerja/{id}/delete', 'AnjabController@deleteLingkunganKerja')->name('deletelingkungankerja');
Route::post('/lingkungan-kerja/update/{id}', 'AnjabController@updateLingkunganKerja')->name('updatelingkungankerja');

Route::get('/prestasi-kerja/{id}', 'AnjabController@prestasiKerja');
Route::post('/prestasi-kerja/{id_skpd_jab}/tambah-prestasi-kerja', 'AnjabController@tambahPrestasiKerja')->name('tambahprestasikerja');
Route::post('/prestasi-kerja/{id}/delete', 'AnjabController@deletePrestasiKerja')->name('deleteprestasikerja');
Route::post('/prestasi-kerja/update/{id}', 'AnjabController@updatePrestasiKerja')->name('updateprestasikerja');

Route::get('/syarat-jabatan/{id}', 'AnjabController@syaratJabatan');
Route::post('/syarat-jabatan/{id_skpd_jab}/tambah-syarat-jabatan', 'AnjabController@tambahSyaratJabatan')->name('tambahsyaratjabatan');
Route::post('/syarat-jabatan/{id}/delete', 'AnjabController@deleteSyaratJabatan')->name('deletesyaratjabatan');
Route::post('/syarat-jabatan/update/{id}', 'AnjabController@updateSyaratJabatan')->name('updatesyaratjabatan');

Route::get('/kualifikasi-jabatan/{id}', 'AnjabController@kualifikasiJabatan');
Route::post('/kualifikasi-jabatan/{id_skpd_jab}/tambah-kualifikasi-jabatan', 'AnjabController@tambahKualifikasiJabatan')->name('tambahkualifikasijabatan');
Route::post('/kualifikasi-jabatan/delete', 'AnjabController@deleteKualifikasiJabatan')->name('deletekualifikasijabatan');
Route::post('/kualifikasi-jabatan/update', 'AnjabController@updateKualifikasiJabatan')->name('updatekualifikasijabatan');

Route::get('/resiko-bahaya/{id}', 'AnjabController@resikoBahaya');
Route::post('/resiko-bahaya/{id_skpd_jab}/tambah-resiko-bahaya', 'AnjabController@tambahResikoBahaya')->name('tambahresikobahaya');
Route::post('/resiko-bahaya/delete', 'AnjabController@deleteResikoBahaya')->name('deleteresikobahaya');
Route::post('/resiko-bahaya/update', 'AnjabController@updateResikoBahaya')->name('updateresikobahaya');

Route::get('/master', 'MasterController@index');
Route::post('/master/create-akun', 'MasterController@createAkun')->name('create-akun');
Route::post('/master/delete-akun/{id}', 'MasterController@deleteAkun')->name('delete-akun');
Route::post('/master/update-akun/{id}', 'MasterController@updateAkun')->name('update-akun');
Route::get('/master/get-user-detail/{id}', 'MasterController@getDetailAkun');
Route::get('/master/get-pegawai-by-skpd/{id_skpd}', 'MasterController@getPegawaiBySKPD');

Route::post('/master/create-pegawai', 'MasterController@createPegawai')->name('create-pegawai');
Route::post('/master/delete-pegawai', 'MasterController@deletePegawai')->name('delete-pegawai');
Route::post('/master/update-pegawai', 'MasterController@updatePegawai')->name('update-pegawai');
Route::get('/master/get-pegawai-detail/{id}', 'MasterController@getDetailPegawai');

Route::get('/master/hasil-uraian-kerja', 'MasterController@indexHasilUraianKerja');
Route::post('/master/hasil-uraian-kerja/tambah', 'MasterController@tambahHasilUraianKerja')->name('tambah-hasil-uraian-kerja');
Route::post('/master/hasil-uraian-kerja/hapus', 'MasterController@hapusHasilUraianKerja')->name('hapus-hasil-uraian-kerja');
Route::post('/master/hasil-uraian-kerja/update', 'MasterController@updateHasilUraianKerja')->name('update-hasil-uraian-kerja');
Route::get('/master/hasil-uraian-kerja/get-detail/{id}', 'MasterController@getDetailHasilUraianKerja');
Route::get('/master/satuan', 'MasterController@indexSatuan');
Route::post('/master/satuan/tambah', 'MasterController@tambahSatuan')->name('tambah-satuan');
Route::post('/master/satuan/hapus', 'MasterController@hapusSatuan')->name('hapus-satuan');
Route::post('/master/satuan/update', 'MasterController@updateSatuan')->name('update-satuan');
Route::get('/master/satuan/get-detail/{id}', 'MasterController@getDetailSatuan');
Route::get('/master/aspek-lingkungan-kerja', 'MasterController@indexLingkunganKerja');
Route::post('/master/aspek-lingkungan-kerja/tambah', 'MasterController@tambahLingkunganKerja')->name('tambah-aspek-lingkungan-kerja');
Route::post('/master/aspek-lingkungan-kerja/hapus', 'MasterController@hapusLingkunganKerja')->name('hapus-aspek-lingkungan-kerja');
Route::post('/master/aspek-lingkungan-kerja/update', 'MasterController@updateLingkunganKerja')->name('update-aspek-lingkungan-kerja');
Route::get('/master/aspek-lingkungan-kerja/get-detail/{id}', 'MasterController@getDetailLingkunganKerja');
Route::post('/master/faktor-aspek/tambah', 'MasterController@tambahFaktorAspek')->name('tambah-faktor-aspek');
Route::post('/master/faktor-aspek/hapus', 'MasterController@hapusFaktorAspek')->name('hapus-faktor-aspek');
Route::post('/master/faktor-aspek/update', 'MasterController@updateFaktorAspek')->name('update-faktor-aspek');
Route::get('/master/faktor-aspek/get-detail/{id}', 'MasterController@getDetailFaktorAspek');

Route::get('/master/panduan-tugas-pokok', 'MasterController@PanduanTugasPokok')->name('panduan-tugas-pokok');
Route::get('/master/panduan-hasil-kerja', 'MasterController@PanduanHasilKerja')->name('panduan-hasil-kerja');
Route::get('/master/panduan-bahan-kerja', 'MasterController@PanduanBahanKerja')->name('panduan-bahan-kerja');
Route::get('/master/panduan-tanggung-jawab', 'MasterController@PanduanTanggungJawab')->name('panduan-tanggung-jawab');
Route::get('/master/panduan-perangkat-kerja', 'MasterController@PanduanPerangkatKerja')->name('panduan-perangkat-kerja');
Route::get('/master/panduan-wewenang', 'MasterController@PanduanWewenang')->name('panduan-wewenang');
Route::get('/master/panduan-korelasi-jabatan', 'MasterController@PanduanKorelasiJabatan')->name('panduan-korelasi-jabatan');
Route::get('/master/panduan-lingkungan-kerja', 'MasterController@PanduanLingkunganKerja')->name('panduan-lingkungan-kerja');
Route::get('/master/panduan-prestasi-kerja', 'MasterController@PanduanPrestasiKerja')->name('panduan-prestasi-kerja');
Route::get('/master/panduan-syarat-jabatan', 'MasterController@PanduanSyaratJabatan')->name('panduan-syarat-jabatan');
Route::get('/master/panduan-kualifikasi-jabatan', 'MasterController@PanduanKualifikasiJabatan')->name('panduan-kualifikasi-jabatan');
Route::get('/master/panduan-resiko-bahaya', 'MasterController@PanduanResikoBahaya')->name('panduan-resiko-bahaya');


Route::get('/master/syarat', 'MasterController@indexSyarat');
Route::post('/master/syarat/tambah', 'MasterController@tambahSyarat')->name('tambah-syarat');
Route::post('/master/syarat/hapus', 'MasterController@hapusSyarat')->name('hapus-syarat');
Route::post('/master/syarat/update', 'MasterController@updateSyarat')->name('update-syarat');
Route::get('/master/syarat/get-detail/{id}', 'MasterController@getDetailSyarat');
// Route::post('/master/syarat-jabatan/tambah', 'MasterController@tambahSyaratJabatan')->name('tambah-syarat-jabatan');
// Route::post('/master/syarat-jabatan/hapus', 'MasterController@hapusSyaratJabatan')->name('hapus-syarat-jabatan');
// Route::post('/master/syarat-jabatan/update', 'MasterController@updateSyaratJabatan')->name('update-syarat-jabatan');
// Route::get('/master/syarat-jabatan/get-detail/{id}', 'MasterController@getDetailSyaratJabatan');

Route::get('/master/pendidikan', 'MasterController@indexPendidikan');
Route::post('/master/pendidikan/tambah', 'MasterController@tambahPendidikan')->name('tambah-pendidikan');
Route::post('/master/pendidikan/hapus', 'MasterController@hapusPendidikan')->name('hapus-pendidikan');
Route::post('/master/pendidikan/update', 'MasterController@updatePendidikan')->name('update-pendidikan');
Route::get('/master/pendidikan/get-detail/{id}', 'MasterController@getDetailPendidikan');

Route::get('/opdjab', 'OPDJabatanController@index');
Route::post('/opdjab/create-jenis-jabatan', 'OPDJabatanController@createJenisJab')->name('create-jenis-jab');
Route::post('/opdjab/delete-jenis-jabatan/{id}', 'OPDJabatanController@deleteJenisJab')->name('delete-jenis-jab');
Route::post('/opdjab/update-jenis-jabatan/{id}', 'OPDJabatanController@updateJenisJab')->name('update-jenis-jab');
Route::get('/opdjab/get-jenis-jabatan-detail/{id}', 'OPDJabatanController@getDetailJenisJab');
Route::get('/opdjab/jabatan', 'OPDJabatanController@jabatan');
Route::post('/opdjab/create-jabatan', 'OPDJabatanController@createJabatan')->name('create-jabatan');
Route::post('/opdjab/delete-jabatan/{id}', 'OPDJabatanController@deleteJabatan')->name('delete-jabatan');
Route::post('/opdjab/update-jabatan/{id}', 'OPDJabatanController@updateJabatan')->name('update-jabatan');
Route::get('/opdjab/get-jabatan-detail/{id}', 'OPDJabatanController@getDetailJabatan');
Route::get('/opdjab/opd', 'OPDJabatanController@OPD');
Route::post('/opdjab/opdjab/create', 'OPDJabatanController@createOPDJab')->name('create-opdjab');
Route::post('/opdjab/opdjab/hapus', 'OPDJabatanController@hapusOPDJab')->name('hapus-opdjab');
Route::post('/opdjab/opdjab/update', 'OPDJabatanController@updateOPDJab')->name('update-opdjab');
Route::get('/opdjab/get-opdjab-detail/{id}', 'OPDJabatanController@getOPDJabDetail');
Route::get('/opdjab/opd-jabatan', 'OPDJabatanController@OPDJabatan');
